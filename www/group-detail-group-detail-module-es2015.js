(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["group-detail-group-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/add-member/add-member.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/add-member/add-member.component.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-card-title>Add New Member</ion-card-title>\n</ion-header>\n<ion-content>\n  <form #newMemberForm=\"ngForm\">\n    <ion-item>\n      <ion-label position=\"floating\">Email</ion-label>\n      <ion-input type=\"text\" name=\"newMemberEmail\" required email ngModel #newMemberEmail=\"ngModel\"></ion-input>\n    </ion-item>\n    <ion-item *ngIf=\"!newMemberEmail.valid && newMemberEmail.touched\" lines=\"none\">\n      <ion-text>Enter a Valid Email</ion-text>\n    </ion-item>\n    <ion-item>\n      <ion-select name=\"newMemberRole\" #newMemberRole ngModel=\"Member\" value=\"Member\">\n        <ion-select-option value=\"Member\">Member</ion-select-option>\n        <ion-select-option value=\"Manager\">Manager</ion-select-option>\n        <ion-select-option value=\"Owner\" *ngIf=\"loadedGroup.role == 'Owner'\">Owner</ion-select-option>\n      </ion-select>\n    </ion-item>\n    <ion-button type=\"button\" color=\"primary\" expand=\"block\" [disabled]=\"!newMemberForm.valid\"\n      (click)=\"onAddNewMember(newMemberForm)\">\n      Add\n    </ion-button>\n    <ion-button type=\"button\" color=\"primary\" expand=\"block\"\n    (click)=\"onCancel()\">\n    Cancel\n  </ion-button>\n  </form>\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/edit-task/edit-task.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/edit-task/edit-task.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n<div *ngIf=\"!isLoading\">\n  <ion-header>\n    <ion-card-title>{{ this.groupTask ? 'Update' : 'Add New' }} Task</ion-card-title>\n  </ion-header>\n  <ion-content>\n    <form #newTaskForm=\"ngForm\">\n      <ion-item>\n        <ion-label position=\"floating\">Name</ion-label>\n        <ion-input type=\"text\" ngModel name=\"name\" required [ngModel]=\"groupTask?.name\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\">Description</ion-label>\n        <ion-input type=\"text\" ngModel name=\"description\" required [ngModel]=\"groupTask?.description\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\">Due Date</ion-label>\n        <ion-datetime display-format=\"MMM DD YYYY\" [min]=\"dueDatePickerStart\" [max]=\"dueDatePickerStop\" name=\"dueDate\"\n          required [ngModel]=\"groupTask?.dueDate.toISOString()\"></ion-datetime>\n      </ion-item>\n      <ion-item *ngIf=\"memberSelectorOptions.length > 1\">\n        <ion-label position=\"floating\">Owner</ion-label>\n        <ion-select required name=\"owner\" [ngModel]=\"groupTask?.ownerIdentifier\">\n          <ion-select-option value=\"{{ option.value}}\" *ngFor=\"let option of memberSelectorOptions\">\n            {{ option.display }}\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" [disabled]=\"!newTaskForm.valid\"\n        (click)=\"onAddTask(newTaskForm)\">\n        {{ this.groupTask ? 'Update' : 'Add' }} Task\n      </ion-button>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onCancel()\">\n        Cancel\n      </ion-button>\n    </form>\n  </ion-content>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/group-detail.page.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/group-detail.page.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Group Detail</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/groups\"></ion-back-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\" *ngIf=\"!isLoading\">\n      <ion-button (click)=\"onRefreshGroup()\">\n        <ion-icon name=\"refresh\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n      <ion-button (click)=\"onDeleteGroup()\" *ngIf=\"amOwner && loadedGroup.ownerCount == 1\">\n        <ion-icon name=\"trash-outline\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n      <ion-button (click)=\"onLeaveGroup()\" *ngIf=\"!amOwner || loadedGroup.ownerCount != 1\">\n        Leave\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n  <div *ngIf=\"!isLoading\">\n\n    <ion-card *ngIf=\"!isEdit\">\n      <ion-card-header>\n        <ion-card-title>Info</ion-card-title>\n      </ion-card-header>\n      <ion-card-subtitle>\n        {{ loadedGroup.name }}\n      </ion-card-subtitle>\n      <ion-card-content>\n        <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"toggleEditGroup()\"\n          *ngIf=\"amOwner || amManager\">\n          Edit\n        </ion-button>\n      </ion-card-content>\n    </ion-card>\n\n    <div *ngIf=\"isEdit\">\n      <ion-card>\n        <ion-card-header>\n          <ion-card-title>Info</ion-card-title>\n        </ion-card-header>\n        <ion-card-content>\n          <form #editForm=\"ngForm\">\n            <ion-item>\n              <ion-label position=\"floating\">Name</ion-label>\n              <ion-input type=\"text\" name=\"groupName\" #groupName required [ngModel]=\"loadedGroup.name\">\n              </ion-input>\n            </ion-item>\n            <ion-button type=\"button\" color=\"primary\" expand=\"block\" [disabled]=\"!editForm.valid\"\n              (click)=\"onEditGroup(editForm)\">\n              Save\n            </ion-button>\n          </form>\n        </ion-card-content>\n      </ion-card>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onAddMember()\">\n        Add New Member\n      </ion-button>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onAddTask(editForm)\">\n        Add New Task\n      </ion-button>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"toggleEditGroup()\">\n        Cancel\n      </ion-button>\n    </div>\n\n    <hr>\n\n    <div *ngIf=\"!showMembers\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"col-xs-1\">\n            <ion-icon name=\"caret-forward-outline\" (click)=\"toggleShowMembers()\"></ion-icon>\n          </ion-col>\n          <ion-col>\n            <ion-text>Members</ion-text>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n\n    <ion-list *ngIf=\"showMembers\">\n      <ion-list-header>\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"col-xs-1\">\n              <ion-icon name=\"caret-down-outline\" (click)=\"toggleShowMembers()\"></ion-icon>\n            </ion-col>\n            <ion-col>\n              <ion-text>Members</ion-text>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-list-header>\n\n      <div *ngFor=\"let member of loadedGroup.members\">\n        <app-show-member [groupMember]=\"member\" [loadedGroup]=\"loadedGroup\"></app-show-member>\n      </div>\n    </ion-list>\n\n    <div *ngIf=\"!showTasks\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"col-xs-1\">\n            <ion-icon name=\"caret-forward-outline\" (click)=\"toggleShowTasks()\"></ion-icon>\n          </ion-col>\n          <ion-col>\n            <ion-text>Tasks</ion-text>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n\n    <ion-list *ngIf=\"showTasks\">\n      <ion-list-header>\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"col-xs-1\">\n              <ion-icon name=\"caret-down-outline\" (click)=\"toggleShowTasks()\"></ion-icon>\n            </ion-col>\n            <ion-col>\n              <ion-text>Tasks</ion-text>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-list-header>\n\n      <div *ngFor=\"let task of loadedGroup.tasks\">\n        <app-show-task [groupTask]=\"task\" [loadedGroup]=\"loadedGroup\"></app-show-task>\n      </div>\n    </ion-list>\n  </div>\n\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-member/show-member.component.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-member/show-member.component.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n<ion-card *ngIf=\"!isLoading\">\n  <ion-card-header>\n    <ion-card-title>{{ groupMember.name }}</ion-card-title>\n    <ion-card-subtitle>{{ groupMember.email }}</ion-card-subtitle>\n  </ion-card-header>\n\n  <ion-card-content>\n    <ion-grid>\n\n      <div\n        *ngIf=\"(loadedGroup.role == 'Owner' || (loadedGroup.role == 'Manager' && groupMember.role != 'Owner')) && groupMember.identifier != myProfile.identifier; else cannotChangeMemberRole\">\n        <ion-row>\n          <ion-col>\n            <ion-text>{{ groupMember.title }}</ion-text>\n          </ion-col>\n          <ion-col>\n            <ion-select value=\"{{ groupMember.role }}\" #updateMemberRole\n              (ionChange)=\"onUpdateGroupMember(groupMember.identifier, updateMemberRole.value)\">\n              <ion-select-option value=\"Member\">Member</ion-select-option>\n              <ion-select-option value=\"Manager\">Manager</ion-select-option>\n              <ion-select-option value=\"Owner\" *ngIf=\"loadedGroup.role == 'Owner'\">Owner</ion-select-option>\n            </ion-select>\n          </ion-col>\n          <ion-col>\n            <ion-icon name=\"trash-outline\" (click)=\"onDeleteGroupMember()\"></ion-icon>\n          </ion-col>\n        </ion-row>\n      </div>\n\n      <ng-template #cannotChangeMemberRole>\n        <ion-row>\n          <ion-col>\n            <ion-text>{{ groupMember.title }}</ion-text>\n          </ion-col>\n          <ion-col>\n            <ion-text>{{ groupMember.role }}</ion-text>\n          </ion-col>\n        </ion-row>\n      </ng-template>\n\n      <ion-row *ngIf=\"!groupMember.accepted\">\n        <ion-col>\n          <ion-text>\n            Awaiting Group Member Acceptance\n          </ion-text>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n  </ion-card-content>\n</ion-card>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-task/show-task.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-task/show-task.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n<ion-card *ngIf=\"!isLoading\">\n  <ion-card-header>\n    <ion-card-title>{{ groupTask.name }}</ion-card-title>\n    <ion-card-subtitle>Due: {{ groupTask.dueDate.toLocaleDateString() }}</ion-card-subtitle>\n    <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-text>{{ groupTask.description }}</ion-text>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf=\"loadedGroup.role != 'Owner' && loadedGroup.role != 'Manager'; else isOwnerOrManager\">\n          <ion-col>\n            Owner:\n          </ion-col>\n          <ion-col>\n            <ion-text>{{ taskOwner.name }}</ion-text>\n          </ion-col>\n          <ion-col>\n            <ion-text>{{ taskOwner.email }}</ion-text>\n          </ion-col>\n          <ion-col>\n            <ion-text>{{ groupTask.status }}</ion-text>\n          </ion-col>\n        </ion-row>\n        <ng-template #isOwnerOrManager>\n          <ion-row>\n            <ion-col>\n              Owner:\n            </ion-col>\n            <ion-col>\n              <ion-select value=\"{{ taskOwner.identifier }}\" #changeOwner\n                *ngIf=\"memberSelectorOptions.length > 1 && groupTask.status != 'Verfiy' && groupTask.status != 'Complete'; else cannotChangeTaskOwner\"\n                (ionChange)=\"onChangeTaskOwner(changeOwner.value)\">\n                <ion-select-option value=\"{{ option.value}}\" *ngFor=\"let option of memberSelectorOptions\">\n                  {{ option.display }}\n                </ion-select-option>\n              </ion-select>\n              <ng-template #cannotChangeTaskOwner>\n                <ion-col>\n                  <ion-text>{{ taskOwner.name }}</ion-text>\n                </ion-col>\n                <ion-col>\n                  <ion-text>{{ taskOwner.email }}</ion-text>\n                </ion-col>\n              </ng-template>\n            </ion-col>\n            <ion-col>\n              <ion-button type=\"button\" color=\"primary\" expand=\"block\"\n                *ngIf=\"groupTask.status == 'Verify'; else notVerify\" (click)=\"onChangeTaskStatus('Complete')\">\n                Verify\n              </ion-button>\n              <ng-template #notVerify>\n                <ion-text>{{ groupTask.status }}</ion-text>\n              </ng-template>\n            </ion-col>\n            <ion-col size=\"col-xs-1\">\n              <ion-icon name=\"build-outline\" (click)=\"onEditTask()\"></ion-icon>\n            </ion-col>\n            <ion-col size=\"col-xs-1\">\n              <ion-icon name=\"trash-outline\" (click)=\"onDeleteTask()\"></ion-icon>\n            </ion-col>\n          </ion-row>\n        </ng-template>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card-header>\n</ion-card>");

/***/ }),

/***/ "./src/app/groups/group-detail/add-member/add-member.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/groups/group-detail/add-member/add-member.component.scss ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvYWRkLW1lbWJlci9hZGQtbWVtYmVyLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/groups/group-detail/add-member/add-member.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/groups/group-detail/add-member/add-member.component.ts ***!
  \************************************************************************/
/*! exports provided: AddMemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddMemberComponent", function() { return AddMemberComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _groups_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../groups.service */ "./src/app/groups/groups.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");




let AddMemberComponent = class AddMemberComponent {
    constructor(popoverController, alertController, navParams, groupsService) {
        this.popoverController = popoverController;
        this.alertController = alertController;
        this.navParams = navParams;
        this.groupsService = groupsService;
    }
    ngOnInit() {
        this.loadedGroup = this.navParams.get('group');
    }
    onAddNewMember(editForm) {
        const newMemberEmail = editForm.value.newMemberEmail;
        const newMemberRole = editForm.value.newMemberRole;
        console.log(newMemberEmail);
        console.log(newMemberRole);
        if (!newMemberEmail || newMemberEmail.length === 0) {
            console.error('Edit Form Email Not Valid');
            return;
        }
        if (!newMemberRole || newMemberRole.length === 0) {
            console.error('Edit Form Role Not Valid');
            return;
        }
        let duplicate = false;
        this.loadedGroup.members.forEach((member) => {
            if (member.email === newMemberEmail) {
                duplicate = true;
            }
        });
        if (duplicate) {
            this.alertController.create({
                header: 'Member Already in Group',
                buttons: ['OK']
            })
                .then((alert) => {
                this.popoverController.dismiss();
                alert.present();
            });
            return;
        }
        this.groupsService.addGroupMember(this.loadedGroup.identifier, newMemberEmail, newMemberRole)
            .subscribe((response) => {
            console.log(response);
            this.alertController.create({
                header: 'New Member Added',
                buttons: ['OK']
            })
                .then((alert) => {
                this.popoverController.dismiss();
                alert.present();
            });
        });
    }
    onCancel() {
        this.popoverController.dismiss();
    }
};
AddMemberComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
    { type: _groups_service__WEBPACK_IMPORTED_MODULE_1__["GroupsService"] }
];
AddMemberComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-add-member',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./add-member.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/add-member/add-member.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./add-member.component.scss */ "./src/app/groups/group-detail/add-member/add-member.component.scss")).default]
    })
], AddMemberComponent);



/***/ }),

/***/ "./src/app/groups/group-detail/edit-task/edit-task.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/groups/group-detail/edit-task/edit-task.component.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvZWRpdC10YXNrL2VkaXQtdGFzay5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/groups/group-detail/edit-task/edit-task.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/groups/group-detail/edit-task/edit-task.component.ts ***!
  \**********************************************************************/
/*! exports provided: EditTaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditTaskComponent", function() { return EditTaskComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _groups_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../groups.service */ "./src/app/groups/groups.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");




let EditTaskComponent = class EditTaskComponent {
    constructor(groupsService, popoverController, alertController, navParams) {
        this.groupsService = groupsService;
        this.popoverController = popoverController;
        this.alertController = alertController;
        this.navParams = navParams;
        this.dueDatePickerStart = new Date(Date.now()).toISOString();
        this.dueDatePickerStop = '' + (new Date(Date.now()).getFullYear() + 5);
        this.isLoading = true;
    }
    ngOnInit() {
        this.loadedGroup = this.navParams.get('group');
        this.groupTask = this.navParams.get('task');
        console.log(this.groupTask);
        this.memberSelectorOptions = this.loadedGroup.getMemberSelectorOptions();
        this.isLoading = false;
    }
    onAddTask(newTaskForm) {
        if (!newTaskForm.valid) {
            console.log('new task for not valid');
            return;
        }
        this.isLoading = true;
        const name = newTaskForm.value.name;
        const description = newTaskForm.value.description;
        const dueDate = newTaskForm.value.dueDate;
        let owner = newTaskForm.value.owner;
        if (!owner) {
            owner = this.memberSelectorOptions[0].value;
        }
        if (this.groupTask) {
            this.groupsService.updateTask(this.loadedGroup.identifier, this.groupTask.identifier, name, description, new Date(dueDate), owner, false)
                .subscribe((addTaskResponse) => {
                console.log(addTaskResponse);
                this.alertController.create({
                    header: 'Task Updated',
                    buttons: ['OK']
                })
                    .then((alert) => {
                    this.popoverController.dismiss();
                    alert.present();
                });
            });
        }
        else {
            this.groupsService.addTask(this.loadedGroup.identifier, name, description, new Date(dueDate), owner, false)
                .subscribe((addTaskResponse) => {
                console.log(addTaskResponse);
                this.alertController.create({
                    header: 'Task Created',
                    buttons: ['OK']
                })
                    .then((alert) => {
                    this.popoverController.dismiss();
                    alert.present();
                });
            });
        }
    }
    onCancel() {
        this.popoverController.dismiss();
    }
};
EditTaskComponent.ctorParameters = () => [
    { type: _groups_service__WEBPACK_IMPORTED_MODULE_1__["GroupsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] }
];
EditTaskComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-add-task',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./edit-task.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/edit-task/edit-task.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./edit-task.component.scss */ "./src/app/groups/group-detail/edit-task/edit-task.component.scss")).default]
    })
], EditTaskComponent);



/***/ }),

/***/ "./src/app/groups/group-detail/group-detail-routing.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/groups/group-detail/group-detail-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: GroupDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupDetailPageRoutingModule", function() { return GroupDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/auth/auth.guard */ "./src/app/auth/auth.guard.ts");
/* harmony import */ var src_app_profile_profile_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/profile/profile.guard */ "./src/app/profile/profile.guard.ts");
/* harmony import */ var _group_detail_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./group-detail.page */ "./src/app/groups/group-detail/group-detail.page.ts");






const routes = [
    {
        path: ':groupIdentifier',
        component: _group_detail_page__WEBPACK_IMPORTED_MODULE_5__["GroupDetailPage"],
        canLoad: [src_app_auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"], src_app_profile_profile_guard__WEBPACK_IMPORTED_MODULE_4__["ProfileGuard"]]
    }
];
let GroupDetailPageRoutingModule = class GroupDetailPageRoutingModule {
};
GroupDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GroupDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/groups/group-detail/group-detail.module.ts":
/*!************************************************************!*\
  !*** ./src/app/groups/group-detail/group-detail.module.ts ***!
  \************************************************************/
/*! exports provided: GroupDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupDetailPageModule", function() { return GroupDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _group_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./group-detail-routing.module */ "./src/app/groups/group-detail/group-detail-routing.module.ts");
/* harmony import */ var _group_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./group-detail.page */ "./src/app/groups/group-detail/group-detail.page.ts");
/* harmony import */ var _show_member_show_member_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./show-member/show-member.component */ "./src/app/groups/group-detail/show-member/show-member.component.ts");
/* harmony import */ var _add_member_add_member_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add-member/add-member.component */ "./src/app/groups/group-detail/add-member/add-member.component.ts");
/* harmony import */ var _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./edit-task/edit-task.component */ "./src/app/groups/group-detail/edit-task/edit-task.component.ts");
/* harmony import */ var _show_task_show_task_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./show-task/show-task.component */ "./src/app/groups/group-detail/show-task/show-task.component.ts");











let GroupDetailPageModule = class GroupDetailPageModule {
};
GroupDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _group_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["GroupDetailPageRoutingModule"]
        ],
        declarations: [_group_detail_page__WEBPACK_IMPORTED_MODULE_6__["GroupDetailPage"], _add_member_add_member_component__WEBPACK_IMPORTED_MODULE_8__["AddMemberComponent"], _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_9__["EditTaskComponent"], _show_member_show_member_component__WEBPACK_IMPORTED_MODULE_7__["ShowMemberComponent"], _show_task_show_task_component__WEBPACK_IMPORTED_MODULE_10__["ShowTaskComponent"]]
    })
], GroupDetailPageModule);



/***/ }),

/***/ "./src/app/groups/group-detail/group-detail.page.scss":
/*!************************************************************!*\
  !*** ./src/app/groups/group-detail/group-detail.page.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvZ3JvdXAtZGV0YWlsLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/groups/group-detail/group-detail.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/groups/group-detail/group-detail.page.ts ***!
  \**********************************************************/
/*! exports provided: GroupDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupDetailPage", function() { return GroupDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../users/user.service */ "./src/app/users/user.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _groups_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../groups.service */ "./src/app/groups/groups.service.ts");
/* harmony import */ var _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-task/edit-task.component */ "./src/app/groups/group-detail/edit-task/edit-task.component.ts");
/* harmony import */ var _add_member_add_member_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./add-member/add-member.component */ "./src/app/groups/group-detail/add-member/add-member.component.ts");








let GroupDetailPage = class GroupDetailPage {
    constructor(groupsService, userService, activatedRoute, navController, alertController, popoverController, gestureController, elementRef) {
        this.groupsService = groupsService;
        this.userService = userService;
        this.activatedRoute = activatedRoute;
        this.navController = navController;
        this.alertController = alertController;
        this.popoverController = popoverController;
        this.gestureController = gestureController;
        this.elementRef = elementRef;
        this.amOwner = false;
        this.amManager = false;
        this.isEdit = false;
        this.showMembers = false;
        this.showTasks = false;
        this.ownerCount = 0;
    }
    ngOnInit() {
        this.activatedRoute.paramMap.subscribe((paramMap) => {
            if (!paramMap.has('groupIdentifier')) {
                this.navController.navigateBack('/groups');
            }
            this.isLoading = true;
            this.groupIdentifier = paramMap.get('groupIdentifier');
            console.log(this.groupIdentifier);
            this.myProfileSubscription = this.userService.getMyProfile()
                .subscribe((myProfile) => {
                console.log(myProfile.identifier);
                this.myProfile = myProfile;
                this.groupsService.getGroupByIdentifier(this.groupIdentifier)
                    .subscribe((groupModel) => {
                    console.log(groupModel);
                    this.loadedGroup = groupModel;
                    if (this.loadedGroup.role === 'Owner') {
                        this.amOwner = true;
                    }
                    else if (this.loadedGroup.role === 'Manager') {
                        this.amManager = true;
                    }
                    this.isLoading = false;
                }, (errorResponse) => {
                    console.log(errorResponse);
                    this.navController.navigateBack('/groups');
                });
            });
        });
        const gestureConfig = {
            gestureName: 'refreshGesture',
            el: this.elementRef.nativeElement,
            direction: 'y',
            blurOnStart: true,
            threshold: 500,
            canStart: () => {
                return !this.isEdit && !this.isLoading;
            },
            onEnd: (event) => {
                console.log(event);
                this.isLoading = true;
                this.groupsService.getGroupByIdentifier(this.groupIdentifier)
                    .subscribe((groupModel) => {
                    console.log(groupModel);
                    this.loadedGroup = groupModel;
                    if (this.loadedGroup.role === 'Owner') {
                        this.amOwner = true;
                    }
                    else if (this.loadedGroup.role === 'Manager') {
                        this.amManager = true;
                    }
                    this.isLoading = false;
                }, (errorResponse) => {
                    console.log(errorResponse);
                    this.navController.navigateBack('/groups');
                });
            }
        };
        this.gestureController.create(gestureConfig, true)
            .enable();
    }
    ngOnDestroy() {
        if (this.groupsSubscription) {
            this.groupsSubscription.unsubscribe();
        }
        if (this.myProfileSubscription) {
            this.myProfileSubscription.unsubscribe();
        }
    }
    onDeleteGroup() {
        console.log('onDeleteGroup');
        this.alertController.create({
            header: 'Delete Group',
            message: 'Deleting the Group will also delete all tasks.',
            buttons: [
                {
                    text: 'Delete',
                    handler: (_) => {
                        this.groupsSubscription.unsubscribe();
                        this.groupsService.deleteGroup(this.loadedGroup.identifier)
                            .subscribe((response) => {
                            console.log(response);
                            this.navController.navigateBack('/groups');
                        }, (errorResponse) => {
                            console.log(errorResponse);
                            this.navController.navigateBack('/groups');
                        });
                    }
                },
                'Cancel'
            ]
        })
            .then((alert) => {
            alert.present();
        });
    }
    toggleEditGroup() {
        this.isEdit = !this.isEdit;
    }
    onEditGroup(editForm) {
        const groupName = editForm.value.groupName;
        console.log(groupName);
        if (!groupName || groupName.length === 0) {
            console.error('Edit Form Group Name Not Valid');
            return;
        }
        this.groupsService.editGroup(this.loadedGroup.identifier, groupName)
            .subscribe((editResponse) => {
            console.log(editResponse);
            this.loadedGroup = editResponse;
            this.isEdit = false;
        }, (errorResponse) => {
            console.log(errorResponse);
            this.isEdit = false;
            this.navController.navigateBack('/groups');
        });
        editForm.reset();
    }
    onRefreshGroup() {
        this.isLoading = true;
        this.groupsService.getGroupByIdentifier(this.groupIdentifier)
            .subscribe((groupModel) => {
            console.log(groupModel);
            this.loadedGroup = groupModel;
            if (this.loadedGroup.role === 'Owner') {
                this.amOwner = true;
            }
            else if (this.loadedGroup.role === 'Manager') {
                this.amManager = true;
            }
            this.isLoading = false;
        }, (errorResponse) => {
            console.log(errorResponse);
            this.navController.navigateBack('/groups');
        });
    }
    onLeaveGroup() {
        console.log('onLeaveGroup');
        this.alertController.create({
            header: 'Leave Group?',
            buttons: [
                {
                    text: 'Leave',
                    handler: (_) => {
                        this.isLoading = true;
                        this.groupsSubscription.unsubscribe();
                        this.groupsService.deleteGroupsMember(this.loadedGroup.identifier, this.myProfile.identifier)
                            .subscribe((deleteResponse) => {
                            console.log(deleteResponse);
                            this.isLoading = false;
                            this.navController.navigateBack('/groups');
                        }, (errorResponse) => {
                            console.log(errorResponse);
                            this.isLoading = false;
                            this.navController.navigateBack('/groups');
                        });
                    }
                },
                'Cancel'
            ]
        })
            .then((alert) => {
            alert.present();
        });
    }
    onAddMember() {
        this.popoverController.create({
            component: _add_member_add_member_component__WEBPACK_IMPORTED_MODULE_7__["AddMemberComponent"],
            componentProps: { group: this.loadedGroup }
        })
            .then((popover) => {
            popover.present();
        });
    }
    onAddTask() {
        this.popoverController.create({
            component: _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_6__["EditTaskComponent"],
            componentProps: { group: this.loadedGroup }
        })
            .then((popover) => {
            popover.present();
        });
    }
    toggleShowMembers() {
        this.showMembers = !this.showMembers;
    }
    toggleShowTasks() {
        this.showTasks = !this.showTasks;
    }
};
GroupDetailPage.ctorParameters = () => [
    { type: _groups_service__WEBPACK_IMPORTED_MODULE_5__["GroupsService"] },
    { type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["GestureController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"] }
];
GroupDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-group-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./group-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/group-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./group-detail.page.scss */ "./src/app/groups/group-detail/group-detail.page.scss")).default]
    })
], GroupDetailPage);



/***/ }),

/***/ "./src/app/groups/group-detail/show-member/show-member.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/groups/group-detail/show-member/show-member.component.scss ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvc2hvdy1tZW1iZXIvc2hvdy1tZW1iZXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/groups/group-detail/show-member/show-member.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/groups/group-detail/show-member/show-member.component.ts ***!
  \**************************************************************************/
/*! exports provided: ShowMemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowMemberComponent", function() { return ShowMemberComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../users/user.service */ "./src/app/users/user.service.ts");
/* harmony import */ var _groups_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../groups.service */ "./src/app/groups/groups.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");





let ShowMemberComponent = class ShowMemberComponent {
    constructor(groupsService, alertController, userService) {
        this.groupsService = groupsService;
        this.alertController = alertController;
        this.userService = userService;
        this.isLoading = true;
    }
    ngOnInit() {
        this.myProfileSubscription = this.userService.getMyProfile()
            .subscribe((myProfile) => {
            this.myProfile = myProfile;
            console.log(this.myProfile);
            this.isLoading = false;
        });
        console.log(this.groupMember);
        console.log(this.loadedGroup);
    }
    ngOnDestroy() {
        if (this.myProfileSubscription) {
            this.myProfileSubscription.unsubscribe();
        }
    }
    onUpdateGroupMember(userIdentifier, role) {
        console.log(userIdentifier);
        console.log(role);
        this.groupsService.updateGroupsMember(this.loadedGroup.identifier, userIdentifier, role)
            .subscribe((updateResponse) => {
            console.log(updateResponse);
        });
    }
    onDeleteGroupMember() {
        console.log(this.groupMember.identifier);
        this.alertController.create({
            header: 'Remove Member from the Group?',
            buttons: [
                {
                    text: 'Remove',
                    handler: (_) => {
                        this.isLoading = true;
                        this.groupsService.deleteGroupsMember(this.loadedGroup.identifier, this.groupMember.identifier)
                            .subscribe((deleteResponse) => {
                            console.log(deleteResponse);
                            this.alertController.create({
                                header: 'Member Removed from Group',
                                buttons: ['OK']
                            })
                                .then((alert) => {
                                this.isLoading = false;
                                alert.present();
                            });
                        });
                    }
                },
                'Cancel'
            ]
        })
            .then((alert) => {
            alert.present();
        });
    }
};
ShowMemberComponent.ctorParameters = () => [
    { type: _groups_service__WEBPACK_IMPORTED_MODULE_2__["GroupsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"] }
];
ShowMemberComponent.propDecorators = {
    groupMember: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"] }],
    loadedGroup: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"] }]
};
ShowMemberComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-show-member',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./show-member.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-member/show-member.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./show-member.component.scss */ "./src/app/groups/group-detail/show-member/show-member.component.scss")).default]
    })
], ShowMemberComponent);



/***/ }),

/***/ "./src/app/groups/group-detail/show-task/show-task.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/groups/group-detail/show-task/show-task.component.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvc2hvdy10YXNrL3Nob3ctdGFzay5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/groups/group-detail/show-task/show-task.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/groups/group-detail/show-task/show-task.component.ts ***!
  \**********************************************************************/
/*! exports provided: ShowTaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowTaskComponent", function() { return ShowTaskComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _groups_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../groups.service */ "./src/app/groups/groups.service.ts");
/* harmony import */ var _users_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../users/user.service */ "./src/app/users/user.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../edit-task/edit-task.component */ "./src/app/groups/group-detail/edit-task/edit-task.component.ts");






let ShowTaskComponent = class ShowTaskComponent {
    constructor(groupsService, userService, popoverController, alertController) {
        this.groupsService = groupsService;
        this.userService = userService;
        this.popoverController = popoverController;
        this.alertController = alertController;
        this.isLoading = true;
    }
    ngOnInit() {
        this.myProfileSubscription = this.userService.getMyProfile()
            .subscribe((myProfile) => {
            this.myProfile = myProfile;
            console.log(this.myProfile);
            this.isLoading = false;
        });
        this.taskOwner = this.loadedGroup.getMember(this.groupTask.ownerIdentifier);
        this.memberSelectorOptions = this.loadedGroup.getMemberSelectorOptions();
        console.log(this.groupTask);
        console.log(this.taskOwner);
        console.log(this.loadedGroup);
    }
    ngOnDestroy() {
        if (this.myProfileSubscription) {
            this.myProfileSubscription.unsubscribe();
        }
    }
    onChangeTaskOwner(newOwnerIdentifier) {
        if (newOwnerIdentifier === this.groupTask.ownerIdentifier) {
            return;
        }
        this.isLoading = true;
        this.groupsService.changeTaskOwner(this.loadedGroup.identifier, this.groupTask.identifier, newOwnerIdentifier)
            .subscribe((response) => {
            console.log(response);
            this.isLoading = false;
        });
    }
    onChangeTaskStatus(newStatus) {
        if (newStatus === this.groupTask.status) {
            return;
        }
        this.isLoading = true;
        this.groupsService.changeTaskStatus(this.loadedGroup.identifier, this.groupTask.identifier, newStatus)
            .subscribe((response) => {
            console.log(response);
            this.isLoading = false;
        });
    }
    onEditTask() {
        this.popoverController.create({
            component: _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_5__["EditTaskComponent"],
            componentProps: { group: this.loadedGroup, task: this.groupTask }
        })
            .then((popover) => {
            popover.present();
        });
    }
    onDeleteTask() {
        this.alertController.create({
            header: 'Delete Task?',
            buttons: [
                {
                    text: 'Delete',
                    handler: (_) => {
                        this.isLoading = true;
                        this.groupsService.deleteTask(this.loadedGroup.identifier, this.groupTask.identifier)
                            .subscribe((deleteResult) => {
                            console.log('delete');
                            this.isLoading = false;
                        });
                    }
                },
                'Cancel'
            ]
        })
            .then((alert) => {
            alert.present();
        });
    }
};
ShowTaskComponent.ctorParameters = () => [
    { type: _groups_service__WEBPACK_IMPORTED_MODULE_2__["GroupsService"] },
    { type: _users_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"] }
];
ShowTaskComponent.propDecorators = {
    groupTask: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"] }],
    loadedGroup: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"] }]
};
ShowTaskComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-show-task',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./show-task.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-task/show-task.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./show-task.component.scss */ "./src/app/groups/group-detail/show-task/show-task.component.scss")).default]
    })
], ShowTaskComponent);



/***/ })

}]);
//# sourceMappingURL=group-detail-group-detail-module-es2015.js.map