(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    "./$$_lazy_route_resource lazy recursive":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function $$_lazy_route_resourceLazyRecursive(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
      /***/
    },

    /***/
    "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
      \*****************************************************************************************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesIonicCoreDistEsmLazyRecursiveEntryJs$IncludeEntryJs$ExcludeSystemEntryJs$(module, exports, __webpack_require__) {
      var map = {
        "./ion-action-sheet.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-action-sheet.entry.js", "common", 0],
        "./ion-alert.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-alert.entry.js", "common", 1],
        "./ion-app_8.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-app_8.entry.js", "common", 2],
        "./ion-avatar_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-avatar_3.entry.js", "common", 3],
        "./ion-back-button.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-back-button.entry.js", "common", 4],
        "./ion-backdrop.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-backdrop.entry.js", 5],
        "./ion-button_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-button_2.entry.js", "common", 6],
        "./ion-card_5.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-card_5.entry.js", "common", 7],
        "./ion-checkbox.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-checkbox.entry.js", "common", 8],
        "./ion-chip.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-chip.entry.js", "common", 9],
        "./ion-col_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js", 10],
        "./ion-datetime_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-datetime_3.entry.js", "common", 11],
        "./ion-fab_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-fab_3.entry.js", "common", 12],
        "./ion-img.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-img.entry.js", 13],
        "./ion-infinite-scroll_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2.entry.js", 14],
        "./ion-input.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-input.entry.js", "common", 15],
        "./ion-item-option_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item-option_3.entry.js", "common", 16],
        "./ion-item_8.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item_8.entry.js", "common", 17],
        "./ion-loading.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-loading.entry.js", "common", 18],
        "./ion-menu_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-menu_3.entry.js", "common", 19],
        "./ion-modal.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-modal.entry.js", "common", 20],
        "./ion-nav_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-nav_2.entry.js", "common", 21],
        "./ion-popover.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-popover.entry.js", "common", 22],
        "./ion-progress-bar.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-progress-bar.entry.js", "common", 23],
        "./ion-radio_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-radio_2.entry.js", "common", 24],
        "./ion-range.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-range.entry.js", "common", 25],
        "./ion-refresher_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-refresher_2.entry.js", "common", 26],
        "./ion-reorder_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-reorder_2.entry.js", "common", 27],
        "./ion-ripple-effect.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js", 28],
        "./ion-route_4.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js", "common", 29],
        "./ion-searchbar.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-searchbar.entry.js", "common", 30],
        "./ion-segment_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-segment_2.entry.js", "common", 31],
        "./ion-select_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-select_3.entry.js", "common", 32],
        "./ion-slide_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-slide_2.entry.js", 33],
        "./ion-spinner.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js", "common", 34],
        "./ion-split-pane.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-split-pane.entry.js", 35],
        "./ion-tab-bar_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab-bar_2.entry.js", "common", 36],
        "./ion-tab_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js", "common", 37],
        "./ion-text.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-text.entry.js", "common", 38],
        "./ion-textarea.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-textarea.entry.js", "common", 39],
        "./ion-toast.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toast.entry.js", "common", 40],
        "./ion-toggle.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toggle.entry.js", "common", 41],
        "./ion-virtual-scroll.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js", 42]
      };

      function webpackAsyncContext(req) {
        if (!__webpack_require__.o(map, req)) {
          return Promise.resolve().then(function () {
            var e = new Error("Cannot find module '" + req + "'");
            e.code = 'MODULE_NOT_FOUND';
            throw e;
          });
        }

        var ids = map[req],
            id = ids[0];
        return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
          return __webpack_require__(id);
        });
      }

      webpackAsyncContext.keys = function webpackAsyncContextKeys() {
        return Object.keys(map);
      };

      webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
      module.exports = webpackAsyncContext;
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
    /*!**************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
      \**************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-app>\n  <ion-menu side=\"start\" contentId=\"main-content\" menuId=\"menu1\">\n    <ion-header>\n      <ion-toolbar>\n        <ion-title>\n          Task Manager\n        </ion-title>\n      </ion-toolbar>\n    </ion-header>\n\n    <ion-content>\n      <ion-menu-toggle>\n        <ion-list>\n          <ion-item lines=\"none\" routerLink=\"/profile\">\n            <ion-icon name=\"business\" slot=\"start\"></ion-icon>\n            <ion-label>Profile</ion-label>\n          </ion-item>\n          <ion-item lines=\"none\" routerLink=\"/groups\">\n            <ion-icon name=\"checkbox-outline\" slot=\"start\"></ion-icon>\n            <ion-label>Groups</ion-label>\n          </ion-item>\n          <ion-item lines=\"none\" routerLink=\"/tasks\">\n            <ion-icon name=\"checkbox-outline\" slot=\"start\"></ion-icon>\n            <ion-label>Tasks</ion-label>\n          </ion-item>\n          <ion-item lines=\"none\" (click)=\"onLogout()\" button>\n            <ion-icon name=\"exit\" slot=\"start\"></ion-icon>\n            <ion-label>Logout</ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-menu-toggle>\n    </ion-content>\n  </ion-menu>\n\n  <ion-router-outlet id=\"main-content\"></ion-router-outlet>\n</ion-app>";
      /***/
    },

    /***/
    "./src/app/app-routing.module.ts":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _profile_profile_guard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./profile/profile.guard */
      "./src/app/profile/profile.guard.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./auth/auth.guard */
      "./src/app/auth/auth.guard.ts");

      var routes = [{
        path: '',
        redirectTo: 'tasks',
        pathMatch: 'full',
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"], _profile_profile_guard__WEBPACK_IMPORTED_MODULE_1__["ProfileGuard"]]
      }, {
        path: 'auth',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | auth-auth-module */
          "auth-auth-module").then(__webpack_require__.bind(null,
          /*! ./auth/auth.module */
          "./src/app/auth/auth.module.ts")).then(function (m) {
            return m.AuthPageModule;
          });
        }
      }, {
        path: 'profile',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | profile-profile-module */
          "profile-profile-module").then(__webpack_require__.bind(null,
          /*! ./profile/profile.module */
          "./src/app/profile/profile.module.ts")).then(function (m) {
            return m.ProfilePageModule;
          });
        },
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]]
      }, {
        path: 'tasks',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | tasks-tasks-module */
          "tasks-tasks-module").then(__webpack_require__.bind(null,
          /*! ./tasks/tasks.module */
          "./src/app/tasks/tasks.module.ts")).then(function (m) {
            return m.TasksPageModule;
          });
        },
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"], _profile_profile_guard__WEBPACK_IMPORTED_MODULE_1__["ProfileGuard"]]
      }, {
        path: 'groups',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | groups-groups-module */
          "groups-groups-module").then(__webpack_require__.bind(null,
          /*! ./groups/groups.module */
          "./src/app/groups/groups.module.ts")).then(function (m) {
            return m.GroupsPageModule;
          });
        },
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"], _profile_profile_guard__WEBPACK_IMPORTED_MODULE_1__["ProfileGuard"]]
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(routes, {
          preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_3__["PreloadAllModules"]
        })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]]
      })], AppRoutingModule);
      /***/
    },

    /***/
    "./src/app/app.component.scss":
    /*!************************************!*\
      !*** ./src/app/app.component.scss ***!
      \************************************/

    /*! exports provided: default */

    /***/
    function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/app.component.ts":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/splash-screen/ngx */
      "./node_modules/@ionic-native/splash-screen/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "./node_modules/@ionic-native/status-bar/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./auth/auth.service */
      "./src/app/auth/auth.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var AppComponent = /*#__PURE__*/function () {
        function AppComponent(platform, splashScreen, statusBar, authService, router) {
          _classCallCheck(this, AppComponent);

          this.platform = platform;
          this.splashScreen = splashScreen;
          this.statusBar = statusBar;
          this.authService = authService;
          this.router = router;
          this.initializeApp();
        }

        _createClass(AppComponent, [{
          key: "initializeApp",
          value: function initializeApp() {
            var _this = this;

            this.platform.ready().then(function () {
              _this.statusBar.styleDefault();

              _this.splashScreen.hide();
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this2 = this;

            this.authService.authStatus.subscribe(function (authenticated) {
              if (authenticated) {
                console.log('app page authenticated');

                _this2.router.navigate(['/']);
              } else {
                console.log('app page not authenticated');

                _this2.router.navigate(['/auth']);

                if (_this2.fcmMessageSubscription) {
                  _this2.fcmMessageSubscription.unsubscribe();
                }
              }
            });
            this.authService.initAuth();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.fcmMessageSubscription) {
              this.fcmMessageSubscription.unsubscribe();
            }
          }
        }, {
          key: "onLogout",
          value: function onLogout() {
            console.log('onLogout');
            this.authService.logout();
          }
        }]);

        return AppComponent;
      }();

      AppComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
        }, {
          type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"]
        }, {
          type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"]
        }, {
          type: _auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }];
      };

      AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./app.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./app.component.scss */
        "./src/app/app.component.scss"))["default"]]
      })], AppComponent);
      /***/
    },

    /***/
    "./src/app/app.module.ts":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/splash-screen/ngx */
      "./node_modules/@ionic-native/splash-screen/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "./node_modules/@ionic-native/status-bar/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./app.component */
      "./src/app/app.component.ts");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./app-routing.module */
      "./src/app/app-routing.module.ts");
      /* harmony import */


      var _auth_auth_interceptor_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./auth/auth-interceptor.service */
      "./src/app/auth/auth-interceptor.service.ts");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! src/environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var angularfire2__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! angularfire2 */
      "./node_modules/angularfire2/index.js");
      /* harmony import */


      var angularfire2__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(angularfire2__WEBPACK_IMPORTED_MODULE_12__);
      /* harmony import */


      var angularfire2_messaging__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! angularfire2/messaging */
      "./node_modules/angularfire2/messaging/index.js");
      /* harmony import */


      var angularfire2_messaging__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(angularfire2_messaging__WEBPACK_IMPORTED_MODULE_13__);

      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
        entryComponents: [],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClientModule"], angularfire2__WEBPACK_IMPORTED_MODULE_12__["AngularFireModule"].initializeApp({
          apiKey: src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].FIREBASE_CONFIG.apiKey,
          authDomain: src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].FIREBASE_CONFIG.authDomain,
          databaseURL: src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].FIREBASE_CONFIG.databaseURL,
          projectId: src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].FIREBASE_CONFIG.projectId,
          storageBucket: src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].FIREBASE_CONFIG.storageBucket,
          messagingSenderId: src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].FIREBASE_CONFIG.messagingSenderId,
          appId: src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].FIREBASE_CONFIG.appId
        }), angularfire2_messaging__WEBPACK_IMPORTED_MODULE_13__["AngularFireMessagingModule"]],
        providers: [_ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"], _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"], {
          provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HTTP_INTERCEPTORS"],
          useClass: _auth_auth_interceptor_service__WEBPACK_IMPORTED_MODULE_9__["AuthInterceptorService"],
          multi: true
        }, {
          provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"],
          useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"]
        }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
      })], AppModule);
      /***/
    },

    /***/
    "./src/app/auth/auth-interceptor.service.ts":
    /*!**************************************************!*\
      !*** ./src/app/auth/auth-interceptor.service.ts ***!
      \**************************************************/

    /*! exports provided: AuthInterceptorService */

    /***/
    function srcAppAuthAuthInterceptorServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthInterceptorService", function () {
        return AuthInterceptorService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./auth.service */
      "./src/app/auth/auth.service.ts");

      var AuthInterceptorService = /*#__PURE__*/function () {
        function AuthInterceptorService(authService) {
          _classCallCheck(this, AuthInterceptorService);

          this.authService = authService;
        }

        _createClass(AuthInterceptorService, [{
          key: "intercept",
          value: function intercept(request, next) {
            return this.authService.getIdToken().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function (token) {
              request = request.clone({
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                  Authorization: token
                })
              });
              return next.handle(request);
            }));
          }
        }]);

        return AuthInterceptorService;
      }();

      AuthInterceptorService.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }];
      };

      AuthInterceptorService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], AuthInterceptorService);
      /***/
    },

    /***/
    "./src/app/auth/auth.guard.ts":
    /*!************************************!*\
      !*** ./src/app/auth/auth.guard.ts ***!
      \************************************/

    /*! exports provided: AuthGuard */

    /***/
    function srcAppAuthAuthGuardTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthGuard", function () {
        return AuthGuard;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./auth.service */
      "./src/app/auth/auth.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var AuthGuard = /*#__PURE__*/function () {
        function AuthGuard(authService, router) {
          _classCallCheck(this, AuthGuard);

          this.authService = authService;
          this.router = router;
        }

        _createClass(AuthGuard, [{
          key: "canActivate",
          value: function canActivate(route, state) {
            var _this3 = this;

            var isAuthenticated = this.authService.userIsAuthenticated();
            console.log(isAuthenticated);
            return isAuthenticated.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (userIsAuthenticated) {
              console.log(userIsAuthenticated);

              if (!userIsAuthenticated) {
                _this3.router.navigateByUrl('/auth');
              }
            }));
          }
        }]);

        return AuthGuard;
      }();

      AuthGuard.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      AuthGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], AuthGuard);
      /***/
    },

    /***/
    "./src/app/auth/auth.service.ts":
    /*!**************************************!*\
      !*** ./src/app/auth/auth.service.ts ***!
      \**************************************/

    /*! exports provided: AuthService */

    /***/
    function srcAppAuthAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _firebase_messaging_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./firebase-messaging.service */
      "./src/app/auth/firebase-messaging.service.ts");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! amazon-cognito-identity-js */
      "./node_modules/amazon-cognito-identity-js/es/index.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var _tasks_tasks_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../tasks/tasks.service */
      "./src/app/tasks/tasks.service.ts");
      /* harmony import */


      var _groups_groups_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../groups/groups.service */
      "./src/app/groups/groups.service.ts");

      var userPool = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["CognitoUserPool"](_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].COGNITO_POOL_DATA);

      var AuthService = /*#__PURE__*/function () {
        function AuthService(userService, groupsService, tasksService, firebaseMessagingService) {
          _classCallCheck(this, AuthService);

          this.userService = userService;
          this.groupsService = groupsService;
          this.tasksService = tasksService;
          this.firebaseMessagingService = firebaseMessagingService;
          this.confirmCodeRequired = new rxjs__WEBPACK_IMPORTED_MODULE_6__["BehaviorSubject"](null);
          this.authStatus = new rxjs__WEBPACK_IMPORTED_MODULE_6__["BehaviorSubject"](false);
        }

        _createClass(AuthService, [{
          key: "getAuthenticatedUser",
          value: function getAuthenticatedUser() {
            return userPool.getCurrentUser();
          }
        }, {
          key: "getIdToken",
          value: function getIdToken() {
            var user = this.getAuthenticatedUser();

            if (!user) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["of"])(null);
            }

            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(new Promise(function (resolve, reject) {
              user.getSession(function (err, session) {
                if (err) {
                  reject(err);
                } else if (!session.isValid()) {
                  resolve(null);
                } else {
                  resolve(session.getIdToken().getJwtToken());
                }
              });
            }));
          }
        }, {
          key: "getProfileAttributes",
          value: function getProfileAttributes() {
            var user = this.getAuthenticatedUser();
            return new Promise(function (resolve, reject) {
              user.getSession(function (err, session) {
                if (err) {
                  reject(err);
                } else if (!session.isValid()) {
                  resolve(null);
                } else {
                  console.log(session.getIdToken());
                  resolve(session.getIdToken().payload);
                }
              });
            });
          }
        }, {
          key: "userIsAuthenticated",
          value: function userIsAuthenticated() {
            var user = this.getAuthenticatedUser();
            var obs = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"](function (observer) {
              if (!user) {
                console.log('false');
                observer.next(false);
              } else {
                user.getSession(function (error, session) {
                  if (error) {
                    console.log(error);
                  } else {
                    if (session.isValid()) {
                      console.log('true');
                      observer.next(true);
                    } else {
                      console.log('false');
                      observer.next(false);
                    }
                  }
                });
              }

              observer.complete();
            });
            return obs;
          }
        }, {
          key: "initAuth",
          value: function initAuth() {
            var _this4 = this;

            this.userIsAuthenticated().subscribe(function (auth) {
              return _this4.authStatus.next(auth);
            });
          }
        }, {
          key: "signUp",
          value: function signUp(username, email, password) {
            var _this5 = this;

            var cognitoUserAttributeList = [];
            var emailAttribute = {
              Name: 'email',
              Value: email
            };
            cognitoUserAttributeList.push(new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["CognitoUserAttribute"](emailAttribute));
            return new Promise(function (resolve, reject) {
              userPool.signUp(username, password, cognitoUserAttributeList, null, function (error, result) {
                if (error) {
                  console.log(error);
                  reject(error.message);
                } else {
                  _this5.confirmCodeRequired.next(username);

                  resolve('Signup Success');
                }
              });
            });
          }
        }, {
          key: "confirmUser",
          value: function confirmUser(username, code) {
            var _this6 = this;

            var userData = {
              Username: username,
              Pool: userPool
            };
            var cognitoUser = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["CognitoUser"](userData);
            return new Promise(function (resolve, reject) {
              cognitoUser.confirmRegistration(code, true, function (error, result) {
                if (error) {
                  console.error(error);
                  reject(error.message);
                } else {
                  _this6.confirmCodeRequired.next(null);

                  resolve('Confirmed');
                }
              });
            });
          }
        }, {
          key: "resendConfirmationCode",
          value: function resendConfirmationCode(username) {
            var userData = {
              Username: username,
              Pool: userPool
            };
            var cognitoUser = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["CognitoUser"](userData);
            return new Promise(function (resolve, reject) {
              cognitoUser.resendConfirmationCode(function (error, result) {
                if (error) {
                  console.error(error);
                  reject(error.message);
                } else {
                  resolve('Code Resent');
                }
              });
            });
          }
        }, {
          key: "signIn",
          value: function signIn(username, password) {
            var _this7 = this;

            var authData = {
              Username: username,
              Password: password
            };
            var authenticationDetails = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["AuthenticationDetails"](authData);
            var userData = {
              Username: username,
              Pool: userPool
            };
            var cognitoUser = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["CognitoUser"](userData);
            var that = this;
            return new Promise(function (resolve, reject) {
              cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: function onSuccess(result) {
                  _this7.authStatus.next(true);

                  resolve('SignIn Success');
                },
                onFailure: function onFailure(error) {
                  if (error.code === 'UserNotConfirmedException') {
                    _this7.confirmCodeRequired.next(username);

                    resolve('SignIn Success Code Required');
                  }

                  console.log(error);
                  reject(error.message);
                }
              });
            });
          }
        }, {
          key: "deleteUser",
          value: function deleteUser(username) {
            console.log(username);
            var userData = {
              Username: username,
              Pool: userPool
            };
            var cognitoUser = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["CognitoUser"](userData);
            return new Promise(function (sessionResolve, sessionReject) {
              cognitoUser.getSession(function (sessionError, session) {
                if (sessionError) {
                  console.error(sessionError);
                  sessionReject('Failed to Get Session for User');
                } else if (session.isValid()) {
                  sessionResolve('Got Session');
                }
              });
            }).then(function (_) {
              return new Promise(function (deleteResolve, deleteReject) {
                cognitoUser.deleteUser(function (error, result) {
                  if (error) {
                    console.log(error);
                    deleteReject(error.message);
                  } else {
                    deleteResolve('Deleted from User Pool ' + username);
                  }
                });
              });
            });
          }
        }, {
          key: "changePassword",
          value: function changePassword(username, oldPassword, newPassword) {
            console.log(username);
            var userData = {
              Username: username,
              Pool: userPool
            };
            var cognitoUser = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["CognitoUser"](userData);
            return new Promise(function (sessionResolve, sessionReject) {
              cognitoUser.getSession(function (sessionError, session) {
                if (sessionError) {
                  console.error(sessionError);
                  sessionReject('Failed to Get Session for User');
                } else if (session.isValid()) {
                  sessionResolve('Got Session');
                }
              });
            }).then(function (_) {
              return new Promise(function (changeResolve, changeReject) {
                cognitoUser.changePassword(oldPassword, newPassword, function (error, result) {
                  if (error) {
                    console.error(error);
                    var message = error.message;

                    if (error.name === 'NotAuthorizedException' || error.name === 'InvalidPasswordException' || error.name === 'InvalidParameterException') {
                      message = 'Incorrect Current Password';
                    } else if (error.name === 'LimitExceededException') {
                      message = 'Exceeded Password Change Attempts';
                    } else if (error.name === 'PasswordResetRequiredException') {
                      message = 'Current Password Must be Reset';
                    }

                    changeReject(message);
                  } else {
                    changeResolve('Password Changed');
                  }
                });
              });
            });
          }
        }, {
          key: "resetPassword",
          value: function resetPassword(username) {
            console.log(username);
            var userData = {
              Username: username,
              Pool: userPool
            };
            var cognitoUser = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["CognitoUser"](userData);
            return new Promise(function (resolve, reject) {
              cognitoUser.forgotPassword({
                onSuccess: function onSuccess(data) {
                  resolve('Reset Code Sent');
                },
                onFailure: function onFailure(error) {
                  console.error(error);
                  reject(error.message);
                }
              });
            });
          }
        }, {
          key: "confirmResetCode",
          value: function confirmResetCode(username, code, newPassword) {
            console.log(username);
            var userData = {
              Username: username,
              Pool: userPool
            };
            var cognitoUser = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_5__["CognitoUser"](userData);
            return new Promise(function (resolve, reject) {
              cognitoUser.confirmPassword(code, newPassword, {
                onSuccess: function onSuccess() {
                  resolve('Password Reset');
                },
                onFailure: function onFailure(error) {
                  console.error(error);
                  reject(error.message);
                }
              });
            });
          }
        }, {
          key: "logout",
          value: function logout() {
            var _this8 = this;

            this.authStatus.next(false);
            this.firebaseMessagingService.stopReceiveMessage().subscribe(function (_) {}).add(function () {
              console.log('sign out');

              _this8.tasksService.onLogout();

              _this8.groupsService.onLogout();

              _this8.userService.onLogout();

              _this8.getAuthenticatedUser().signOut();
            });
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [{
          type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
        }, {
          type: _groups_groups_service__WEBPACK_IMPORTED_MODULE_8__["GroupsService"]
        }, {
          type: _tasks_tasks_service__WEBPACK_IMPORTED_MODULE_7__["TasksService"]
        }, {
          type: _firebase_messaging_service__WEBPACK_IMPORTED_MODULE_2__["FirebaseMessagingService"]
        }];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Injectable"])({
        providedIn: 'root'
      })], AuthService);
      /***/
    },

    /***/
    "./src/app/auth/firebase-messaging.service.ts":
    /*!****************************************************!*\
      !*** ./src/app/auth/firebase-messaging.service.ts ***!
      \****************************************************/

    /*! exports provided: FirebaseMessagingService */

    /***/
    function srcAppAuthFirebaseMessagingServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FirebaseMessagingService", function () {
        return FirebaseMessagingService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _tasks_tasks_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../tasks/tasks.service */
      "./src/app/tasks/tasks.service.ts");
      /* harmony import */


      var _groups_groups_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../groups/groups.service */
      "./src/app/groups/groups.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var angularfire2_messaging__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! angularfire2/messaging */
      "./node_modules/angularfire2/messaging/index.js");
      /* harmony import */


      var angularfire2_messaging__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(angularfire2_messaging__WEBPACK_IMPORTED_MODULE_6__);
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! src/environments/environment */
      "./src/environments/environment.ts");

      var FirebaseMessagingService = /*#__PURE__*/function () {
        function FirebaseMessagingService(angularFireMessaging, groupsService, tasksService, userService, httpClient) {
          var _this9 = this;

          _classCallCheck(this, FirebaseMessagingService);

          this.angularFireMessaging = angularFireMessaging;
          this.groupsService = groupsService;
          this.tasksService = tasksService;
          this.userService = userService;
          this.httpClient = httpClient;
          this.angularFireMessaging.messaging.subscribe( // tslint:disable-next-line: variable-name
          function (_messaging) {
            _messaging.onMessage = _messaging.onMessage.bind(_messaging);
            _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
          });
          var myProfileSubscription = this.userService.getMyProfile().subscribe(function (myProfile) {
            if (myProfile) {
              _this9.requestToken().subscribe(function (deviceId) {
                console.log(deviceId);

                _this9.updateDeviceToken(deviceId).subscribe(function (endpointArn) {
                  console.log(endpointArn);

                  _this9.receiveMessage();
                });
              });
            }
          });
        }

        _createClass(FirebaseMessagingService, [{
          key: "requestPermission",
          value: function requestPermission() {
            return Notification.requestPermission();
          }
        }, {
          key: "requestToken",
          value: function requestToken() {
            return this.angularFireMessaging.requestToken.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (deviceToken) {
              console.log(deviceToken);
            }));
          }
        }, {
          key: "receiveMessage",
          value: function receiveMessage() {
            var _this10 = this;

            console.log('receiveMessage');
            this.receiveMessageSubscription = this.angularFireMessaging.messages.subscribe(function (fcmMessage) {
              console.log('receiveMessaged');
              console.log(fcmMessage);

              if (fcmMessage && fcmMessage.data && fcmMessage.data["default"]) {
                var payload = JSON.parse(fcmMessage.data["default"]);
                console.log(payload);

                if (payload) {
                  var notificationBody = 'A change detected in your groups or tasks';

                  if (payload.object === 'group') {
                    _this10.groupsService.fetchGroups().subscribe();

                    if (payload.action === 'add') {
                      notificationBody = 'You have been added to a group';
                    } else if (payload.action === 'update') {
                      notificationBody = 'Your group role has been changed';
                    }
                  } else if (payload.object === 'task') {
                    _this10.tasksService.fetchMyTasks().subscribe();

                    if (payload.action === 'create' || payload.action === 'assign') {
                      notificationBody = 'You have been assigned to a task';
                    } else if (payload.action === 'unassign') {
                      notificationBody = 'You have been unassigned from a task';
                    } else if (payload.action === 'delete') {
                      notificationBody = 'A task you were assigned to has been deleted';
                    }
                  }

                  _capacitor_core__WEBPACK_IMPORTED_MODULE_9__["LocalNotifications"].schedule({
                    notifications: [{
                      id: 0,
                      body: notificationBody,
                      title: 'Task Manager'
                    }]
                  });
                }
              }
            });
          }
        }, {
          key: "stopReceiveMessage",
          value: function stopReceiveMessage() {
            if (this.receiveMessageSubscription) {
              this.receiveMessageSubscription.unsubscribe();
            }

            return this.deleteDeviceToken();
          }
        }, {
          key: "updateDeviceToken",
          value: function updateDeviceToken(deviceToken) {
            return this.httpClient.patch(src_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].API_URL + 'users/mydevice', {
              deviceToken: deviceToken
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (responseData) {
              console.log('updateDeviceToken');
              console.log(responseData);
            }));
          }
        }, {
          key: "deleteDeviceToken",
          value: function deleteDeviceToken() {
            return this.httpClient["delete"](src_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].API_URL + 'users/mydevice').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (responseData) {
              console.log('deleteDeviceToken');
            }));
          }
        }, {
          key: "handleError",
          value: function handleError(error) {
            console.error(error);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["throwError"])(error.error.message);
          }
        }]);

        return FirebaseMessagingService;
      }();

      FirebaseMessagingService.ctorParameters = function () {
        return [{
          type: angularfire2_messaging__WEBPACK_IMPORTED_MODULE_6__["AngularFireMessaging"]
        }, {
          type: _groups_groups_service__WEBPACK_IMPORTED_MODULE_4__["GroupsService"]
        }, {
          type: _tasks_tasks_service__WEBPACK_IMPORTED_MODULE_3__["TasksService"]
        }, {
          type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      FirebaseMessagingService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Injectable"])({
        providedIn: 'root'
      })], FirebaseMessagingService);
      /***/
    },

    /***/
    "./src/app/groups/group.model.ts":
    /*!***************************************!*\
      !*** ./src/app/groups/group.model.ts ***!
      \***************************************/

    /*! exports provided: GroupMember, MemberSelectorOptions, GroupModel */

    /***/
    function srcAppGroupsGroupModelTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GroupMember", function () {
        return GroupMember;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MemberSelectorOptions", function () {
        return MemberSelectorOptions;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GroupModel", function () {
        return GroupModel;
      });

      var GroupMember = function GroupMember(email, identifier, name, role, accepted, title) {
        _classCallCheck(this, GroupMember);

        this.email = email;
        this.identifier = identifier;
        this.name = name;
        this.role = role;
        this.accepted = accepted;
        this.title = title;
      };

      var MemberSelectorOptions = function MemberSelectorOptions() {
        _classCallCheck(this, MemberSelectorOptions);
      };

      var GroupModel = /*#__PURE__*/function () {
        function GroupModel(identifier, name, role, accepted) {
          _classCallCheck(this, GroupModel);

          this.identifier = identifier;
          this.name = name;
          this.role = role;
          this.accepted = accepted; // tslint:disable-next-line: variable-name

          this._members = []; // tslint:disable-next-line: variable-name

          this._tasks = []; // tslint:disable-next-line: variable-name

          this._ownerCount = 0;
        }

        _createClass(GroupModel, [{
          key: "getMember",
          value: function getMember(identifier) {
            if (!identifier) {
              return null;
            }

            var _iterator = _createForOfIteratorHelper(this._members),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var member = _step.value;

                if (member.identifier === identifier) {
                  return member;
                }
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
          }
        }, {
          key: "getMemberSelectorOptions",
          value: function getMemberSelectorOptions() {
            var memberSelectionOptions = [];

            this._members.forEach(function (member) {
              if (member.accepted) {
                var option = {
                  value: member.identifier,
                  display: member.name + ' ' + member.email
                };
                memberSelectionOptions.push(option);
              }
            });

            return memberSelectionOptions;
          }
        }, {
          key: "setMembers",
          value: function setMembers(members) {
            var _this11 = this;

            this._members = members.sort(function (a, b) {
              return a.name < b.name ? -1 : 1;
            });
            this._ownerCount = 0;

            if (this._members) {
              this._members.forEach(function (member) {
                if (member.role === 'Owner') {
                  _this11._ownerCount++;
                }
              });
            }
          }
        }, {
          key: "setTasks",
          value: function setTasks(tasks) {
            this._tasks = tasks.sort(function (a, b) {
              return a.dueDate.getTime() - b.dueDate.getTime();
            });
          }
        }, {
          key: "members",
          get: function get() {
            return _toConsumableArray(this._members);
          }
        }, {
          key: "tasks",
          get: function get() {
            return _toConsumableArray(this._tasks);
          }
        }, {
          key: "ownerCount",
          get: function get() {
            return this._ownerCount;
          }
        }]);

        return GroupModel;
      }();
      /***/

    },

    /***/
    "./src/app/groups/groups.service.ts":
    /*!******************************************!*\
      !*** ./src/app/groups/groups.service.ts ***!
      \******************************************/

    /*! exports provided: GroupsService */

    /***/
    function srcAppGroupsGroupsServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GroupsService", function () {
        return GroupsService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _tasks_tasks_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../tasks/tasks.service */
      "./src/app/tasks/tasks.service.ts");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var _group_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./group.model */
      "./src/app/groups/group.model.ts");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var GroupResponse = function GroupResponse() {
        _classCallCheck(this, GroupResponse);
      };

      var GroupMembershipResponse = function GroupMembershipResponse() {
        _classCallCheck(this, GroupMembershipResponse);
      };

      var UpdateGroupMembershipResponse = function UpdateGroupMembershipResponse() {
        _classCallCheck(this, UpdateGroupMembershipResponse);
      };

      var DeleteGroupMembershipResponse = function DeleteGroupMembershipResponse() {
        _classCallCheck(this, DeleteGroupMembershipResponse);
      };

      var GroupsService = /*#__PURE__*/function () {
        function GroupsService(httpClient, tasksService, userService) {
          var _this12 = this;

          _classCallCheck(this, GroupsService);

          this.httpClient = httpClient;
          this.tasksService = tasksService;
          this.userService = userService; // tslint:disable-next-line: variable-name

          this._groups = new rxjs__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"]([]);
          this.userService.getMyProfile().subscribe(function (myProfile) {
            _this12.myProfile = myProfile;
          });
        }

        _createClass(GroupsService, [{
          key: "fetchGroups",
          value: function fetchGroups() {
            var _this13 = this;

            return this.httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups/mygroups').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (myGroupsResponse) {
              var newGroups = [];
              myGroupsResponse.forEach(function (myGroup) {
                var groupModel = new _group_model__WEBPACK_IMPORTED_MODULE_4__["GroupModel"](myGroup.identifier, myGroup.name, myGroup.role, myGroup.accepted);
                newGroups.push(groupModel);
              });

              var sortedGroups = _this13.sortGroups(newGroups);

              _this13._groups.next(sortedGroups);

              return sortedGroups;
            }));
          }
        }, {
          key: "getGroupByIdentifier",
          value: function getGroupByIdentifier(identifier) {
            var _this14 = this;

            var groups = this._groups.getValue();

            console.log(groups);
            return this.httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups/' + identifier).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["switchMap"])(function (groupResponse) {
              var groupModel = new _group_model__WEBPACK_IMPORTED_MODULE_4__["GroupModel"](groupResponse.identifier, groupResponse.name, groupResponse.role, groupResponse.accepted);
              return _this14.getGroupsMembers(identifier).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["switchMap"])(function (groupMembers) {
                console.log(groupMembers);
                groupModel.setMembers(groupMembers);
                return _this14.tasksService.fetchTasksByGroup(identifier).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(_this14.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (groupsTasks) {
                  console.log(groupsTasks);
                  groupModel.setTasks(groupsTasks);
                  groups.forEach(function (group, index) {
                    if (group.identifier === groupModel.identifier) {
                      groups[index] = groupModel;
                    }
                  });

                  _this14._groups.next(_toConsumableArray(groups));

                  return groupModel;
                }));
              }));
            }));
          }
        }, {
          key: "addGroup",
          value: function addGroup(name) {
            var _this15 = this;

            return this.httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups', {
              name: name
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (response) {
              console.log(response);

              var groups = _this15._groups.getValue();

              var newGroup = new _group_model__WEBPACK_IMPORTED_MODULE_4__["GroupModel"](response.identifier, response.name, response.role, response.accepted);
              groups.push(newGroup);

              _this15._groups.next(_toConsumableArray(groups));

              return newGroup;
            }));
          }
        }, {
          key: "editGroup",
          value: function editGroup(identifier, name) {
            var _this16 = this;

            return this.httpClient.patch(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups/' + identifier, {
              name: name
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (response) {
              console.log(response);

              var groups = _this16._groups.getValue();

              var newGroup = new _group_model__WEBPACK_IMPORTED_MODULE_4__["GroupModel"](response.identifier, response.name, response.role, response.accepted);
              groups.forEach(function (group, index) {
                if (group.identifier === newGroup.identifier) {
                  newGroup.setMembers(groups[index].members);
                  groups[index] = newGroup;
                }
              });

              var sortedGroups = _this16.sortGroups(groups);

              _this16._groups.next(_toConsumableArray(sortedGroups));

              return newGroup;
            }));
          }
        }, {
          key: "deleteGroup",
          value: function deleteGroup(identifier) {
            var _this17 = this;

            return this.httpClient["delete"](_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups/' + identifier).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (deleteResponse) {
              var groups = _this17._groups.getValue();

              groups.forEach(function (group, index) {
                if (group.identifier === identifier) {
                  groups.splice(index, 1);
                }
              });

              _this17._groups.next(_toConsumableArray(groups));
            }));
          }
        }, {
          key: "getGroupsMembers",
          value: function getGroupsMembers(identifier) {
            var _this18 = this;

            return this.httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups/' + identifier + '/members').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (groupMembership) {
              console.log(groupMembership);
              var groupMembers = [];
              groupMembership.forEach(function (member) {
                var newMember = new _group_model__WEBPACK_IMPORTED_MODULE_4__["GroupMember"](member.email, member.userIdentifier, member.name, member.role, member.accepted, member.title);
                groupMembers.push(newMember);
              });
              return _this18.sortMembers(groupMembers);
            }));
          }
        }, {
          key: "addGroupMember",
          value: function addGroupMember(groupIdentifier, newMemberEmail, role) {
            var _this19 = this;

            return this.httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups/' + groupIdentifier + '/members', {
              email: newMemberEmail,
              role: role
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (response) {
              console.log(response);
              var newMember = new _group_model__WEBPACK_IMPORTED_MODULE_4__["GroupMember"](newMemberEmail, response.userIdentifier, response.name, role, response.accepted, response.title);

              var groups = _this19._groups.getValue();

              groups.forEach(function (group) {
                if (group.identifier === groupIdentifier) {
                  var groupsMembers = group.members;
                  groupsMembers.push(newMember);
                  group.setMembers(groupsMembers);
                }
              });

              _this19._groups.next(_toConsumableArray(groups));

              return newMember;
            }));
          }
        }, {
          key: "updateGroupsMember",
          value: function updateGroupsMember(groupIdentifier, userIdentifier, role) {
            var _this20 = this;

            return this.httpClient.patch(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups/' + groupIdentifier + '/members/' + userIdentifier, {
              role: role
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (patchResponse) {
              console.log(patchResponse);

              var groups = _this20._groups.getValue();

              groups.forEach(function (group) {
                if (group.identifier === groupIdentifier) {
                  var groupsMembers = group.members;
                  groupsMembers.forEach(function (member) {
                    if (member.identifier === userIdentifier) {
                      member.role = role;
                    }
                  });
                  group.setMembers(groupsMembers);
                }
              });

              _this20._groups.next(_toConsumableArray(groups));
            }));
          }
        }, {
          key: "deleteGroupsMember",
          value: function deleteGroupsMember(groupIdentifier, userIdentifier) {
            var _this21 = this;

            return this.httpClient["delete"](_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups/' + groupIdentifier + '/members/' + userIdentifier).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (deleteResponse) {
              console.log(deleteResponse);

              var groups = _this21._groups.getValue();

              groups.forEach(function (group, groupIndex) {
                if (group.identifier === groupIdentifier) {
                  if (_this21.myProfile && userIdentifier !== _this21.myProfile.identifier) {
                    var groupsMembers = group.members;
                    groupsMembers.forEach(function (member, memberIndex) {
                      if (member.identifier === userIdentifier) {
                        groupsMembers.splice(memberIndex, 1);
                      }
                    });
                    group.setMembers(groupsMembers);
                  } else {
                    groups.splice(groupIndex, 1);
                  }
                }
              });

              _this21._groups.next(_toConsumableArray(groups));

              return deleteResponse;
            }));
          }
        }, {
          key: "updateGroupsMemberAccepted",
          value: function updateGroupsMemberAccepted(groupIdentifier, accepted) {
            var _this22 = this;

            return this.httpClient.patch(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'groups/' + groupIdentifier + '/accepted/', {
              accepted: accepted
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (patchResponse) {
              console.log(patchResponse);

              var groups = _this22._groups.getValue();

              groups.forEach(function (group) {
                if (group.identifier === groupIdentifier) {
                  var groupsMembers = group.members;
                  groupsMembers.forEach(function (member) {
                    if (member.identifier === patchResponse.userIdentifier) {
                      member.accepted = accepted;
                    }
                  });
                  group.setMembers(groupsMembers);
                }
              });

              _this22._groups.next(_toConsumableArray(groups));
            }));
          }
        }, {
          key: "addTask",
          value: function addTask(groupIdentifier, name, description, dueDate, owner, repeats) {
            var _this23 = this;

            return this.tasksService.addTask(groupIdentifier, name, description, dueDate, owner, repeats).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (taskModel) {
              var groups = _this23._groups.getValue();

              groups.forEach(function (group) {
                if (group.identifier === groupIdentifier) {
                  var groupsTasks = group.tasks;
                  groupsTasks.push(taskModel);
                  group.setTasks(groupsTasks);
                }
              });
              console.log(groups);

              _this23._groups.next(_toConsumableArray(groups));
            }));
          }
        }, {
          key: "changeTaskOwner",
          value: function changeTaskOwner(groupIdentifier, taskIdentifier, ownerIdentifier) {
            var _this24 = this;

            return this.tasksService.changeTaskOwner(taskIdentifier, ownerIdentifier).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (taskModel) {
              var groups = _this24._groups.getValue();

              groups.forEach(function (group) {
                if (group.identifier === groupIdentifier) {
                  var groupsTasks = group.tasks;
                  groupsTasks.forEach(function (task, taskIndex) {
                    if (task.identifier === taskIdentifier) {
                      groupsTasks[taskIndex] = taskModel;
                    }
                  });
                  group.setTasks(groupsTasks);
                }
              });
              console.log(groups);

              _this24._groups.next(_toConsumableArray(groups));
            }));
          }
        }, {
          key: "changeTaskStatus",
          value: function changeTaskStatus(groupIdentifier, taskIdentifier, status) {
            var _this25 = this;

            return this.tasksService.changeTaskStatus(taskIdentifier, status).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (taskModel) {
              var groups = _this25._groups.getValue();

              groups.forEach(function (group) {
                if (group.identifier === groupIdentifier) {
                  var groupsTasks = group.tasks;
                  groupsTasks.forEach(function (task, taskIndex) {
                    if (task.identifier === taskIdentifier) {
                      groupsTasks[taskIndex] = taskModel;
                    }
                  });
                  group.setTasks(groupsTasks);
                }
              });
              console.log(groups);

              _this25._groups.next(_toConsumableArray(groups));
            }));
          }
        }, {
          key: "updateTask",
          value: function updateTask(groupIdentifier, taskIdentifier, name, description, dueDate, owner, repeats) {
            var _this26 = this;

            return this.tasksService.updateTask(taskIdentifier, name, description, dueDate, owner, repeats).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (taskModel) {
              console.log(taskModel);

              var groups = _this26._groups.getValue();

              groups.forEach(function (group) {
                if (group.identifier === groupIdentifier) {
                  var groupsTasks = group.tasks;
                  groupsTasks.forEach(function (task, taskIndex) {
                    if (task.identifier === taskIdentifier) {
                      groupsTasks[taskIndex] = taskModel;
                    }
                  });
                  group.setTasks(groupsTasks);
                }
              });
              console.log(groups);

              _this26._groups.next(_toConsumableArray(groups));
            }));
          }
        }, {
          key: "deleteTask",
          value: function deleteTask(groupIdentifier, taskIdentifier) {
            var _this27 = this;

            return this.tasksService.deleteTask(taskIdentifier).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["tap"])(function (_) {
              var groups = _this27._groups.getValue();

              groups.forEach(function (group) {
                if (group.identifier === groupIdentifier) {
                  var groupsTasks = group.tasks;
                  groupsTasks.forEach(function (task, taskIndex) {
                    if (task.identifier === taskIdentifier) {
                      groupsTasks.splice(taskIndex, 1);
                    }
                  });
                  group.setTasks(groupsTasks);
                }
              });
              console.log(groups);

              _this27._groups.next(_toConsumableArray(groups));
            }));
          }
        }, {
          key: "onLogout",
          value: function onLogout() {
            this._groups.next(null);
          }
        }, {
          key: "handleError",
          value: function handleError(error) {
            console.error(error);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["throwError"])(error.error.message);
          }
        }, {
          key: "sortGroups",
          value: function sortGroups(groups) {
            return groups.sort(function (a, b) {
              return a.name.toLocaleLowerCase() > b.name.toLocaleLowerCase() ? 1 : -1;
            });
          }
        }, {
          key: "sortMembers",
          value: function sortMembers(members) {
            return members.sort(function (a, b) {
              return a.name.toLocaleLowerCase() > b.name.toLocaleLowerCase() ? 1 : -1;
            });
          }
        }, {
          key: "groups",
          get: function get() {
            var _this28 = this;

            if (this._groups.getValue()) {
              return this._groups.asObservable();
            }

            return this.fetchGroups().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["switchMap"])(function (fetchGroupsResult) {
              return _this28._groups.asObservable();
            }));
          }
        }]);

        return GroupsService;
      }();

      GroupsService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]
        }, {
          type: _tasks_tasks_service__WEBPACK_IMPORTED_MODULE_2__["TasksService"]
        }, {
          type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
        }];
      };

      GroupsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_7__["Injectable"])({
        providedIn: 'root'
      })], GroupsService);
      /***/
    },

    /***/
    "./src/app/profile/profile.guard.ts":
    /*!******************************************!*\
      !*** ./src/app/profile/profile.guard.ts ***!
      \******************************************/

    /*! exports provided: ProfileGuard */

    /***/
    function srcAppProfileProfileGuardTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfileGuard", function () {
        return ProfileGuard;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var ProfileGuard = /*#__PURE__*/function () {
        function ProfileGuard(userService, router) {
          _classCallCheck(this, ProfileGuard);

          this.userService = userService;
          this.router = router;
          this.profileOk = false;
        }

        _createClass(ProfileGuard, [{
          key: "canActivate",
          value: function canActivate(route, state) {
            var _this29 = this;

            console.log('canActivate');
            return this.userService.getMyProfile().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (myProfile) {
              if (myProfile) {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(true);
              } else {
                return _this29.userService.fetchMyProfile().pipe( // tslint:disable-next-line: arrow-return-shorthand
                Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (_) {
                  return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(false);
                }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (myProfile) {
                  if (!myProfile) {
                    _this29.router.navigateByUrl('/profile');

                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(false);
                  }

                  _this29.profileOk = true;
                  return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(true);
                }));
              }
            }));
          }
        }]);

        return ProfileGuard;
      }();

      ProfileGuard.ctorParameters = function () {
        return [{
          type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      ProfileGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], ProfileGuard);
      /***/
    },

    /***/
    "./src/app/tasks/notifications.service.ts":
    /*!************************************************!*\
      !*** ./src/app/tasks/notifications.service.ts ***!
      \************************************************/

    /*! exports provided: NotificationsService */

    /***/
    function srcAppTasksNotificationsServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationsService", function () {
        return NotificationsService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");

      var LocalStorageNotification = function LocalStorageNotification() {
        _classCallCheck(this, LocalStorageNotification);
      };

      var NotificationsService = /*#__PURE__*/function () {
        function NotificationsService(userService) {
          var _this30 = this;

          _classCallCheck(this, NotificationsService);

          this.userService = userService;
          this.storageKeyPrefix = 'notification:task:';
          var myProfileSubscription = this.userService.getMyProfile().subscribe(function (myProfile) {
            if (myProfile) {
              _this30.myProfile = myProfile;
              myProfileSubscription.unsubscribe();
            }
          });
        }

        _createClass(NotificationsService, [{
          key: "scheduleNotification",
          value: function scheduleNotification(taskModel) {
            var _this31 = this;

            if (!this.myProfile || taskModel.ownerIdentifier !== this.myProfile.identifier) {
              console.log('not my task ' + taskModel.identifier);
              this.cancelNotification(taskModel.identifier);
              return;
            }

            var today = new Date(Date.now()).setHours(0, 0, 0, 0);

            if (taskModel.dueDate.getTime() < today) {
              console.log('task in the past - no notification scheduled ' + taskModel.identifier);
              this.cancelNotification(taskModel.identifier);
              return;
            }

            this.readStoredNotification(taskModel.identifier).then(function (localStorageNotification) {
              console.log(localStorageNotification);

              if (!localStorageNotification || new Date(localStorageNotification.dueDate).getTime() !== taskModel.dueDate.getTime()) {
                if (localStorageNotification) {
                  console.log('rescheduing');
                  console.log(new Date(localStorageNotification.dueDate));
                  console.log(taskModel.dueDate);

                  _this31.cancelNotification(taskModel.identifier).then(function (cancelResponse) {
                    _this31.doSchedule(taskModel);
                  });
                } else {
                  _this31.doSchedule(taskModel);
                }
              }
            });
          }
        }, {
          key: "doSchedule",
          value: function doSchedule(taskModel) {
            var _this32 = this;

            var localNotificationSchedule = {
              repeats: false,
              at: taskModel.dueDate
            };
            var notificationId = Math.round(Math.random() * 10000000);
            var notification = {
              id: notificationId,
              title: 'Task Master',
              body: 'Due today: ' + taskModel.name,
              schedule: localNotificationSchedule,
              group: 'tasks'
            };
            var notifications = [];
            notifications.push(notification);

            _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["LocalNotifications"].schedule({
              notifications: notifications
            }).then(function (notificationResult) {
              console.log(notificationResult);

              _this32.writeNotificationToStorage(taskModel, notificationResult.notifications[0].id);
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "clearDeletedAndPastTaskNotifications",
          value: function clearDeletedAndPastTaskNotifications(taskModels) {
            var _this33 = this;

            if (taskModels && taskModels.length > 0) {
              var myFutureTaskIdentifiers = [];
              var today = new Date(Date.now()).setHours(0, 0, 0, 0);
              taskModels.forEach(function (taskModel) {
                if (!_this33.myProfile || taskModel.ownerIdentifier !== _this33.myProfile.identifier) {
                  console.log('not my task - cancel any notification ' + taskModel.identifier);

                  _this33.cancelNotification(taskModel.identifier);
                } else if (taskModel.dueDate.getTime() < today) {
                  console.log('task in the past - cancel any notification ' + taskModel.identifier);

                  _this33.cancelNotification(taskModel.identifier);
                } else {
                  myFutureTaskIdentifiers.push(taskModel.identifier);
                }
              });

              _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].Storage.keys().then(function (keys) {
                if (keys && keys.keys) {
                  keys.keys.forEach(function (key) {
                    var storageKeyPrefixIndex = key.indexOf(_this33.storageKeyPrefix);

                    if (storageKeyPrefixIndex !== -1) {
                      var taskIdentiferStartIndex = storageKeyPrefixIndex + _this33.storageKeyPrefix.length;
                      var taskIdentifier = key.substring(taskIdentiferStartIndex);
                      console.log(taskIdentifier);

                      _this33.cancelNotification(taskIdentifier);

                      if (!(taskIdentifier in myFutureTaskIdentifiers)) {
                        _this33.cancelNotification(taskIdentifier);
                      }
                    }
                  });
                }
              });
            } else {
              _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].Storage.keys().then(function (keys) {
                if (keys && keys.keys) {
                  keys.keys.forEach(function (key) {
                    var storageKeyPrefixIndex = key.indexOf(_this33.storageKeyPrefix);

                    if (storageKeyPrefixIndex !== -1) {
                      var taskIdentiferStartIndex = storageKeyPrefixIndex + _this33.storageKeyPrefix.length;
                      var taskIdentifier = key.substring(taskIdentiferStartIndex);
                      console.log(taskIdentifier);

                      _this33.cancelNotification(taskIdentifier);
                    }
                  });
                }
              });
            }
          }
        }, {
          key: "cancelNotification",
          value: function cancelNotification(taskIdentifier) {
            var _this34 = this;

            return this.readStoredNotification(taskIdentifier).then(function (localStorageNotification) {
              console.log(localStorageNotification);

              if (localStorageNotification) {
                console.log(localStorageNotification.notificationId);
                var localNotificationRequest = {
                  id: localStorageNotification.notificationId
                };
                var localNotificationRequests = [];
                localNotificationRequests.push(localNotificationRequest);
                var localNotificationPendingList = {
                  notifications: localNotificationRequests
                };
                return _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["LocalNotifications"].cancel(localNotificationPendingList).then(function (cancelResponse) {
                  _this34.removeNotificationInStorage(taskIdentifier).then(function (removeResponse) {});
                });
              }
            });
          }
        }, {
          key: "readStoredNotification",
          value: function readStoredNotification(taskIdentifier) {
            return _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].Storage.get({
              key: this.storageKeyPrefix + taskIdentifier
            }).then(function (storedData) {
              if (!storedData || !storedData.value) {
                console.log('no stored data ' + taskIdentifier);
                return null;
              }

              var parsedData = JSON.parse(storedData.value);
              return parsedData;
            });
          }
        }, {
          key: "writeNotificationToStorage",
          value: function writeNotificationToStorage(taskModel, notificationId) {
            var localStorageNotification = {
              notificationId: notificationId,
              taskIdentifier: taskModel.identifier,
              dueDate: taskModel.dueDate.toISOString()
            };
            var data = JSON.stringify(localStorageNotification);
            console.log(data);
            return _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].Storage.set({
              key: this.storageKeyPrefix + taskModel.identifier,
              value: data
            });
          }
        }, {
          key: "removeNotificationInStorage",
          value: function removeNotificationInStorage(taskIdentifier) {
            console.log(taskIdentifier);
            return _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].Storage.remove({
              key: this.storageKeyPrefix + taskIdentifier
            });
          }
        }]);

        return NotificationsService;
      }();

      NotificationsService.ctorParameters = function () {
        return [{
          type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
        }];
      };

      NotificationsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], NotificationsService);
      /***/
    },

    /***/
    "./src/app/tasks/task.model.ts":
    /*!*************************************!*\
      !*** ./src/app/tasks/task.model.ts ***!
      \*************************************/

    /*! exports provided: TaskModel */

    /***/
    function srcAppTasksTaskModelTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TaskModel", function () {
        return TaskModel;
      });

      var TaskModel = function TaskModel(identifier, name, description, dueDate, ownerIdentifier, groupIdentifier, repeats, status) {
        _classCallCheck(this, TaskModel);

        this.identifier = identifier;
        this.name = name;
        this.description = description;
        this.dueDate = dueDate;
        this.ownerIdentifier = ownerIdentifier;
        this.groupIdentifier = groupIdentifier;
        this.repeats = repeats;
        this.status = status;
      };
      /***/

    },

    /***/
    "./src/app/tasks/tasks.service.ts":
    /*!****************************************!*\
      !*** ./src/app/tasks/tasks.service.ts ***!
      \****************************************/

    /*! exports provided: TasksService */

    /***/
    function srcAppTasksTasksServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TasksService", function () {
        return TasksService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _task_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./task.model */
      "./src/app/tasks/task.model.ts");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _notifications_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./notifications.service */
      "./src/app/tasks/notifications.service.ts");

      var TaskResponse = function TaskResponse() {
        _classCallCheck(this, TaskResponse);
      };

      var TasksService = /*#__PURE__*/function () {
        function TasksService(httpClient, notificationsService) {
          _classCallCheck(this, TasksService);

          this.httpClient = httpClient;
          this.notificationsService = notificationsService;
          this.myTasks = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
        }

        _createClass(TasksService, [{
          key: "fetchTasksByGroup",
          value: function fetchTasksByGroup(groupIdentifier) {
            return this.httpClient.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].API_URL + 'groups/' + groupIdentifier + '/tasks').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (tasksResponse) {
              console.log(tasksResponse);
              var receivedTasks = [];
              tasksResponse.forEach(function (task) {
                var taskModel = new _task_model__WEBPACK_IMPORTED_MODULE_1__["TaskModel"](task.identifier, task.name, task.description, new Date(task.dueDate), task.ownerIdentifier, task.groupIdentifier, task.repeats, task.status);
                receivedTasks.push(taskModel);
              });
              return receivedTasks;
            }));
          }
        }, {
          key: "fetchMyTasks",
          value: function fetchMyTasks() {
            var _this35 = this;

            return this.httpClient.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].API_URL + 'tasks/mytasks').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (tasksResponse) {
              console.log(tasksResponse);
              var myTasks = [];
              tasksResponse.forEach(function (task, taskIndex) {
                var taskModel = new _task_model__WEBPACK_IMPORTED_MODULE_1__["TaskModel"](task.identifier, task.name, task.description, new Date(task.dueDate), task.ownerIdentifier, task.groupIdentifier, task.repeats, task.status);
                myTasks.push(taskModel);

                _this35.notificationsService.scheduleNotification(taskModel);
              });

              _this35.notificationsService.clearDeletedAndPastTaskNotifications(myTasks);

              _this35.myTasks.next(myTasks);

              return myTasks;
            }));
          }
        }, {
          key: "addTask",
          value: function addTask(groupIdentifier, name, description, dueDate, // tslint:disable-next-line: align
          owner, repeats) {
            var _this36 = this;

            return this.httpClient.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].API_URL + 'tasks/', {
              group: groupIdentifier,
              name: name,
              description: description,
              dueDate: dueDate.toISOString(),
              owner: owner,
              repeats: repeats
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (response) {
              console.log(response);
              var newTask = new _task_model__WEBPACK_IMPORTED_MODULE_1__["TaskModel"](response.identifier, response.name, response.description, new Date(response.dueDate), response.ownerIdentifier, response.groupIdentifier, response.repeats, response.status);

              _this36.notificationsService.scheduleNotification(newTask);

              return newTask;
            }));
          }
        }, {
          key: "updateTask",
          value: function updateTask(taskIdentifier, name, description, dueDate, // tslint:disable-next-line: align
          owner, repeats) {
            var _this37 = this;

            return this.httpClient.patch(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].API_URL + 'tasks/' + taskIdentifier, {
              name: name,
              description: description,
              dueDate: dueDate.toISOString(),
              owner: owner,
              repeats: repeats
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (response) {
              console.log(response);
              var newTask = new _task_model__WEBPACK_IMPORTED_MODULE_1__["TaskModel"](taskIdentifier, response.name, response.description, new Date(response.dueDate), response.ownerIdentifier, response.groupIdentifier, response.repeats, response.status);

              _this37.notificationsService.scheduleNotification(newTask);

              return newTask;
            }));
          }
        }, {
          key: "changeTaskOwner",
          value: function changeTaskOwner(taskIdentifier, ownerIdentifier) {
            var _this38 = this;

            return this.httpClient.patch(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].API_URL + 'tasks/' + taskIdentifier + '/owner', {
              newOwner: ownerIdentifier
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (response) {
              console.log(response);
              var newTask = new _task_model__WEBPACK_IMPORTED_MODULE_1__["TaskModel"](response.identifier, response.name, response.description, new Date(response.dueDate), response.ownerIdentifier, response.groupIdentifier, response.repeats, response.status);

              _this38.notificationsService.scheduleNotification(newTask);

              return newTask;
            }));
          }
        }, {
          key: "changeTaskStatus",
          value: function changeTaskStatus(taskIdentifier, status) {
            var _this39 = this;

            return this.httpClient.patch(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].API_URL + 'tasks/' + taskIdentifier + '/status', {
              status: status
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (response) {
              console.log(response);
              var newTask = new _task_model__WEBPACK_IMPORTED_MODULE_1__["TaskModel"](response.identifier, response.name, response.description, new Date(response.dueDate), response.ownerIdentifier, response.groupIdentifier, response.repeats, response.status);

              var myTasks = _this39.myTasks.getValue();

              myTasks.forEach(function (task, taskIndex) {
                if (task.identifier === taskIdentifier) {
                  myTasks[taskIndex] = newTask;
                }
              });
              console.log(myTasks);

              _this39.myTasks.next(_toConsumableArray(myTasks));

              return newTask;
            }));
          }
        }, {
          key: "deleteTask",
          value: function deleteTask(taskIdentifier) {
            var _this40 = this;

            return this.httpClient["delete"](src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].API_URL + 'tasks/' + taskIdentifier).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])(function (deleteResponse) {
              console.log(deleteResponse);

              _this40.notificationsService.cancelNotification(taskIdentifier);
            }));
          }
        }, {
          key: "onLogout",
          value: function onLogout() {
            this.myTasks.next(null);
          }
        }, {
          key: "handleError",
          value: function handleError(error) {
            console.error(error);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(error.error.message);
          }
        }]);

        return TasksService;
      }();

      TasksService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }, {
          type: _notifications_service__WEBPACK_IMPORTED_MODULE_7__["NotificationsService"]
        }];
      };

      TasksService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])({
        providedIn: 'root'
      })], TasksService);
      /***/
    },

    /***/
    "./src/app/users/user.model.ts":
    /*!*************************************!*\
      !*** ./src/app/users/user.model.ts ***!
      \*************************************/

    /*! exports provided: UserModel */

    /***/
    function srcAppUsersUserModelTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserModel", function () {
        return UserModel;
      });

      var UserModel = function UserModel(email, identifier, name, title) {
        _classCallCheck(this, UserModel);

        this.email = email;
        this.identifier = identifier;
        this.name = name;
        this.title = title;
      };
      /***/

    },

    /***/
    "./src/app/users/user.service.ts":
    /*!***************************************!*\
      !*** ./src/app/users/user.service.ts ***!
      \***************************************/

    /*! exports provided: UserService */

    /***/
    function srcAppUsersUserServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserService", function () {
        return UserService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../../environments/environment.prod */
      "./src/environments/environment.prod.ts");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var _user_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./user.model */
      "./src/app/users/user.model.ts");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var UserResponse = function UserResponse() {
        _classCallCheck(this, UserResponse);
      };

      var UserService = /*#__PURE__*/function () {
        function UserService(httpClient) {
          _classCallCheck(this, UserService);

          this.httpClient = httpClient;
          this.myProfile = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
        }

        _createClass(UserService, [{
          key: "getMyProfile",
          value: function getMyProfile() {
            return this.myProfile.asObservable();
          }
        }, {
          key: "getUserById",
          value: function getUserById(idToken) {
            console.log(idToken);
            return this.httpClient.get(_environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].API_URL + 'users/' + idToken).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (responseData) {
              console.log('getMyProfile');
              console.log(responseData);
              var userModel = new _user_model__WEBPACK_IMPORTED_MODULE_5__["UserModel"](responseData.email, responseData.identifier, responseData.name, responseData.title);
              return userModel;
            }));
          }
        }, {
          key: "fetchMyProfile",
          value: function fetchMyProfile() {
            var _this41 = this;

            return this.httpClient.get(_environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].API_URL + 'users/myprofile').pipe( // catchError(this.handleError),
            Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (responseData) {
              console.log('getMyProfile');
              console.log(responseData);
              var userModel = new _user_model__WEBPACK_IMPORTED_MODULE_5__["UserModel"](responseData.email, responseData.identifier, responseData.name, responseData.title);

              _this41.myProfile.next(userModel);

              return userModel;
            }));
          }
        }, {
          key: "createMyProfile",
          value: function createMyProfile(email, name, title) {
            var _this42 = this;

            return this.httpClient.post(_environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].API_URL + 'users', {
              email: email,
              name: name,
              title: title
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (responseData) {
              console.log('createMyProfile');
              console.log(responseData);
              var userModel = new _user_model__WEBPACK_IMPORTED_MODULE_5__["UserModel"](email, responseData.identifier, name, title);

              _this42.myProfile.next(userModel);

              return userModel;
            }));
          }
        }, {
          key: "updateMyProfile",
          value: function updateMyProfile(name, title) {
            var _this43 = this;

            return this.httpClient.patch(_environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].API_URL + 'users/myprofile', {
              name: name,
              title: title
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])(function (responseData) {
              console.log('updateMyProfile');
              console.log(responseData);

              _this43.myProfile.next(responseData);
            }));
          }
        }, {
          key: "deleteMyProfile",
          value: function deleteMyProfile() {
            var _this44 = this;

            return this.httpClient["delete"](_environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].API_URL + 'users/myprofile').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])(function (responseData) {
              console.log('deleteMyProfile');
              console.log(responseData);

              _this44.myProfile.next(null);
            }));
          }
        }, {
          key: "onLogout",
          value: function onLogout() {
            console.log('onLogout');
            this.myProfile.next(null);
          }
        }, {
          key: "handleError",
          value: function handleError(error) {
            console.error(error);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(error.error.message);
          }
        }]);

        return UserService;
      }();

      UserService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])({
        providedIn: 'root'
      })], UserService);
      /***/
    },

    /***/
    "./src/environments/environment.prod.ts":
    /*!**********************************************!*\
      !*** ./src/environments/environment.prod.ts ***!
      \**********************************************/

    /*! exports provided: environment */

    /***/
    function srcEnvironmentsEnvironmentProdTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      });

      var environment = {
        production: true,
        API_URL: 'https://vkbrcrmdkf.execute-api.us-east-2.amazonaws.com/dev/',
        COGNITO_POOL_DATA: {
          UserPoolId: 'us-east-2_ijH9DnT8x',
          ClientId: '2cevsbk24c8bkgolmesom4gdh6'
        },
        FIREBASE_CONFIG: {
          apiKey: 'AIzaSyDdF2M9ZqZaNbpMjcaBExn12mKctTaOfVM',
          authDomain: 'taskmanager-4dc45.firebaseapp.com',
          databaseURL: 'https://taskmanager-4dc45.firebaseio.com',
          projectId: 'taskmanager-4dc45',
          storageBucket: 'taskmanager-4dc45.appspot.com',
          messagingSenderId: '486225432160',
          appId: '1:486225432160:web:7d80bf27073e1ec8353d05'
        }
      };
      /***/
    },

    /***/
    "./src/environments/environment.ts":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      });

      var environment = {
        production: false,
        API_URL: 'https://vkbrcrmdkf.execute-api.us-east-2.amazonaws.com/dev/',
        COGNITO_POOL_DATA: {
          UserPoolId: 'us-east-2_ijH9DnT8x',
          ClientId: '2cevsbk24c8bkgolmesom4gdh6'
        },
        FIREBASE_CONFIG: {
          apiKey: 'AIzaSyDdF2M9ZqZaNbpMjcaBExn12mKctTaOfVM',
          authDomain: 'taskmanager-4dc45.firebaseapp.com',
          databaseURL: 'https://taskmanager-4dc45.firebaseio.com',
          projectId: 'taskmanager-4dc45',
          storageBucket: 'taskmanager-4dc45.appspot.com',
          messagingSenderId: '486225432160',
          appId: '1:486225432160:web:7d80bf27073e1ec8353d05'
        }
      };
      /***/
    },

    /***/
    "./src/main.ts":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function srcMainTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/platform-browser-dynamic */
      "./node_modules/@angular/platform-browser-dynamic/__ivy_ngcc__/fesm2015/platform-browser-dynamic.js");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "./src/app/app.module.ts");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./environments/environment */
      "./src/environments/environment.ts");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
      }

      Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.log(err);
      });
      /***/
    },

    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! /home/john/Github/ionicSandbox/task-manager/src/main.ts */
      "./src/main.ts");
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map