(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["groups-groups-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/add-group/add-group.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/add-group/add-group.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n  <form (ngSubmit)=\"onSubmit(newGroupForm)\" #newGroupForm=\"ngForm\" *ngIf=\"!isLoading\">\n    <ion-item>\n      <ion-label position=\"floating\">Group Name</ion-label>\n      <ion-input type=\"text\" ngModel name=\"name\" required ngModel></ion-input>\n    </ion-item>\n\n    <ion-button type=\"submit\" color=\"primary\" expand=\"block\" [disabled]=\"!newGroupForm.valid\">\n      Create Group\n    </ion-button>\n    <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onCancel()\">\n      Cancel\n    </ion-button>\n\n  </form>\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/groups.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/groups.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Groups</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"menu1\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"onAddGroup()\">\n        <ion-icon name=\"add\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n      <ion-button (click)=\"onRefreshGroups()\">\n        <ion-icon name=\"refresh\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content (ionScroll)=\"onScroll($event)\" [scrollEvents]=\"true\">\n  <ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n  <div *ngIf=\"!isLoading\">\n    <ion-list *ngIf=\"loadedGroups?.length > 0\">\n      <ion-item *ngFor=\"let group of loadedGroups\" detail>\n        <ion-card (click)=\"onClickGroupCard(group)\">\n          <ion-card-header>\n            <ion-card-title>{{ group.name }}</ion-card-title>\n            <ion-card-subtitle>{{ group.accepted ? group.role : 'Click to Manage Membership' }}</ion-card-subtitle>\n          </ion-card-header>\n          <ion-card-content>\n          </ion-card-content>\n        </ion-card>\n      </ion-item>\n    </ion-list>\n    <ion-text *ngIf=\"loadedGroups?.length === 0\">You are not a member of any groups</ion-text>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/groups/add-group/add-group.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/groups/add-group/add-group.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9hZGQtZ3JvdXAvYWRkLWdyb3VwLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/groups/add-group/add-group.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/groups/add-group/add-group.component.ts ***!
  \*********************************************************/
/*! exports provided: AddGroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddGroupComponent", function() { return AddGroupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _groups_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../groups.service */ "./src/app/groups/groups.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");




let AddGroupComponent = class AddGroupComponent {
    constructor(groupsService, popOverController, alertController) {
        this.groupsService = groupsService;
        this.popOverController = popOverController;
        this.alertController = alertController;
    }
    ngOnInit() { }
    onSubmit(addGroupForm) {
        if (!addGroupForm.valid) {
            console.error('Add Group Form Invalid');
            return;
        }
        this.isLoading = true;
        const name = addGroupForm.value.name;
        this.groupsService.addGroup(name)
            .subscribe((responseData) => {
            this.isLoading = false;
            console.log(responseData);
            this.alertController.create({
                header: 'Group Created',
                buttons: ['OK']
            })
                .then((alert) => {
                alert.present()
                    .then((_) => {
                    this.popOverController.dismiss();
                });
            });
        });
    }
    onCancel() {
        this.popOverController.dismiss();
    }
};
AddGroupComponent.ctorParameters = () => [
    { type: _groups_service__WEBPACK_IMPORTED_MODULE_2__["GroupsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"] }
];
AddGroupComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-add-group',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./add-group.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/add-group/add-group.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./add-group.component.scss */ "./src/app/groups/add-group/add-group.component.scss")).default]
    })
], AddGroupComponent);



/***/ }),

/***/ "./src/app/groups/groups-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/groups/groups-routing.module.ts ***!
  \*************************************************/
/*! exports provided: GroupsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsPageRoutingModule", function() { return GroupsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth/auth.guard */ "./src/app/auth/auth.guard.ts");
/* harmony import */ var _profile_profile_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../profile/profile.guard */ "./src/app/profile/profile.guard.ts");
/* harmony import */ var _groups_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./groups.page */ "./src/app/groups/groups.page.ts");






const routes = [
    {
        path: '',
        component: _groups_page__WEBPACK_IMPORTED_MODULE_5__["GroupsPage"]
    },
    {
        path: 'detail',
        loadChildren: () => __webpack_require__.e(/*! import() | group-detail-group-detail-module */ "group-detail-group-detail-module").then(__webpack_require__.bind(null, /*! ./group-detail/group-detail.module */ "./src/app/groups/group-detail/group-detail.module.ts")).then(m => m.GroupDetailPageModule),
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"], _profile_profile_guard__WEBPACK_IMPORTED_MODULE_4__["ProfileGuard"]]
    }
];
let GroupsPageRoutingModule = class GroupsPageRoutingModule {
};
GroupsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GroupsPageRoutingModule);



/***/ }),

/***/ "./src/app/groups/groups.module.ts":
/*!*****************************************!*\
  !*** ./src/app/groups/groups.module.ts ***!
  \*****************************************/
/*! exports provided: GroupsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsPageModule", function() { return GroupsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _groups_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./groups-routing.module */ "./src/app/groups/groups-routing.module.ts");
/* harmony import */ var _groups_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./groups.page */ "./src/app/groups/groups.page.ts");
/* harmony import */ var _add_group_add_group_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./add-group/add-group.component */ "./src/app/groups/add-group/add-group.component.ts");








let GroupsPageModule = class GroupsPageModule {
};
GroupsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _groups_routing_module__WEBPACK_IMPORTED_MODULE_5__["GroupsPageRoutingModule"]
        ],
        declarations: [_groups_page__WEBPACK_IMPORTED_MODULE_6__["GroupsPage"], _add_group_add_group_component__WEBPACK_IMPORTED_MODULE_7__["AddGroupComponent"]]
    })
], GroupsPageModule);



/***/ }),

/***/ "./src/app/groups/groups.page.scss":
/*!*****************************************!*\
  !*** ./src/app/groups/groups.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZ3JvdXBzL2dyb3Vwcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9ncm91cHMvZ3JvdXBzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkIHtcbiAgICB3aWR0aDogMTAwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/groups/groups.page.ts":
/*!***************************************!*\
  !*** ./src/app/groups/groups.page.ts ***!
  \***************************************/
/*! exports provided: GroupsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsPage", function() { return GroupsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _add_group_add_group_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-group/add-group.component */ "./src/app/groups/add-group/add-group.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _groups_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./groups.service */ "./src/app/groups/groups.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _users_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../users/user.service */ "./src/app/users/user.service.ts");






let GroupsPage = class GroupsPage {
    constructor(groupsService, userService, popOverController, gestureController, elementRef, navController, alertController) {
        this.groupsService = groupsService;
        this.userService = userService;
        this.popOverController = popOverController;
        this.gestureController = gestureController;
        this.elementRef = elementRef;
        this.navController = navController;
        this.alertController = alertController;
        this.isLoading = true;
        this.scrollAtTop = true;
        this.scrollAtBottom = false;
    }
    ngOnInit() {
        this.myProfileSubscription = this.userService.getMyProfile()
            .subscribe((myProfile) => {
            console.log(myProfile.identifier);
            this.myProfile = myProfile;
        });
        this.groupsSubscription = this.groupsService.groups
            .subscribe((groups) => {
            this.loadedGroups = groups;
            this.isLoading = false;
            console.log(this.loadedGroups);
        });
        const gestureConfig = {
            gestureName: 'refreshGesture',
            el: this.elementRef.nativeElement,
            direction: 'y',
            threshold: 500,
            canStart: () => {
                return !this.isLoading && (this.scrollAtTop || this.scrollAtBottom);
            },
            onEnd: (event) => {
                const direction = event.currentY - event.startY;
                if ((this.scrollAtTop && direction > 0) ||
                    (this.scrollAtBottom && direction < 0)) {
                    this.isLoading = true;
                    this.groupsService.fetchGroups()
                        .subscribe((_) => { }, (errorResponse) => {
                        console.log(errorResponse);
                        this.isLoading = false;
                    });
                }
            }
        };
        this.gestureController.create(gestureConfig, true)
            .enable();
    }
    ngOnDestroy() {
        if (this.groupsSubscription) {
            console.log("groupsSubscription unsubscribe");
            this.groupsSubscription.unsubscribe();
        }
        if (this.myProfileSubscription) {
            this.myProfileSubscription.unsubscribe();
        }
    }
    ionViewWillEnter() {
        this.isLoading = true;
        this.groupsService.fetchGroups()
            .subscribe((_) => { }, (errorResponse) => {
            console.log(errorResponse);
            this.isLoading = false;
        });
    }
    onAddGroup() {
        this.popOverController.create({
            component: _add_group_add_group_component__WEBPACK_IMPORTED_MODULE_1__["AddGroupComponent"]
        }).then((popOver) => {
            popOver.present();
        });
    }
    onRefreshGroups() {
        this.isLoading = true;
        this.groupsService.fetchGroups()
            .subscribe((_) => { }, (errorResponse) => {
            console.log(errorResponse);
            this.isLoading = false;
        });
    }
    onClickGroupCard(groupModel) {
        console.log(groupModel);
        if (groupModel.accepted) {
            this.navController.navigateForward(['/', 'groups', 'detail', groupModel.identifier]);
        }
        else {
            this.onAcceptGroup(groupModel.identifier);
        }
    }
    onAcceptGroup(groupIdentifier) {
        console.log('onLeaveGroup');
        this.alertController.create({
            header: 'Accept Group Membership?',
            buttons: [
                {
                    text: 'Accept',
                    handler: (_) => {
                        this.isLoading = true;
                        this.groupsService.updateGroupsMemberAccepted(groupIdentifier, true)
                            .subscribe((acceptGroupMembershipResponse) => {
                            console.log(acceptGroupMembershipResponse);
                            this.navController.navigateForward(['/', 'groups', 'detail', groupIdentifier]);
                        }, (errorResponse) => {
                            console.log(errorResponse);
                            this.isLoading = false;
                        });
                    }
                },
                {
                    text: 'Reject',
                    handler: (_) => {
                        this.isLoading = true;
                        this.groupsSubscription.unsubscribe();
                        this.groupsService.deleteGroupsMember(groupIdentifier, this.myProfile.identifier)
                            .subscribe((deleteResponse) => {
                            console.log(deleteResponse);
                            this.isLoading = false;
                        }, (errorResponse) => {
                            console.log(errorResponse);
                            this.isLoading = false;
                        });
                    }
                },
                'Cancel'
            ]
        })
            .then((alert) => {
            alert.present();
        });
    }
    onScroll(event) {
        event.target.getScrollElement()
            .then((scrollElement) => {
            const scrollHeight = scrollElement.scrollHeight - scrollElement.clientHeight;
            const scrollTop = event.detail.scrollTop;
            if (scrollTop === 0) {
                this.scrollAtTop = true;
                this.scrollAtBottom = false;
            }
            else if (scrollTop === scrollHeight) {
                this.scrollAtTop = false;
                this.scrollAtBottom = true;
            }
            else {
                this.scrollAtTop = false;
                this.scrollAtBottom = false;
            }
        });
    }
};
GroupsPage.ctorParameters = () => [
    { type: _groups_service__WEBPACK_IMPORTED_MODULE_3__["GroupsService"] },
    { type: _users_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["GestureController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ElementRef"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
GroupsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-groups',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./groups.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/groups.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./groups.page.scss */ "./src/app/groups/groups.page.scss")).default]
    })
], GroupsPage);



/***/ })

}]);
//# sourceMappingURL=groups-groups-module-es2015.js.map