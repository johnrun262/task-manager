(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/change-password/change-password.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/change-password/change-password.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-grid>\n  <ion-row *ngIf=\"isLoading\">\n    <ion-col>\n      <ion-spinner></ion-spinner>\n    </ion-col>\n  </ion-row>\n\n  <form [formGroup]=\"changePasswordForm\" *ngIf=\"!errorMessage && !isLoading\">\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list>\n          <ion-item>\n            <ion-label position=\"floating\"> Current Password</ion-label>\n            <ion-input type=\"password\" #currentPassword formControlName=\"currentPassword\"\n              (ionFocus)=\"currentPassword.value = ''\">\n            </ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">New Password</ion-label>\n            <ion-input type=\"password\" #newPassword formControlName=\"newPassword\" (ionFocus)=\"newPassword.value = ''\">\n            </ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label position=\"floating\">Confirm New Password</ion-label>\n            <ion-input type=\"password\" #confirmPassword (ionFocus)=\"confirmPassword.value = ''\">\n            </ion-input>\n          </ion-item>\n          <ion-item\n            *ngIf=\"!changePasswordForm.get('newPassword').valid && changePasswordForm.get('newPassword').touched\"\n            lines=\"none\">\n            <ion-text>Enter a Valid Password</ion-text>\n          </ion-item>\n          <ion-item *ngIf=\"changePasswordForm.get('newPassword').valid && newPassword.value != confirmPassword.value\"\n            lines=\"none\">\n            <ion-text>Passwords do not match</ion-text>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-button type=\"button\" color=\"primary\" expand=\"block\"\n          [disabled]=\"!changePasswordForm.valid || newPassword.value != confirmPassword.value\"\n          (click)=\"onChangePassword()\">\n          Change Password\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onCancel()\">\n          Cancel\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </form>\n  <ion-row *ngIf=\"errorMessage && !isLoading\">\n    <ion-col>\n      <ion-text>{{ errorMessage }}</ion-text>\n    </ion-col>\n  </ion-row>\n</ion-grid>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Profile</ion-title>\n    <ion-buttons slot=\"start\" *ngIf=\"!createProfile\">\n      <ion-menu-button menu=\"menu1\"></ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n  <form [formGroup]=\"profileForm\" *ngIf=\"!isLoading\">\n    <ion-list>\n      <ion-item>\n        <ion-text>{{ user }}</ion-text>\n      </ion-item>\n      <ion-item>\n        <ion-text>{{ email }}</ion-text>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\">Name</ion-label>\n        <ion-input type=\"text\" formControlName=\"name\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\">Title</ion-label>\n        <ion-input type=\"text\" formControlName=\"title\"></ion-input>\n      </ion-item>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" [disabled]=\"!profileForm.valid\" (click)=\"onUpdate()\">\n        {{ createProfile ? \"Create\" : \"Update\" }}\n      </ion-button>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onChangePassword()\" *ngIf=\"!createProfile\">\n        Change Password\n      </ion-button>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onDelete()\" *ngIf=\"!createProfile\">\n        Delete\n      </ion-button>\n    </ion-list>\n  </form>\n\n  <ion-text *ngIf=\"errorMessage && !isLoading\">{{ errorMessage }}</ion-text>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/profile/change-password/change-password.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/profile/change-password/change-password.component.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvY2hhbmdlLXBhc3N3b3JkL2NoYW5nZS1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/profile/change-password/change-password.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/profile/change-password/change-password.component.ts ***!
  \**********************************************************************/
/*! exports provided: ChangePasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordComponent", function() { return ChangePasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");





let ChangePasswordComponent = class ChangePasswordComponent {
    constructor(navParams, authService, alertController, popOverController) {
        this.navParams = navParams;
        this.authService = authService;
        this.alertController = alertController;
        this.popOverController = popOverController;
        this.isLoading = false;
    }
    ngOnInit() {
        this.changePasswordForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            currentPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'change',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
            }),
            newPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'change',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8), this.passwordValidator]
            }),
        });
    }
    ngOnDestroy() {
    }
    onChangePassword() {
        console.log('onChangePassword');
        if (!this.changePasswordForm.valid) {
            console.error('not valid');
            return;
        }
        const userId = this.navParams.get('user');
        if (!userId) {
            console.error('no user');
            return;
        }
        this.isLoading = true;
        const currentPassword = this.changePasswordForm.value.currentPassword;
        const newPassword = this.changePasswordForm.value.newPassword;
        this.authService.changePassword(userId, currentPassword, newPassword)
            .then((_) => {
        })
            .catch((errorMessage) => {
            console.error('failMessage ' + errorMessage);
            this.errorMessage = errorMessage;
        })
            .finally(() => {
            this.alertController.create({
                header: 'Password Change',
                message: this.errorMessage ? this.errorMessage : 'Your Password has been Changed',
                buttons: [
                    'OK'
                ]
            }).then((alert) => {
                alert.present()
                    .then((_) => {
                    this.popOverController.dismiss();
                });
            });
            this.isLoading = false;
        });
    }
    onCancel() {
        this.popOverController.dismiss();
    }
    passwordValidator(formControl) {
        const hasNumber = /\d/.test(formControl.value);
        const hasLower = /[A-Z]/.test(formControl.value);
        const hasUpper = /[a-z]/.test(formControl.value);
        if (hasLower && hasUpper && hasNumber) {
            return null;
        }
        return { weak: true };
    }
};
ChangePasswordComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"] },
    { type: _auth_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"] }
];
ChangePasswordComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-change-password',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./change-password.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/change-password/change-password.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./change-password.component.scss */ "./src/app/profile/change-password/change-password.component.scss")).default]
    })
], ChangePasswordComponent);



/***/ }),

/***/ "./src/app/profile/profile-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/profile/profile-routing.module.ts ***!
  \***************************************************/
/*! exports provided: ProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageRoutingModule", function() { return ProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile.page */ "./src/app/profile/profile.page.ts");




const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_3__["ProfilePage"]
    }
];
let ProfilePageRoutingModule = class ProfilePageRoutingModule {
};
ProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProfilePageRoutingModule);



/***/ }),

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./change-password/change-password.component */ "./src/app/profile/change-password/change-password.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile-routing.module */ "./src/app/profile/profile-routing.module.ts");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./profile.page */ "./src/app/profile/profile.page.ts");








let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _profile_routing_module__WEBPACK_IMPORTED_MODULE_6__["ProfilePageRoutingModule"]
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_7__["ProfilePage"], _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_1__["ChangePasswordComponent"]]
    })
], ProfilePageModule);



/***/ }),

/***/ "./src/app/profile/profile.page.scss":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/profile/profile.page.ts":
/*!*****************************************!*\
  !*** ./src/app/profile/profile.page.ts ***!
  \*****************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./change-password/change-password.component */ "./src/app/profile/change-password/change-password.component.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _users_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../users/user.service */ "./src/app/users/user.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");









let ProfilePage = class ProfilePage {
    constructor(userService, authService, router, popOverController, alertController) {
        this.userService = userService;
        this.authService = authService;
        this.router = router;
        this.popOverController = popOverController;
        this.alertController = alertController;
        this.isLoading = true;
        this.createProfile = false;
    }
    ngOnInit() {
        this.profileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"](null, {
                updateOn: 'change',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(80)]
            }),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"](null, {
                updateOn: 'change',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(80)]
            }),
        });
        this.authService.getProfileAttributes()
            .then((attributes) => {
            console.log(attributes);
            this.email = attributes.email;
            this.user = attributes['cognito:username'];
            this.myProfileSubscription = this.userService.getMyProfile()
                .subscribe((myProfile) => {
                if (myProfile) {
                    this.profileForm.patchValue({
                        name: myProfile.name,
                        title: myProfile.title
                    });
                    this.createProfile = false;
                    this.isLoading = false;
                }
                else {
                    this.createProfile = true;
                    this.isLoading = false;
                }
            });
        });
    }
    ngOnDestroy() {
        if (this.myProfileSubscription) {
            this.myProfileSubscription.unsubscribe();
        }
        if (this.didFailSubscription) {
            this.didFailSubscription.unsubscribe();
        }
    }
    ionViewWillEnter() {
        this.isLoading = true;
        this.userService.fetchMyProfile()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])((error) => {
            this.isLoading = false;
            return error;
        }))
            .subscribe((groups) => {
            this.isLoading = false;
        });
    }
    onUpdate() {
        const name = this.profileForm.value.name;
        const title = this.profileForm.value.title;
        console.log(name);
        console.log(title);
        this.isLoading = true;
        if (this.createProfile) {
            this.userService.createMyProfile(this.email, name, title)
                .subscribe((response) => {
                console.log(response);
                this.createProfile = false;
                this.router.navigateByUrl('/tasks');
            }, (error) => {
                console.error(error);
            });
        }
        else {
            this.userService.updateMyProfile(name, title)
                .subscribe((response) => {
                console.log(response);
            }, (error) => {
                console.error(error);
            });
        }
    }
    onDelete() {
        this.alertController.create({
            header: 'Delete Profile',
            message: 'Deleting your Profile will also delete all groups for which you are the sole owner',
            buttons: [
                {
                    text: 'Delete',
                    handler: (_) => this.doDelete()
                },
                'Cancel'
            ]
        }).then((alert) => {
            alert.present();
        });
    }
    doDelete() {
        if (this.user) {
            this.isLoading = true;
            this.userService.deleteMyProfile()
                .subscribe((response) => {
                console.log(response);
                this.createProfile = true;
                this.authService.deleteUser(this.user)
                    .then((_) => {
                    this.router.navigateByUrl('/auth');
                })
                    .catch((errorMessage) => {
                    console.error(errorMessage);
                    this.errorMessage = errorMessage;
                    this.isLoading = false;
                });
            }, (error) => {
                console.error(error);
                this.isLoading = false;
            });
        }
    }
    onChangePassword() {
        this.popOverController.create({
            component: _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_2__["ChangePasswordComponent"],
            componentProps: { user: this.user }
        }).then((popover) => {
            popover.present();
        });
    }
};
ProfilePage.ctorParameters = () => [
    { type: _users_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["AlertController"] }
];
ProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
        selector: 'app-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./profile.page.scss */ "./src/app/profile/profile.page.scss")).default]
    })
], ProfilePage);



/***/ })

}]);
//# sourceMappingURL=profile-profile-module-es2015.js.map