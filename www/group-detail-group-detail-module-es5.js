(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["group-detail-group-detail-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/add-member/add-member.component.html":
    /*!****************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/add-member/add-member.component.html ***!
      \****************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppGroupsGroupDetailAddMemberAddMemberComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-card-title>Add New Member</ion-card-title>\n</ion-header>\n<ion-content>\n  <form #newMemberForm=\"ngForm\">\n    <ion-item>\n      <ion-label position=\"floating\">Email</ion-label>\n      <ion-input type=\"text\" name=\"newMemberEmail\" required email ngModel #newMemberEmail=\"ngModel\"></ion-input>\n    </ion-item>\n    <ion-item *ngIf=\"!newMemberEmail.valid && newMemberEmail.touched\" lines=\"none\">\n      <ion-text>Enter a Valid Email</ion-text>\n    </ion-item>\n    <ion-item>\n      <ion-select name=\"newMemberRole\" #newMemberRole ngModel=\"Member\" value=\"Member\">\n        <ion-select-option value=\"Member\">Member</ion-select-option>\n        <ion-select-option value=\"Manager\">Manager</ion-select-option>\n        <ion-select-option value=\"Owner\" *ngIf=\"loadedGroup.role == 'Owner'\">Owner</ion-select-option>\n      </ion-select>\n    </ion-item>\n    <ion-button type=\"button\" color=\"primary\" expand=\"block\" [disabled]=\"!newMemberForm.valid\"\n      (click)=\"onAddNewMember(newMemberForm)\">\n      Add\n    </ion-button>\n    <ion-button type=\"button\" color=\"primary\" expand=\"block\"\n    (click)=\"onCancel()\">\n    Cancel\n  </ion-button>\n  </form>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/edit-task/edit-task.component.html":
    /*!**************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/edit-task/edit-task.component.html ***!
      \**************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppGroupsGroupDetailEditTaskEditTaskComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n<div *ngIf=\"!isLoading\">\n  <ion-header>\n    <ion-card-title>{{ this.groupTask ? 'Update' : 'Add New' }} Task</ion-card-title>\n  </ion-header>\n  <ion-content>\n    <form #newTaskForm=\"ngForm\">\n      <ion-item>\n        <ion-label position=\"floating\">Name</ion-label>\n        <ion-input type=\"text\" ngModel name=\"name\" required [ngModel]=\"groupTask?.name\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\">Description</ion-label>\n        <ion-input type=\"text\" ngModel name=\"description\" required [ngModel]=\"groupTask?.description\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"floating\">Due Date</ion-label>\n        <ion-datetime display-format=\"MMM DD YYYY\" [min]=\"dueDatePickerStart\" [max]=\"dueDatePickerStop\" name=\"dueDate\"\n          required [ngModel]=\"groupTask?.dueDate.toISOString()\"></ion-datetime>\n      </ion-item>\n      <ion-item *ngIf=\"memberSelectorOptions.length > 1\">\n        <ion-label position=\"floating\">Owner</ion-label>\n        <ion-select required name=\"owner\" [ngModel]=\"groupTask?.ownerIdentifier\">\n          <ion-select-option value=\"{{ option.value}}\" *ngFor=\"let option of memberSelectorOptions\">\n            {{ option.display }}\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" [disabled]=\"!newTaskForm.valid\"\n        (click)=\"onAddTask(newTaskForm)\">\n        {{ this.groupTask ? 'Update' : 'Add' }} Task\n      </ion-button>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onCancel()\">\n        Cancel\n      </ion-button>\n    </form>\n  </ion-content>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/group-detail.page.html":
    /*!**************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/group-detail.page.html ***!
      \**************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppGroupsGroupDetailGroupDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Group Detail</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/groups\"></ion-back-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\" *ngIf=\"!isLoading\">\n      <ion-button (click)=\"onRefreshGroup()\">\n        <ion-icon name=\"refresh\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n      <ion-button (click)=\"onDeleteGroup()\" *ngIf=\"amOwner && loadedGroup.ownerCount == 1\">\n        <ion-icon name=\"trash-outline\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n      <ion-button (click)=\"onLeaveGroup()\" *ngIf=\"!amOwner || loadedGroup.ownerCount != 1\">\n        Leave\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n  <div *ngIf=\"!isLoading\">\n\n    <ion-card *ngIf=\"!isEdit\">\n      <ion-card-header>\n        <ion-card-title>Info</ion-card-title>\n      </ion-card-header>\n      <ion-card-subtitle>\n        {{ loadedGroup.name }}\n      </ion-card-subtitle>\n      <ion-card-content>\n        <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"toggleEditGroup()\"\n          *ngIf=\"amOwner || amManager\">\n          Edit\n        </ion-button>\n      </ion-card-content>\n    </ion-card>\n\n    <div *ngIf=\"isEdit\">\n      <ion-card>\n        <ion-card-header>\n          <ion-card-title>Info</ion-card-title>\n        </ion-card-header>\n        <ion-card-content>\n          <form #editForm=\"ngForm\">\n            <ion-item>\n              <ion-label position=\"floating\">Name</ion-label>\n              <ion-input type=\"text\" name=\"groupName\" #groupName required [ngModel]=\"loadedGroup.name\">\n              </ion-input>\n            </ion-item>\n            <ion-button type=\"button\" color=\"primary\" expand=\"block\" [disabled]=\"!editForm.valid\"\n              (click)=\"onEditGroup(editForm)\">\n              Save\n            </ion-button>\n          </form>\n        </ion-card-content>\n      </ion-card>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onAddMember()\">\n        Add New Member\n      </ion-button>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onAddTask(editForm)\">\n        Add New Task\n      </ion-button>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"toggleEditGroup()\">\n        Cancel\n      </ion-button>\n    </div>\n\n    <hr>\n\n    <div *ngIf=\"!showMembers\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"col-xs-1\">\n            <ion-icon name=\"caret-forward-outline\" (click)=\"toggleShowMembers()\"></ion-icon>\n          </ion-col>\n          <ion-col>\n            <ion-text>Members</ion-text>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n\n    <ion-list *ngIf=\"showMembers\">\n      <ion-list-header>\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"col-xs-1\">\n              <ion-icon name=\"caret-down-outline\" (click)=\"toggleShowMembers()\"></ion-icon>\n            </ion-col>\n            <ion-col>\n              <ion-text>Members</ion-text>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-list-header>\n\n      <div *ngFor=\"let member of loadedGroup.members\">\n        <app-show-member [groupMember]=\"member\" [loadedGroup]=\"loadedGroup\"></app-show-member>\n      </div>\n    </ion-list>\n\n    <div *ngIf=\"!showTasks\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"col-xs-1\">\n            <ion-icon name=\"caret-forward-outline\" (click)=\"toggleShowTasks()\"></ion-icon>\n          </ion-col>\n          <ion-col>\n            <ion-text>Tasks</ion-text>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n\n    <ion-list *ngIf=\"showTasks\">\n      <ion-list-header>\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"col-xs-1\">\n              <ion-icon name=\"caret-down-outline\" (click)=\"toggleShowTasks()\"></ion-icon>\n            </ion-col>\n            <ion-col>\n              <ion-text>Tasks</ion-text>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-list-header>\n\n      <div *ngFor=\"let task of loadedGroup.tasks\">\n        <app-show-task [groupTask]=\"task\" [loadedGroup]=\"loadedGroup\"></app-show-task>\n      </div>\n    </ion-list>\n  </div>\n\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-member/show-member.component.html":
    /*!******************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-member/show-member.component.html ***!
      \******************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppGroupsGroupDetailShowMemberShowMemberComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n<ion-card *ngIf=\"!isLoading\">\n  <ion-card-header>\n    <ion-card-title>{{ groupMember.name }}</ion-card-title>\n    <ion-card-subtitle>{{ groupMember.email }}</ion-card-subtitle>\n  </ion-card-header>\n\n  <ion-card-content>\n    <ion-grid>\n\n      <div\n        *ngIf=\"(loadedGroup.role == 'Owner' || (loadedGroup.role == 'Manager' && groupMember.role != 'Owner')) && groupMember.identifier != myProfile.identifier; else cannotChangeMemberRole\">\n        <ion-row>\n          <ion-col>\n            <ion-text>{{ groupMember.title }}</ion-text>\n          </ion-col>\n          <ion-col>\n            <ion-select value=\"{{ groupMember.role }}\" #updateMemberRole\n              (ionChange)=\"onUpdateGroupMember(groupMember.identifier, updateMemberRole.value)\">\n              <ion-select-option value=\"Member\">Member</ion-select-option>\n              <ion-select-option value=\"Manager\">Manager</ion-select-option>\n              <ion-select-option value=\"Owner\" *ngIf=\"loadedGroup.role == 'Owner'\">Owner</ion-select-option>\n            </ion-select>\n          </ion-col>\n          <ion-col>\n            <ion-icon name=\"trash-outline\" (click)=\"onDeleteGroupMember()\"></ion-icon>\n          </ion-col>\n        </ion-row>\n      </div>\n\n      <ng-template #cannotChangeMemberRole>\n        <ion-row>\n          <ion-col>\n            <ion-text>{{ groupMember.title }}</ion-text>\n          </ion-col>\n          <ion-col>\n            <ion-text>{{ groupMember.role }}</ion-text>\n          </ion-col>\n        </ion-row>\n      </ng-template>\n\n      <ion-row *ngIf=\"!groupMember.accepted\">\n        <ion-col>\n          <ion-text>\n            Awaiting Group Member Acceptance\n          </ion-text>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n  </ion-card-content>\n</ion-card>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-task/show-task.component.html":
    /*!**************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-task/show-task.component.html ***!
      \**************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppGroupsGroupDetailShowTaskShowTaskComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n<ion-card *ngIf=\"!isLoading\">\n  <ion-card-header>\n    <ion-card-title>{{ groupTask.name }}</ion-card-title>\n    <ion-card-subtitle>Due: {{ groupTask.dueDate.toLocaleDateString() }}</ion-card-subtitle>\n    <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-text>{{ groupTask.description }}</ion-text>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf=\"loadedGroup.role != 'Owner' && loadedGroup.role != 'Manager'; else isOwnerOrManager\">\n          <ion-col>\n            Owner:\n          </ion-col>\n          <ion-col>\n            <ion-text>{{ taskOwner.name }}</ion-text>\n          </ion-col>\n          <ion-col>\n            <ion-text>{{ taskOwner.email }}</ion-text>\n          </ion-col>\n          <ion-col>\n            <ion-text>{{ groupTask.status }}</ion-text>\n          </ion-col>\n        </ion-row>\n        <ng-template #isOwnerOrManager>\n          <ion-row>\n            <ion-col>\n              Owner:\n            </ion-col>\n            <ion-col>\n              <ion-select value=\"{{ taskOwner.identifier }}\" #changeOwner\n                *ngIf=\"memberSelectorOptions.length > 1 && groupTask.status != 'Verfiy' && groupTask.status != 'Complete'; else cannotChangeTaskOwner\"\n                (ionChange)=\"onChangeTaskOwner(changeOwner.value)\">\n                <ion-select-option value=\"{{ option.value}}\" *ngFor=\"let option of memberSelectorOptions\">\n                  {{ option.display }}\n                </ion-select-option>\n              </ion-select>\n              <ng-template #cannotChangeTaskOwner>\n                <ion-col>\n                  <ion-text>{{ taskOwner.name }}</ion-text>\n                </ion-col>\n                <ion-col>\n                  <ion-text>{{ taskOwner.email }}</ion-text>\n                </ion-col>\n              </ng-template>\n            </ion-col>\n            <ion-col>\n              <ion-button type=\"button\" color=\"primary\" expand=\"block\"\n                *ngIf=\"groupTask.status == 'Verify'; else notVerify\" (click)=\"onChangeTaskStatus('Complete')\">\n                Verify\n              </ion-button>\n              <ng-template #notVerify>\n                <ion-text>{{ groupTask.status }}</ion-text>\n              </ng-template>\n            </ion-col>\n            <ion-col size=\"col-xs-1\">\n              <ion-icon name=\"build-outline\" (click)=\"onEditTask()\"></ion-icon>\n            </ion-col>\n            <ion-col size=\"col-xs-1\">\n              <ion-icon name=\"trash-outline\" (click)=\"onDeleteTask()\"></ion-icon>\n            </ion-col>\n          </ion-row>\n        </ng-template>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card-header>\n</ion-card>";
      /***/
    },

    /***/
    "./src/app/groups/group-detail/add-member/add-member.component.scss":
    /*!**************************************************************************!*\
      !*** ./src/app/groups/group-detail/add-member/add-member.component.scss ***!
      \**************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppGroupsGroupDetailAddMemberAddMemberComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvYWRkLW1lbWJlci9hZGQtbWVtYmVyLmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/groups/group-detail/add-member/add-member.component.ts":
    /*!************************************************************************!*\
      !*** ./src/app/groups/group-detail/add-member/add-member.component.ts ***!
      \************************************************************************/

    /*! exports provided: AddMemberComponent */

    /***/
    function srcAppGroupsGroupDetailAddMemberAddMemberComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddMemberComponent", function () {
        return AddMemberComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _groups_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../../groups.service */
      "./src/app/groups/groups.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AddMemberComponent = /*#__PURE__*/function () {
        function AddMemberComponent(popoverController, alertController, navParams, groupsService) {
          _classCallCheck(this, AddMemberComponent);

          this.popoverController = popoverController;
          this.alertController = alertController;
          this.navParams = navParams;
          this.groupsService = groupsService;
        }

        _createClass(AddMemberComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.loadedGroup = this.navParams.get('group');
          }
        }, {
          key: "onAddNewMember",
          value: function onAddNewMember(editForm) {
            var _this = this;

            var newMemberEmail = editForm.value.newMemberEmail;
            var newMemberRole = editForm.value.newMemberRole;
            console.log(newMemberEmail);
            console.log(newMemberRole);

            if (!newMemberEmail || newMemberEmail.length === 0) {
              console.error('Edit Form Email Not Valid');
              return;
            }

            if (!newMemberRole || newMemberRole.length === 0) {
              console.error('Edit Form Role Not Valid');
              return;
            }

            var duplicate = false;
            this.loadedGroup.members.forEach(function (member) {
              if (member.email === newMemberEmail) {
                duplicate = true;
              }
            });

            if (duplicate) {
              this.alertController.create({
                header: 'Member Already in Group',
                buttons: ['OK']
              }).then(function (alert) {
                _this.popoverController.dismiss();

                alert.present();
              });
              return;
            }

            this.groupsService.addGroupMember(this.loadedGroup.identifier, newMemberEmail, newMemberRole).subscribe(function (response) {
              console.log(response);

              _this.alertController.create({
                header: 'New Member Added',
                buttons: ['OK']
              }).then(function (alert) {
                _this.popoverController.dismiss();

                alert.present();
              });
            });
          }
        }, {
          key: "onCancel",
          value: function onCancel() {
            this.popoverController.dismiss();
          }
        }]);

        return AddMemberComponent;
      }();

      AddMemberComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
        }, {
          type: _groups_service__WEBPACK_IMPORTED_MODULE_1__["GroupsService"]
        }];
      };

      AddMemberComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-add-member',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./add-member.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/add-member/add-member.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./add-member.component.scss */
        "./src/app/groups/group-detail/add-member/add-member.component.scss"))["default"]]
      })], AddMemberComponent);
      /***/
    },

    /***/
    "./src/app/groups/group-detail/edit-task/edit-task.component.scss":
    /*!************************************************************************!*\
      !*** ./src/app/groups/group-detail/edit-task/edit-task.component.scss ***!
      \************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppGroupsGroupDetailEditTaskEditTaskComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvZWRpdC10YXNrL2VkaXQtdGFzay5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/groups/group-detail/edit-task/edit-task.component.ts":
    /*!**********************************************************************!*\
      !*** ./src/app/groups/group-detail/edit-task/edit-task.component.ts ***!
      \**********************************************************************/

    /*! exports provided: EditTaskComponent */

    /***/
    function srcAppGroupsGroupDetailEditTaskEditTaskComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditTaskComponent", function () {
        return EditTaskComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _groups_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../groups.service */
      "./src/app/groups/groups.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var EditTaskComponent = /*#__PURE__*/function () {
        function EditTaskComponent(groupsService, popoverController, alertController, navParams) {
          _classCallCheck(this, EditTaskComponent);

          this.groupsService = groupsService;
          this.popoverController = popoverController;
          this.alertController = alertController;
          this.navParams = navParams;
          this.dueDatePickerStart = new Date(Date.now()).toISOString();
          this.dueDatePickerStop = '' + (new Date(Date.now()).getFullYear() + 5);
          this.isLoading = true;
        }

        _createClass(EditTaskComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.loadedGroup = this.navParams.get('group');
            this.groupTask = this.navParams.get('task');
            console.log(this.groupTask);
            this.memberSelectorOptions = this.loadedGroup.getMemberSelectorOptions();
            this.isLoading = false;
          }
        }, {
          key: "onAddTask",
          value: function onAddTask(newTaskForm) {
            var _this2 = this;

            if (!newTaskForm.valid) {
              console.log('new task for not valid');
              return;
            }

            this.isLoading = true;
            var name = newTaskForm.value.name;
            var description = newTaskForm.value.description;
            var dueDate = newTaskForm.value.dueDate;
            var owner = newTaskForm.value.owner;

            if (!owner) {
              owner = this.memberSelectorOptions[0].value;
            }

            if (this.groupTask) {
              this.groupsService.updateTask(this.loadedGroup.identifier, this.groupTask.identifier, name, description, new Date(dueDate), owner, false).subscribe(function (addTaskResponse) {
                console.log(addTaskResponse);

                _this2.alertController.create({
                  header: 'Task Updated',
                  buttons: ['OK']
                }).then(function (alert) {
                  _this2.popoverController.dismiss();

                  alert.present();
                });
              });
            } else {
              this.groupsService.addTask(this.loadedGroup.identifier, name, description, new Date(dueDate), owner, false).subscribe(function (addTaskResponse) {
                console.log(addTaskResponse);

                _this2.alertController.create({
                  header: 'Task Created',
                  buttons: ['OK']
                }).then(function (alert) {
                  _this2.popoverController.dismiss();

                  alert.present();
                });
              });
            }
          }
        }, {
          key: "onCancel",
          value: function onCancel() {
            this.popoverController.dismiss();
          }
        }]);

        return EditTaskComponent;
      }();

      EditTaskComponent.ctorParameters = function () {
        return [{
          type: _groups_service__WEBPACK_IMPORTED_MODULE_1__["GroupsService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
        }];
      };

      EditTaskComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-add-task',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./edit-task.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/edit-task/edit-task.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./edit-task.component.scss */
        "./src/app/groups/group-detail/edit-task/edit-task.component.scss"))["default"]]
      })], EditTaskComponent);
      /***/
    },

    /***/
    "./src/app/groups/group-detail/group-detail-routing.module.ts":
    /*!********************************************************************!*\
      !*** ./src/app/groups/group-detail/group-detail-routing.module.ts ***!
      \********************************************************************/

    /*! exports provided: GroupDetailPageRoutingModule */

    /***/
    function srcAppGroupsGroupDetailGroupDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GroupDetailPageRoutingModule", function () {
        return GroupDetailPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var src_app_auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/auth/auth.guard */
      "./src/app/auth/auth.guard.ts");
      /* harmony import */


      var src_app_profile_profile_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/profile/profile.guard */
      "./src/app/profile/profile.guard.ts");
      /* harmony import */


      var _group_detail_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./group-detail.page */
      "./src/app/groups/group-detail/group-detail.page.ts");

      var routes = [{
        path: ':groupIdentifier',
        component: _group_detail_page__WEBPACK_IMPORTED_MODULE_5__["GroupDetailPage"],
        canLoad: [src_app_auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"], src_app_profile_profile_guard__WEBPACK_IMPORTED_MODULE_4__["ProfileGuard"]]
      }];

      var GroupDetailPageRoutingModule = function GroupDetailPageRoutingModule() {
        _classCallCheck(this, GroupDetailPageRoutingModule);
      };

      GroupDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], GroupDetailPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/groups/group-detail/group-detail.module.ts":
    /*!************************************************************!*\
      !*** ./src/app/groups/group-detail/group-detail.module.ts ***!
      \************************************************************/

    /*! exports provided: GroupDetailPageModule */

    /***/
    function srcAppGroupsGroupDetailGroupDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GroupDetailPageModule", function () {
        return GroupDetailPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _group_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./group-detail-routing.module */
      "./src/app/groups/group-detail/group-detail-routing.module.ts");
      /* harmony import */


      var _group_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./group-detail.page */
      "./src/app/groups/group-detail/group-detail.page.ts");
      /* harmony import */


      var _show_member_show_member_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./show-member/show-member.component */
      "./src/app/groups/group-detail/show-member/show-member.component.ts");
      /* harmony import */


      var _add_member_add_member_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./add-member/add-member.component */
      "./src/app/groups/group-detail/add-member/add-member.component.ts");
      /* harmony import */


      var _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./edit-task/edit-task.component */
      "./src/app/groups/group-detail/edit-task/edit-task.component.ts");
      /* harmony import */


      var _show_task_show_task_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./show-task/show-task.component */
      "./src/app/groups/group-detail/show-task/show-task.component.ts");

      var GroupDetailPageModule = function GroupDetailPageModule() {
        _classCallCheck(this, GroupDetailPageModule);
      };

      GroupDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _group_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["GroupDetailPageRoutingModule"]],
        declarations: [_group_detail_page__WEBPACK_IMPORTED_MODULE_6__["GroupDetailPage"], _add_member_add_member_component__WEBPACK_IMPORTED_MODULE_8__["AddMemberComponent"], _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_9__["EditTaskComponent"], _show_member_show_member_component__WEBPACK_IMPORTED_MODULE_7__["ShowMemberComponent"], _show_task_show_task_component__WEBPACK_IMPORTED_MODULE_10__["ShowTaskComponent"]]
      })], GroupDetailPageModule);
      /***/
    },

    /***/
    "./src/app/groups/group-detail/group-detail.page.scss":
    /*!************************************************************!*\
      !*** ./src/app/groups/group-detail/group-detail.page.scss ***!
      \************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppGroupsGroupDetailGroupDetailPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvZ3JvdXAtZGV0YWlsLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/groups/group-detail/group-detail.page.ts":
    /*!**********************************************************!*\
      !*** ./src/app/groups/group-detail/group-detail.page.ts ***!
      \**********************************************************/

    /*! exports provided: GroupDetailPage */

    /***/
    function srcAppGroupsGroupDetailGroupDetailPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GroupDetailPage", function () {
        return GroupDetailPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _groups_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../groups.service */
      "./src/app/groups/groups.service.ts");
      /* harmony import */


      var _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./edit-task/edit-task.component */
      "./src/app/groups/group-detail/edit-task/edit-task.component.ts");
      /* harmony import */


      var _add_member_add_member_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./add-member/add-member.component */
      "./src/app/groups/group-detail/add-member/add-member.component.ts");

      var GroupDetailPage = /*#__PURE__*/function () {
        function GroupDetailPage(groupsService, userService, activatedRoute, navController, alertController, popoverController, gestureController, elementRef) {
          _classCallCheck(this, GroupDetailPage);

          this.groupsService = groupsService;
          this.userService = userService;
          this.activatedRoute = activatedRoute;
          this.navController = navController;
          this.alertController = alertController;
          this.popoverController = popoverController;
          this.gestureController = gestureController;
          this.elementRef = elementRef;
          this.amOwner = false;
          this.amManager = false;
          this.isEdit = false;
          this.showMembers = false;
          this.showTasks = false;
          this.ownerCount = 0;
        }

        _createClass(GroupDetailPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this3 = this;

            this.activatedRoute.paramMap.subscribe(function (paramMap) {
              if (!paramMap.has('groupIdentifier')) {
                _this3.navController.navigateBack('/groups');
              }

              _this3.isLoading = true;
              _this3.groupIdentifier = paramMap.get('groupIdentifier');
              console.log(_this3.groupIdentifier);
              _this3.myProfileSubscription = _this3.userService.getMyProfile().subscribe(function (myProfile) {
                console.log(myProfile.identifier);
                _this3.myProfile = myProfile;

                _this3.groupsService.getGroupByIdentifier(_this3.groupIdentifier).subscribe(function (groupModel) {
                  console.log(groupModel);
                  _this3.loadedGroup = groupModel;

                  if (_this3.loadedGroup.role === 'Owner') {
                    _this3.amOwner = true;
                  } else if (_this3.loadedGroup.role === 'Manager') {
                    _this3.amManager = true;
                  }

                  _this3.isLoading = false;
                }, function (errorResponse) {
                  console.log(errorResponse);

                  _this3.navController.navigateBack('/groups');
                });
              });
            });
            var gestureConfig = {
              gestureName: 'refreshGesture',
              el: this.elementRef.nativeElement,
              direction: 'y',
              blurOnStart: true,
              threshold: 500,
              canStart: function canStart() {
                return !_this3.isEdit && !_this3.isLoading;
              },
              onEnd: function onEnd(event) {
                console.log(event);
                _this3.isLoading = true;

                _this3.groupsService.getGroupByIdentifier(_this3.groupIdentifier).subscribe(function (groupModel) {
                  console.log(groupModel);
                  _this3.loadedGroup = groupModel;

                  if (_this3.loadedGroup.role === 'Owner') {
                    _this3.amOwner = true;
                  } else if (_this3.loadedGroup.role === 'Manager') {
                    _this3.amManager = true;
                  }

                  _this3.isLoading = false;
                }, function (errorResponse) {
                  console.log(errorResponse);

                  _this3.navController.navigateBack('/groups');
                });
              }
            };
            this.gestureController.create(gestureConfig, true).enable();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.groupsSubscription) {
              this.groupsSubscription.unsubscribe();
            }

            if (this.myProfileSubscription) {
              this.myProfileSubscription.unsubscribe();
            }
          }
        }, {
          key: "onDeleteGroup",
          value: function onDeleteGroup() {
            var _this4 = this;

            console.log('onDeleteGroup');
            this.alertController.create({
              header: 'Delete Group',
              message: 'Deleting the Group will also delete all tasks.',
              buttons: [{
                text: 'Delete',
                handler: function handler(_) {
                  _this4.groupsSubscription.unsubscribe();

                  _this4.groupsService.deleteGroup(_this4.loadedGroup.identifier).subscribe(function (response) {
                    console.log(response);

                    _this4.navController.navigateBack('/groups');
                  }, function (errorResponse) {
                    console.log(errorResponse);

                    _this4.navController.navigateBack('/groups');
                  });
                }
              }, 'Cancel']
            }).then(function (alert) {
              alert.present();
            });
          }
        }, {
          key: "toggleEditGroup",
          value: function toggleEditGroup() {
            this.isEdit = !this.isEdit;
          }
        }, {
          key: "onEditGroup",
          value: function onEditGroup(editForm) {
            var _this5 = this;

            var groupName = editForm.value.groupName;
            console.log(groupName);

            if (!groupName || groupName.length === 0) {
              console.error('Edit Form Group Name Not Valid');
              return;
            }

            this.groupsService.editGroup(this.loadedGroup.identifier, groupName).subscribe(function (editResponse) {
              console.log(editResponse);
              _this5.loadedGroup = editResponse;
              _this5.isEdit = false;
            }, function (errorResponse) {
              console.log(errorResponse);
              _this5.isEdit = false;

              _this5.navController.navigateBack('/groups');
            });
            editForm.reset();
          }
        }, {
          key: "onRefreshGroup",
          value: function onRefreshGroup() {
            var _this6 = this;

            this.isLoading = true;
            this.groupsService.getGroupByIdentifier(this.groupIdentifier).subscribe(function (groupModel) {
              console.log(groupModel);
              _this6.loadedGroup = groupModel;

              if (_this6.loadedGroup.role === 'Owner') {
                _this6.amOwner = true;
              } else if (_this6.loadedGroup.role === 'Manager') {
                _this6.amManager = true;
              }

              _this6.isLoading = false;
            }, function (errorResponse) {
              console.log(errorResponse);

              _this6.navController.navigateBack('/groups');
            });
          }
        }, {
          key: "onLeaveGroup",
          value: function onLeaveGroup() {
            var _this7 = this;

            console.log('onLeaveGroup');
            this.alertController.create({
              header: 'Leave Group?',
              buttons: [{
                text: 'Leave',
                handler: function handler(_) {
                  _this7.isLoading = true;

                  _this7.groupsSubscription.unsubscribe();

                  _this7.groupsService.deleteGroupsMember(_this7.loadedGroup.identifier, _this7.myProfile.identifier).subscribe(function (deleteResponse) {
                    console.log(deleteResponse);
                    _this7.isLoading = false;

                    _this7.navController.navigateBack('/groups');
                  }, function (errorResponse) {
                    console.log(errorResponse);
                    _this7.isLoading = false;

                    _this7.navController.navigateBack('/groups');
                  });
                }
              }, 'Cancel']
            }).then(function (alert) {
              alert.present();
            });
          }
        }, {
          key: "onAddMember",
          value: function onAddMember() {
            this.popoverController.create({
              component: _add_member_add_member_component__WEBPACK_IMPORTED_MODULE_7__["AddMemberComponent"],
              componentProps: {
                group: this.loadedGroup
              }
            }).then(function (popover) {
              popover.present();
            });
          }
        }, {
          key: "onAddTask",
          value: function onAddTask() {
            this.popoverController.create({
              component: _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_6__["EditTaskComponent"],
              componentProps: {
                group: this.loadedGroup
              }
            }).then(function (popover) {
              popover.present();
            });
          }
        }, {
          key: "toggleShowMembers",
          value: function toggleShowMembers() {
            this.showMembers = !this.showMembers;
          }
        }, {
          key: "toggleShowTasks",
          value: function toggleShowTasks() {
            this.showTasks = !this.showTasks;
          }
        }]);

        return GroupDetailPage;
      }();

      GroupDetailPage.ctorParameters = function () {
        return [{
          type: _groups_service__WEBPACK_IMPORTED_MODULE_5__["GroupsService"]
        }, {
          type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }];
      };

      GroupDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-group-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./group-detail.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/group-detail.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./group-detail.page.scss */
        "./src/app/groups/group-detail/group-detail.page.scss"))["default"]]
      })], GroupDetailPage);
      /***/
    },

    /***/
    "./src/app/groups/group-detail/show-member/show-member.component.scss":
    /*!****************************************************************************!*\
      !*** ./src/app/groups/group-detail/show-member/show-member.component.scss ***!
      \****************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppGroupsGroupDetailShowMemberShowMemberComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvc2hvdy1tZW1iZXIvc2hvdy1tZW1iZXIuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/groups/group-detail/show-member/show-member.component.ts":
    /*!**************************************************************************!*\
      !*** ./src/app/groups/group-detail/show-member/show-member.component.ts ***!
      \**************************************************************************/

    /*! exports provided: ShowMemberComponent */

    /***/
    function srcAppGroupsGroupDetailShowMemberShowMemberComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ShowMemberComponent", function () {
        return ShowMemberComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../../../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _groups_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../groups.service */
      "./src/app/groups/groups.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ShowMemberComponent = /*#__PURE__*/function () {
        function ShowMemberComponent(groupsService, alertController, userService) {
          _classCallCheck(this, ShowMemberComponent);

          this.groupsService = groupsService;
          this.alertController = alertController;
          this.userService = userService;
          this.isLoading = true;
        }

        _createClass(ShowMemberComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this8 = this;

            this.myProfileSubscription = this.userService.getMyProfile().subscribe(function (myProfile) {
              _this8.myProfile = myProfile;
              console.log(_this8.myProfile);
              _this8.isLoading = false;
            });
            console.log(this.groupMember);
            console.log(this.loadedGroup);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.myProfileSubscription) {
              this.myProfileSubscription.unsubscribe();
            }
          }
        }, {
          key: "onUpdateGroupMember",
          value: function onUpdateGroupMember(userIdentifier, role) {
            console.log(userIdentifier);
            console.log(role);
            this.groupsService.updateGroupsMember(this.loadedGroup.identifier, userIdentifier, role).subscribe(function (updateResponse) {
              console.log(updateResponse);
            });
          }
        }, {
          key: "onDeleteGroupMember",
          value: function onDeleteGroupMember() {
            var _this9 = this;

            console.log(this.groupMember.identifier);
            this.alertController.create({
              header: 'Remove Member from the Group?',
              buttons: [{
                text: 'Remove',
                handler: function handler(_) {
                  _this9.isLoading = true;

                  _this9.groupsService.deleteGroupsMember(_this9.loadedGroup.identifier, _this9.groupMember.identifier).subscribe(function (deleteResponse) {
                    console.log(deleteResponse);

                    _this9.alertController.create({
                      header: 'Member Removed from Group',
                      buttons: ['OK']
                    }).then(function (alert) {
                      _this9.isLoading = false;
                      alert.present();
                    });
                  });
                }
              }, 'Cancel']
            }).then(function (alert) {
              alert.present();
            });
          }
        }]);

        return ShowMemberComponent;
      }();

      ShowMemberComponent.ctorParameters = function () {
        return [{
          type: _groups_service__WEBPACK_IMPORTED_MODULE_2__["GroupsService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _users_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
        }];
      };

      ShowMemberComponent.propDecorators = {
        groupMember: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"]
        }],
        loadedGroup: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"]
        }]
      };
      ShowMemberComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-show-member',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./show-member.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-member/show-member.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./show-member.component.scss */
        "./src/app/groups/group-detail/show-member/show-member.component.scss"))["default"]]
      })], ShowMemberComponent);
      /***/
    },

    /***/
    "./src/app/groups/group-detail/show-task/show-task.component.scss":
    /*!************************************************************************!*\
      !*** ./src/app/groups/group-detail/show-task/show-task.component.scss ***!
      \************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppGroupsGroupDetailShowTaskShowTaskComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9ncm91cC1kZXRhaWwvc2hvdy10YXNrL3Nob3ctdGFzay5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/groups/group-detail/show-task/show-task.component.ts":
    /*!**********************************************************************!*\
      !*** ./src/app/groups/group-detail/show-task/show-task.component.ts ***!
      \**********************************************************************/

    /*! exports provided: ShowTaskComponent */

    /***/
    function srcAppGroupsGroupDetailShowTaskShowTaskComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ShowTaskComponent", function () {
        return ShowTaskComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _groups_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../groups.service */
      "./src/app/groups/groups.service.ts");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../../users/user.service */
      "./src/app/users/user.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../edit-task/edit-task.component */
      "./src/app/groups/group-detail/edit-task/edit-task.component.ts");

      var ShowTaskComponent = /*#__PURE__*/function () {
        function ShowTaskComponent(groupsService, userService, popoverController, alertController) {
          _classCallCheck(this, ShowTaskComponent);

          this.groupsService = groupsService;
          this.userService = userService;
          this.popoverController = popoverController;
          this.alertController = alertController;
          this.isLoading = true;
        }

        _createClass(ShowTaskComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this10 = this;

            this.myProfileSubscription = this.userService.getMyProfile().subscribe(function (myProfile) {
              _this10.myProfile = myProfile;
              console.log(_this10.myProfile);
              _this10.isLoading = false;
            });
            this.taskOwner = this.loadedGroup.getMember(this.groupTask.ownerIdentifier);
            this.memberSelectorOptions = this.loadedGroup.getMemberSelectorOptions();
            console.log(this.groupTask);
            console.log(this.taskOwner);
            console.log(this.loadedGroup);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.myProfileSubscription) {
              this.myProfileSubscription.unsubscribe();
            }
          }
        }, {
          key: "onChangeTaskOwner",
          value: function onChangeTaskOwner(newOwnerIdentifier) {
            var _this11 = this;

            if (newOwnerIdentifier === this.groupTask.ownerIdentifier) {
              return;
            }

            this.isLoading = true;
            this.groupsService.changeTaskOwner(this.loadedGroup.identifier, this.groupTask.identifier, newOwnerIdentifier).subscribe(function (response) {
              console.log(response);
              _this11.isLoading = false;
            });
          }
        }, {
          key: "onChangeTaskStatus",
          value: function onChangeTaskStatus(newStatus) {
            var _this12 = this;

            if (newStatus === this.groupTask.status) {
              return;
            }

            this.isLoading = true;
            this.groupsService.changeTaskStatus(this.loadedGroup.identifier, this.groupTask.identifier, newStatus).subscribe(function (response) {
              console.log(response);
              _this12.isLoading = false;
            });
          }
        }, {
          key: "onEditTask",
          value: function onEditTask() {
            this.popoverController.create({
              component: _edit_task_edit_task_component__WEBPACK_IMPORTED_MODULE_5__["EditTaskComponent"],
              componentProps: {
                group: this.loadedGroup,
                task: this.groupTask
              }
            }).then(function (popover) {
              popover.present();
            });
          }
        }, {
          key: "onDeleteTask",
          value: function onDeleteTask() {
            var _this13 = this;

            this.alertController.create({
              header: 'Delete Task?',
              buttons: [{
                text: 'Delete',
                handler: function handler(_) {
                  _this13.isLoading = true;

                  _this13.groupsService.deleteTask(_this13.loadedGroup.identifier, _this13.groupTask.identifier).subscribe(function (deleteResult) {
                    console.log('delete');
                    _this13.isLoading = false;
                  });
                }
              }, 'Cancel']
            }).then(function (alert) {
              alert.present();
            });
          }
        }]);

        return ShowTaskComponent;
      }();

      ShowTaskComponent.ctorParameters = function () {
        return [{
          type: _groups_service__WEBPACK_IMPORTED_MODULE_2__["GroupsService"]
        }, {
          type: _users_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"]
        }];
      };

      ShowTaskComponent.propDecorators = {
        groupTask: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"]
        }],
        loadedGroup: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"]
        }]
      };
      ShowTaskComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-show-task',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./show-task.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/group-detail/show-task/show-task.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./show-task.component.scss */
        "./src/app/groups/group-detail/show-task/show-task.component.scss"))["default"]]
      })], ShowTaskComponent);
      /***/
    }
  }]);
})();
//# sourceMappingURL=group-detail-group-detail-module-es5.js.map