(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["groups-groups-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/add-group/add-group.component.html":
    /*!*************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/add-group/add-group.component.html ***!
      \*************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppGroupsAddGroupAddGroupComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n  <form (ngSubmit)=\"onSubmit(newGroupForm)\" #newGroupForm=\"ngForm\" *ngIf=\"!isLoading\">\n    <ion-item>\n      <ion-label position=\"floating\">Group Name</ion-label>\n      <ion-input type=\"text\" ngModel name=\"name\" required ngModel></ion-input>\n    </ion-item>\n\n    <ion-button type=\"submit\" color=\"primary\" expand=\"block\" [disabled]=\"!newGroupForm.valid\">\n      Create Group\n    </ion-button>\n    <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onCancel()\">\n      Cancel\n    </ion-button>\n\n  </form>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/groups.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/groups/groups.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppGroupsGroupsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Groups</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"menu1\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"onAddGroup()\">\n        <ion-icon name=\"add\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n      <ion-button (click)=\"onRefreshGroups()\">\n        <ion-icon name=\"refresh\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content (ionScroll)=\"onScroll($event)\" [scrollEvents]=\"true\">\n  <ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n  <div *ngIf=\"!isLoading\">\n    <ion-list *ngIf=\"loadedGroups?.length > 0\">\n      <ion-item *ngFor=\"let group of loadedGroups\" detail>\n        <ion-card (click)=\"onClickGroupCard(group)\">\n          <ion-card-header>\n            <ion-card-title>{{ group.name }}</ion-card-title>\n            <ion-card-subtitle>{{ group.accepted ? group.role : 'Click to Manage Membership' }}</ion-card-subtitle>\n          </ion-card-header>\n          <ion-card-content>\n          </ion-card-content>\n        </ion-card>\n      </ion-item>\n    </ion-list>\n    <ion-text *ngIf=\"loadedGroups?.length === 0\">You are not a member of any groups</ion-text>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/groups/add-group/add-group.component.scss":
    /*!***********************************************************!*\
      !*** ./src/app/groups/add-group/add-group.component.scss ***!
      \***********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppGroupsAddGroupAddGroupComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dyb3Vwcy9hZGQtZ3JvdXAvYWRkLWdyb3VwLmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/groups/add-group/add-group.component.ts":
    /*!*********************************************************!*\
      !*** ./src/app/groups/add-group/add-group.component.ts ***!
      \*********************************************************/

    /*! exports provided: AddGroupComponent */

    /***/
    function srcAppGroupsAddGroupAddGroupComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddGroupComponent", function () {
        return AddGroupComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _groups_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../groups.service */
      "./src/app/groups/groups.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AddGroupComponent = /*#__PURE__*/function () {
        function AddGroupComponent(groupsService, popOverController, alertController) {
          _classCallCheck(this, AddGroupComponent);

          this.groupsService = groupsService;
          this.popOverController = popOverController;
          this.alertController = alertController;
        }

        _createClass(AddGroupComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "onSubmit",
          value: function onSubmit(addGroupForm) {
            var _this = this;

            if (!addGroupForm.valid) {
              console.error('Add Group Form Invalid');
              return;
            }

            this.isLoading = true;
            var name = addGroupForm.value.name;
            this.groupsService.addGroup(name).subscribe(function (responseData) {
              _this.isLoading = false;
              console.log(responseData);

              _this.alertController.create({
                header: 'Group Created',
                buttons: ['OK']
              }).then(function (alert) {
                alert.present().then(function (_) {
                  _this.popOverController.dismiss();
                });
              });
            });
          }
        }, {
          key: "onCancel",
          value: function onCancel() {
            this.popOverController.dismiss();
          }
        }]);

        return AddGroupComponent;
      }();

      AddGroupComponent.ctorParameters = function () {
        return [{
          type: _groups_service__WEBPACK_IMPORTED_MODULE_2__["GroupsService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"]
        }];
      };

      AddGroupComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-add-group',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./add-group.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/add-group/add-group.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./add-group.component.scss */
        "./src/app/groups/add-group/add-group.component.scss"))["default"]]
      })], AddGroupComponent);
      /***/
    },

    /***/
    "./src/app/groups/groups-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/groups/groups-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: GroupsPageRoutingModule */

    /***/
    function srcAppGroupsGroupsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GroupsPageRoutingModule", function () {
        return GroupsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../auth/auth.guard */
      "./src/app/auth/auth.guard.ts");
      /* harmony import */


      var _profile_profile_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../profile/profile.guard */
      "./src/app/profile/profile.guard.ts");
      /* harmony import */


      var _groups_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./groups.page */
      "./src/app/groups/groups.page.ts");

      var routes = [{
        path: '',
        component: _groups_page__WEBPACK_IMPORTED_MODULE_5__["GroupsPage"]
      }, {
        path: 'detail',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | group-detail-group-detail-module */
          "group-detail-group-detail-module").then(__webpack_require__.bind(null,
          /*! ./group-detail/group-detail.module */
          "./src/app/groups/group-detail/group-detail.module.ts")).then(function (m) {
            return m.GroupDetailPageModule;
          });
        },
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"], _profile_profile_guard__WEBPACK_IMPORTED_MODULE_4__["ProfileGuard"]]
      }];

      var GroupsPageRoutingModule = function GroupsPageRoutingModule() {
        _classCallCheck(this, GroupsPageRoutingModule);
      };

      GroupsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], GroupsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/groups/groups.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/groups/groups.module.ts ***!
      \*****************************************/

    /*! exports provided: GroupsPageModule */

    /***/
    function srcAppGroupsGroupsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GroupsPageModule", function () {
        return GroupsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _groups_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./groups-routing.module */
      "./src/app/groups/groups-routing.module.ts");
      /* harmony import */


      var _groups_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./groups.page */
      "./src/app/groups/groups.page.ts");
      /* harmony import */


      var _add_group_add_group_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./add-group/add-group.component */
      "./src/app/groups/add-group/add-group.component.ts");

      var GroupsPageModule = function GroupsPageModule() {
        _classCallCheck(this, GroupsPageModule);
      };

      GroupsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _groups_routing_module__WEBPACK_IMPORTED_MODULE_5__["GroupsPageRoutingModule"]],
        declarations: [_groups_page__WEBPACK_IMPORTED_MODULE_6__["GroupsPage"], _add_group_add_group_component__WEBPACK_IMPORTED_MODULE_7__["AddGroupComponent"]]
      })], GroupsPageModule);
      /***/
    },

    /***/
    "./src/app/groups/groups.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/groups/groups.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppGroupsGroupsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZ3JvdXBzL2dyb3Vwcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9ncm91cHMvZ3JvdXBzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkIHtcbiAgICB3aWR0aDogMTAwJTtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/groups/groups.page.ts":
    /*!***************************************!*\
      !*** ./src/app/groups/groups.page.ts ***!
      \***************************************/

    /*! exports provided: GroupsPage */

    /***/
    function srcAppGroupsGroupsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GroupsPage", function () {
        return GroupsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _add_group_add_group_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./add-group/add-group.component */
      "./src/app/groups/add-group/add-group.component.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _groups_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./groups.service */
      "./src/app/groups/groups.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _users_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../users/user.service */
      "./src/app/users/user.service.ts");

      var GroupsPage = /*#__PURE__*/function () {
        function GroupsPage(groupsService, userService, popOverController, gestureController, elementRef, navController, alertController) {
          _classCallCheck(this, GroupsPage);

          this.groupsService = groupsService;
          this.userService = userService;
          this.popOverController = popOverController;
          this.gestureController = gestureController;
          this.elementRef = elementRef;
          this.navController = navController;
          this.alertController = alertController;
          this.isLoading = true;
          this.scrollAtTop = true;
          this.scrollAtBottom = false;
        }

        _createClass(GroupsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this2 = this;

            this.myProfileSubscription = this.userService.getMyProfile().subscribe(function (myProfile) {
              console.log(myProfile.identifier);
              _this2.myProfile = myProfile;
            });
            this.groupsSubscription = this.groupsService.groups.subscribe(function (groups) {
              _this2.loadedGroups = groups;
              _this2.isLoading = false;
              console.log(_this2.loadedGroups);
            });
            var gestureConfig = {
              gestureName: 'refreshGesture',
              el: this.elementRef.nativeElement,
              direction: 'y',
              threshold: 500,
              canStart: function canStart() {
                return !_this2.isLoading && (_this2.scrollAtTop || _this2.scrollAtBottom);
              },
              onEnd: function onEnd(event) {
                var direction = event.currentY - event.startY;

                if (_this2.scrollAtTop && direction > 0 || _this2.scrollAtBottom && direction < 0) {
                  _this2.isLoading = true;

                  _this2.groupsService.fetchGroups().subscribe(function (_) {}, function (errorResponse) {
                    console.log(errorResponse);
                    _this2.isLoading = false;
                  });
                }
              }
            };
            this.gestureController.create(gestureConfig, true).enable();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.groupsSubscription) {
              console.log("groupsSubscription unsubscribe");
              this.groupsSubscription.unsubscribe();
            }

            if (this.myProfileSubscription) {
              this.myProfileSubscription.unsubscribe();
            }
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            var _this3 = this;

            this.isLoading = true;
            this.groupsService.fetchGroups().subscribe(function (_) {}, function (errorResponse) {
              console.log(errorResponse);
              _this3.isLoading = false;
            });
          }
        }, {
          key: "onAddGroup",
          value: function onAddGroup() {
            this.popOverController.create({
              component: _add_group_add_group_component__WEBPACK_IMPORTED_MODULE_1__["AddGroupComponent"]
            }).then(function (popOver) {
              popOver.present();
            });
          }
        }, {
          key: "onRefreshGroups",
          value: function onRefreshGroups() {
            var _this4 = this;

            this.isLoading = true;
            this.groupsService.fetchGroups().subscribe(function (_) {}, function (errorResponse) {
              console.log(errorResponse);
              _this4.isLoading = false;
            });
          }
        }, {
          key: "onClickGroupCard",
          value: function onClickGroupCard(groupModel) {
            console.log(groupModel);

            if (groupModel.accepted) {
              this.navController.navigateForward(['/', 'groups', 'detail', groupModel.identifier]);
            } else {
              this.onAcceptGroup(groupModel.identifier);
            }
          }
        }, {
          key: "onAcceptGroup",
          value: function onAcceptGroup(groupIdentifier) {
            var _this5 = this;

            console.log('onLeaveGroup');
            this.alertController.create({
              header: 'Accept Group Membership?',
              buttons: [{
                text: 'Accept',
                handler: function handler(_) {
                  _this5.isLoading = true;

                  _this5.groupsService.updateGroupsMemberAccepted(groupIdentifier, true).subscribe(function (acceptGroupMembershipResponse) {
                    console.log(acceptGroupMembershipResponse);

                    _this5.navController.navigateForward(['/', 'groups', 'detail', groupIdentifier]);
                  }, function (errorResponse) {
                    console.log(errorResponse);
                    _this5.isLoading = false;
                  });
                }
              }, {
                text: 'Reject',
                handler: function handler(_) {
                  _this5.isLoading = true;

                  _this5.groupsSubscription.unsubscribe();

                  _this5.groupsService.deleteGroupsMember(groupIdentifier, _this5.myProfile.identifier).subscribe(function (deleteResponse) {
                    console.log(deleteResponse);
                    _this5.isLoading = false;
                  }, function (errorResponse) {
                    console.log(errorResponse);
                    _this5.isLoading = false;
                  });
                }
              }, 'Cancel']
            }).then(function (alert) {
              alert.present();
            });
          }
        }, {
          key: "onScroll",
          value: function onScroll(event) {
            var _this6 = this;

            event.target.getScrollElement().then(function (scrollElement) {
              var scrollHeight = scrollElement.scrollHeight - scrollElement.clientHeight;
              var scrollTop = event.detail.scrollTop;

              if (scrollTop === 0) {
                _this6.scrollAtTop = true;
                _this6.scrollAtBottom = false;
              } else if (scrollTop === scrollHeight) {
                _this6.scrollAtTop = false;
                _this6.scrollAtBottom = true;
              } else {
                _this6.scrollAtTop = false;
                _this6.scrollAtBottom = false;
              }
            });
          }
        }]);

        return GroupsPage;
      }();

      GroupsPage.ctorParameters = function () {
        return [{
          type: _groups_service__WEBPACK_IMPORTED_MODULE_3__["GroupsService"]
        }, {
          type: _users_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ElementRef"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }];
      };

      GroupsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-groups',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./groups.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/groups/groups.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./groups.page.scss */
        "./src/app/groups/groups.page.scss"))["default"]]
      })], GroupsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=groups-groups-module-es5.js.map