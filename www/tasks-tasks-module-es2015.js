(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tasks-tasks-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tasks/task-detail/task-detail.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tasks/task-detail/task-detail.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-spinner *ngIf=\"isLoading\"></ion-spinner>\n\n<ion-card *ngIf=\"!isLoading\">\n  <ion-card-header>\n    <ion-card-title>{{ myTask.name }}</ion-card-title>\n    <ion-card-subtitle>Due: {{ myTask.dueDate.toLocaleDateString() }}</ion-card-subtitle>\n    <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-button type=\"button\" color=\"primary\" expand=\"block\" *ngIf=\"myTask.status == 'Open'\"\n              (click)=\"onChangeStatus('Accepted')\">\n              Accept\n            </ion-button>\n            <ion-button type=\"button\" color=\"primary\" expand=\"block\" *ngIf=\"myTask.status == 'Accepted'\"\n              (click)=\"onChangeStatus('Working')\">\n              Working\n            </ion-button>\n            <ion-button type=\"button\" color=\"primary\" expand=\"block\" *ngIf=\"myTask.status == 'Working'\"\n              (click)=\"onChangeStatus('Verify')\">\n              Ready to Verify\n            </ion-button>\n            <ion-text *ngIf=\"myTask.status == 'Verify'\">Awaiting Verification</ion-text>\n            <ion-text *ngIf=\"myTask.status == 'Complete'\">Complete</ion-text>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-text>{{ myTask.description }}</ion-text>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <a routerLink=\"/groups/detail/{{ myTask.groupIdentifier }}\">Goto Group</a>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card-header>\n</ion-card>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tasks/tasks.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tasks/tasks.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>My Tasks</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"menu1\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"onRefreshTasks()\">\n        <ion-icon name=\"refresh\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content (ionScroll)=\"onScroll($event)\" [scrollEvents]=\"true\">\n  <ion-spinner *ngIf=\"isLoading; else notIsLoading\"></ion-spinner>\n\n  <ng-template #notIsLoading>\n    <ion-text *ngIf=\"myTasks?.length === 0\">You are not assigned any tasks</ion-text>\n\n    <ion-list *ngIf=\"myTasks?.length > 0\">\n      <div *ngFor=\"let task of myTasks\">\n        <app-task-detail [myTask]=\"task\"></app-task-detail>\n      </div>\n    </ion-list>\n  </ng-template>\n</ion-content>");

/***/ }),

/***/ "./src/app/tasks/task-detail/task-detail.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/tasks/task-detail/task-detail.component.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rhc2tzL3Rhc2stZGV0YWlsL3Rhc2stZGV0YWlsLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/tasks/task-detail/task-detail.component.ts":
/*!************************************************************!*\
  !*** ./src/app/tasks/task-detail/task-detail.component.ts ***!
  \************************************************************/
/*! exports provided: TaskDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskDetailComponent", function() { return TaskDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _tasks_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../tasks.service */ "./src/app/tasks/tasks.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");



let TaskDetailComponent = class TaskDetailComponent {
    constructor(tasksService) {
        this.tasksService = tasksService;
        this.isLoading = false;
    }
    ngOnInit() { }
    onChangeStatus(newStatus) {
        console.log(newStatus);
        this.tasksService.changeTaskStatus(this.myTask.identifier, newStatus)
            .subscribe((taskModel) => {
            console.log(taskModel);
        }, (errorResponse) => {
            console.log(errorResponse);
        });
    }
};
TaskDetailComponent.ctorParameters = () => [
    { type: _tasks_service__WEBPACK_IMPORTED_MODULE_1__["TasksService"] }
];
TaskDetailComponent.propDecorators = {
    myTask: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"] }]
};
TaskDetailComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-task-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./task-detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tasks/task-detail/task-detail.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./task-detail.component.scss */ "./src/app/tasks/task-detail/task-detail.component.scss")).default]
    })
], TaskDetailComponent);



/***/ }),

/***/ "./src/app/tasks/tasks-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/tasks/tasks-routing.module.ts ***!
  \***********************************************/
/*! exports provided: TasksPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksPageRoutingModule", function() { return TasksPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _tasks_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tasks.page */ "./src/app/tasks/tasks.page.ts");




const routes = [
    {
        path: '',
        component: _tasks_page__WEBPACK_IMPORTED_MODULE_3__["TasksPage"]
    }
];
let TasksPageRoutingModule = class TasksPageRoutingModule {
};
TasksPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TasksPageRoutingModule);



/***/ }),

/***/ "./src/app/tasks/tasks.module.ts":
/*!***************************************!*\
  !*** ./src/app/tasks/tasks.module.ts ***!
  \***************************************/
/*! exports provided: TasksPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksPageModule", function() { return TasksPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _tasks_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tasks-routing.module */ "./src/app/tasks/tasks-routing.module.ts");
/* harmony import */ var _tasks_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tasks.page */ "./src/app/tasks/tasks.page.ts");
/* harmony import */ var _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./task-detail/task-detail.component */ "./src/app/tasks/task-detail/task-detail.component.ts");








let TasksPageModule = class TasksPageModule {
};
TasksPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _tasks_routing_module__WEBPACK_IMPORTED_MODULE_5__["TasksPageRoutingModule"]
        ],
        declarations: [_tasks_page__WEBPACK_IMPORTED_MODULE_6__["TasksPage"], _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_7__["TaskDetailComponent"]]
    })
], TasksPageModule);



/***/ }),

/***/ "./src/app/tasks/tasks.page.scss":
/*!***************************************!*\
  !*** ./src/app/tasks/tasks.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rhc2tzL3Rhc2tzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/tasks/tasks.page.ts":
/*!*************************************!*\
  !*** ./src/app/tasks/tasks.page.ts ***!
  \*************************************/
/*! exports provided: TasksPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksPage", function() { return TasksPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _tasks_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tasks.service */ "./src/app/tasks/tasks.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");




let TasksPage = class TasksPage {
    constructor(tasksService, gestureController, elementRef) {
        this.tasksService = tasksService;
        this.gestureController = gestureController;
        this.elementRef = elementRef;
        this.isLoading = true;
        this.scrollAtTop = true;
        this.scrollAtBottom = false;
    }
    ngOnInit() {
        this.myTasksSubscription = this.tasksService.myTasks
            .subscribe((myTasks) => {
            this.myTasks = myTasks;
            this.isLoading = false;
        });
        const gestureConfig = {
            gestureName: 'refreshGesture',
            el: this.elementRef.nativeElement,
            direction: 'y',
            threshold: 500,
            canStart: () => {
                return !this.isLoading && (this.scrollAtTop || this.scrollAtBottom);
            },
            onEnd: (event) => {
                const direction = event.currentY - event.startY;
                if ((this.scrollAtTop && direction > 0) ||
                    (this.scrollAtBottom && direction < 0)) {
                    this.isLoading = true;
                    this.tasksService.fetchMyTasks()
                        .subscribe((_) => { }, (errorResponse) => {
                        console.log(errorResponse);
                        this.isLoading = false;
                    });
                }
            }
        };
        this.gestureController.create(gestureConfig, true)
            .enable();
    }
    ngOnDestroy() {
        if (this.myTasksSubscription) {
            this.myTasksSubscription.unsubscribe();
        }
    }
    ionViewWillEnter() {
        this.isLoading = true;
        this.tasksService.fetchMyTasks()
            .subscribe((_) => { }, (errorResponse) => {
            console.log(errorResponse);
            this.isLoading = false;
        });
    }
    onRefreshTasks() {
        this.isLoading = true;
        this.tasksService.fetchMyTasks()
            .subscribe((_) => { }, (errorResponse) => {
            console.log(errorResponse);
            this.isLoading = false;
        });
    }
    onScroll(event) {
        event.target.getScrollElement()
            .then((scrollElement) => {
            const scrollHeight = scrollElement.scrollHeight - scrollElement.clientHeight;
            const scrollTop = event.detail.scrollTop;
            if (scrollTop === 0) {
                this.scrollAtTop = true;
                this.scrollAtBottom = false;
            }
            else if (scrollTop === scrollHeight) {
                this.scrollAtTop = false;
                this.scrollAtBottom = true;
            }
            else {
                this.scrollAtTop = false;
                this.scrollAtBottom = false;
            }
        });
    }
};
TasksPage.ctorParameters = () => [
    { type: _tasks_service__WEBPACK_IMPORTED_MODULE_1__["TasksService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["GestureController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"] }
];
TasksPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-tasks',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./tasks.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tasks/tasks.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./tasks.page.scss */ "./src/app/tasks/tasks.page.scss")).default]
    })
], TasksPage);



/***/ })

}]);
//# sourceMappingURL=tasks-tasks-module-es2015.js.map