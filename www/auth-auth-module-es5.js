(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["auth-auth-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/auth.page.html":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/auth.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAuthAuthPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title> {{ !isLogin ? \"Signup\" : \"Login\" }}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content *ngIf=\"!confirmCodeRequiredUserId\">\n  <ion-grid>\n    <app-login *ngIf=\"isLogin\"></app-login>\n    <app-signup *ngIf=\"!isLogin\"></app-signup>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-button type=\"button\" color=\"primary\" fill=\"clear\" expand=\"block\" (click)=\"onSwitchAuthMode()\">\n          Switch to {{ isLogin ? \"Signup\" : \"Login\" }}\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n<ion-content *ngIf=\"confirmCodeRequiredUserId\">\n  <ion-grid>\n    <app-confirm [userId]=confirmCodeRequiredUserId></app-confirm>\n  </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/confirm/confirm.component.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/confirm/confirm.component.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAuthConfirmConfirmComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-row *ngIf=\"isLoading\">\n  <ion-col>\n    <ion-spinner></ion-spinner>\n  </ion-col>\n</ion-row>\n\n<form (ngSubmit)=\"onSubmit(confirmCodeForm)\" #confirmCodeForm=\"ngForm\" *ngIf=\"!isLoading\">\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label position=\"floating\">Verification Code</ion-label>\n        <ion-input type=\"text\" ngModel name=\"confirmationCode\" required ngModel></ion-input>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-button type=\"submit\" color=\"primary\" expand=\"block\" [disabled]=\"!confirmCodeForm.valid \">\n        Submit Code\n      </ion-button>\n    </ion-col>\n    <ion-col>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onResendCode()\">\n        Resend Code\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</form>\n\n<ion-row *ngIf=\"errorMessage && !isLoading\">\n  <ion-col>\n    <ion-text>{{ errorMessage }}</ion-text>\n  </ion-col>\n</ion-row>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/forgot-password/forgot-password.component.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/forgot-password/forgot-password.component.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAuthForgotPasswordForgotPasswordComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-row *ngIf=\"isLoading\">\n  <ion-col>\n    <ion-spinner></ion-spinner>\n  </ion-col>\n</ion-row>\n\n<form #resetForm=\"ngForm\" *ngIf=\"!isLoading && !inputResetCode\">\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label position=\"floating\">UserId</ion-label>\n        <ion-input type=\"text\" ngModel name=\"userId\" required ngModel></ion-input>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" [disabled]=\"!resetForm.valid\"\n        (click)=\"onSendResetCode(resetForm)\">\n        Login\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</form>\n\n<form [formGroup]=\"confirmForm\" *ngIf=\"!isLoading && inputResetCode\">\n  <ion-row>\n    <ion-col size-sm=\"6\" offset-sm=\"3\">\n      <ion-list>\n        <ion-item>\n          <ion-label position=\"floating\">Emailed Code</ion-label>\n          <ion-input type=\"text\" required formControlName=\"code\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"floating\">New Password</ion-label>\n          <ion-input type=\"password\" #newPassword formControlName=\"newPassword\" (ionFocus)=\"newPassword.value = ''\">\n          </ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"floating\">Confirm New Password</ion-label>\n          <ion-input type=\"password\" #confirmPassword (ionFocus)=\"confirmPassword.value = ''\">\n          </ion-input>\n        </ion-item>\n        <ion-item *ngIf=\"!confirmForm.get('newPassword').valid && confirmForm.get('newPassword').touched\" lines=\"none\">\n          <ion-text>Enter a Valid Password</ion-text>\n        </ion-item>\n        <ion-item *ngIf=\"confirmForm.get('newPassword').valid && newPassword.value != confirmPassword.value\"\n          lines=\"none\">\n          <ion-text>Passwords do not match</ion-text>\n        </ion-item>\n      </ion-list>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" [disabled]=\"!confirmForm.valid\"\n        (click)=\"onVerifyCode(confirmForm)\">\n        Verify Code\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</form>\n\n<ion-row *ngIf=\"errorMessage && !isLoading\">\n  <ion-col>\n    <ion-text>{{ errorMessage }}</ion-text>\n  </ion-col>\n</ion-row>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/login/login.component.html":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/login/login.component.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAuthLoginLoginComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-row *ngIf=\"isLoading\">\n  <ion-col>\n    <ion-spinner></ion-spinner>\n  </ion-col>\n</ion-row>\n\n<form (ngSubmit)=\"onSubmit(loginForm)\" #loginForm=\"ngForm\" *ngIf=\"!isLoading\">\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label position=\"floating\">UserId</ion-label>\n        <ion-input type=\"text\" ngModel name=\"userId\" required ngModel></ion-input>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-item>\n        <ion-label position=\"floating\">Password</ion-label>\n        <ion-input type=\"password\" ngModel name=\"password\" required ngModel #inputPassword\n          (ionFocus)=\"inputPassword.value = ''\"></ion-input>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-button type=\"submit\" color=\"primary\" expand=\"block\" [disabled]=\"!loginForm.valid \">\n        Login\n      </ion-button>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\" (click)=\"onForgotPassword()\">\n        Forgot Password\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</form>\n\n<ion-row *ngIf=\"errorMessage && !isLoading\">\n  <ion-col>\n    <ion-text>{{ errorMessage }}</ion-text>\n  </ion-col>\n</ion-row>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/signup/signup.component.html":
    /*!*****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/signup/signup.component.html ***!
      \*****************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAuthSignupSignupComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-row *ngIf=\"isLoading\">\n  <ion-col>\n    <ion-spinner></ion-spinner>\n  </ion-col>\n</ion-row>\n\n<form [formGroup]=\"signupForm\" *ngIf=\"!isLoading\">\n  <ion-row>\n    <ion-col size-sm=\"6\" offset-sm=\"3\">\n      <ion-list>\n        <ion-item>\n          <ion-label position=\"floating\">User Id</ion-label>\n          <ion-input type=\"text\" formControlName=\"userId\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"floating\">Email</ion-label>\n          <ion-input type=\"email\" #email formControlName=\"email\"></ion-input>\n        </ion-item>\n        <ion-item *ngIf=\"!signupForm.get('email').valid && signupForm.get('email').touched\" lines=\"none\">\n          <ion-text>Enter a Valid Email</ion-text>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"floating\">Password</ion-label>\n          <ion-input type=\"password\" #inputPassword formControlName=\"inputPassword\"\n            (ionFocus)=\"inputPassword.value = ''\">\n          </ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"floating\">Confirm Password</ion-label>\n          <ion-input type=\"password\" #confirmPassword (ionFocus)=\"confirmPassword.value = ''\">\n          </ion-input>\n        </ion-item>\n        <ion-item *ngIf=\"!signupForm.get('inputPassword').valid && signupForm.get('inputPassword').touched\"\n          lines=\"none\">\n          <ion-text>Enter a Valid Password</ion-text>\n        </ion-item>\n        <ion-item *ngIf=\"signupForm.get('inputPassword').valid && inputPassword.value != confirmPassword.value\"\n          lines=\"none\">\n          <ion-text>Passwords do not match</ion-text>\n        </ion-item>\n      </ion-list>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-button type=\"button\" color=\"primary\" expand=\"block\"\n        [disabled]=\"!signupForm.valid || inputPassword.value != confirmPassword.value\" (click)=\"onSignup()\">\n        Signup\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</form>\n<ion-row *ngIf=\"errorMessage && !isLoading\">\n  <ion-col>\n    <ion-text>{{ errorMessage }}</ion-text>\n  </ion-col>\n</ion-row>";
      /***/
    },

    /***/
    "./src/app/auth/auth-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/auth/auth-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: AuthPageRoutingModule */

    /***/
    function srcAppAuthAuthRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthPageRoutingModule", function () {
        return AuthPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth.page */
      "./src/app/auth/auth.page.ts");

      var routes = [{
        path: '',
        component: _auth_page__WEBPACK_IMPORTED_MODULE_3__["AuthPage"]
      }];

      var AuthPageRoutingModule = function AuthPageRoutingModule() {
        _classCallCheck(this, AuthPageRoutingModule);
      };

      AuthPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AuthPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/auth/auth.module.ts":
    /*!*************************************!*\
      !*** ./src/app/auth/auth.module.ts ***!
      \*************************************/

    /*! exports provided: AuthPageModule */

    /***/
    function srcAppAuthAuthModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthPageModule", function () {
        return AuthPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _login_login_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./login/login.component */
      "./src/app/auth/login/login.component.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _auth_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./auth-routing.module */
      "./src/app/auth/auth-routing.module.ts");
      /* harmony import */


      var _auth_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./auth.page */
      "./src/app/auth/auth.page.ts");
      /* harmony import */


      var _signup_signup_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./signup/signup.component */
      "./src/app/auth/signup/signup.component.ts");
      /* harmony import */


      var _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./confirm/confirm.component */
      "./src/app/auth/confirm/confirm.component.ts");
      /* harmony import */


      var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./forgot-password/forgot-password.component */
      "./src/app/auth/forgot-password/forgot-password.component.ts");

      var AuthPageModule = function AuthPageModule() {
        _classCallCheck(this, AuthPageModule);
      };

      AuthPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _auth_routing_module__WEBPACK_IMPORTED_MODULE_6__["AuthPageRoutingModule"]],
        declarations: [_auth_page__WEBPACK_IMPORTED_MODULE_7__["AuthPage"], _signup_signup_component__WEBPACK_IMPORTED_MODULE_8__["SignupComponent"], _login_login_component__WEBPACK_IMPORTED_MODULE_1__["LoginComponent"], _confirm_confirm_component__WEBPACK_IMPORTED_MODULE_9__["ConfirmComponent"], _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_10__["ForgotPasswordComponent"]]
      })], AuthPageModule);
      /***/
    },

    /***/
    "./src/app/auth/auth.page.scss":
    /*!*************************************!*\
      !*** ./src/app/auth/auth.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function srcAppAuthAuthPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvYXV0aC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/auth/auth.page.ts":
    /*!***********************************!*\
      !*** ./src/app/auth/auth.page.ts ***!
      \***********************************/

    /*! exports provided: AuthPage */

    /***/
    function srcAppAuthAuthPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthPage", function () {
        return AuthPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./auth.service */
      "./src/app/auth/auth.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AuthPage = /*#__PURE__*/function () {
        function AuthPage(authService) {
          _classCallCheck(this, AuthPage);

          this.authService = authService;
          this.isLogin = true;
        }

        _createClass(AuthPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.confirmCodeRequiredSubscription = this.authService.confirmCodeRequired.subscribe(function (userId) {
              console.log(userId);
              _this.confirmCodeRequiredUserId = userId;

              if (_this.confirmCodeRequiredUserId) {
                _this.isLogin = true;
              }
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.confirmCodeRequiredSubscription) {
              this.confirmCodeRequiredSubscription.unsubscribe();
            }
          }
        }, {
          key: "onSwitchAuthMode",
          value: function onSwitchAuthMode() {
            this.isLogin = !this.isLogin;
          }
        }]);

        return AuthPage;
      }();

      AuthPage.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
        }];
      };

      AuthPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-auth',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./auth.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/auth.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./auth.page.scss */
        "./src/app/auth/auth.page.scss"))["default"]]
      })], AuthPage);
      /***/
    },

    /***/
    "./src/app/auth/confirm/confirm.component.scss":
    /*!*****************************************************!*\
      !*** ./src/app/auth/confirm/confirm.component.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppAuthConfirmConfirmComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvY29uZmlybS9jb25maXJtLmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/auth/confirm/confirm.component.ts":
    /*!***************************************************!*\
      !*** ./src/app/auth/confirm/confirm.component.ts ***!
      \***************************************************/

    /*! exports provided: ConfirmComponent */

    /***/
    function srcAppAuthConfirmConfirmComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConfirmComponent", function () {
        return ConfirmComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../auth.service */
      "./src/app/auth/auth.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ConfirmComponent = /*#__PURE__*/function () {
        function ConfirmComponent(authService, alertController) {
          _classCallCheck(this, ConfirmComponent);

          this.authService = authService;
          this.alertController = alertController;
          this.isLoading = false;
        }

        _createClass(ConfirmComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {}
        }, {
          key: "onSubmit",
          value: function onSubmit(confirmForm) {
            var _this2 = this;

            console.log(confirmForm);
            this.isLoading = true;
            var confirmationCode = confirmForm.value.confirmationCode;
            console.log(confirmationCode);
            console.log(this.userId);
            this.authService.confirmUser(this.userId, confirmationCode).then(function (_) {
              _this2.isLoading = false;
            })["catch"](function (errorMessage) {
              console.error(errorMessage);
              _this2.errorMessage = errorMessage;
              _this2.isLoading = false;
            });
          }
        }, {
          key: "onResendCode",
          value: function onResendCode() {
            var _this3 = this;

            this.authService.resendConfirmationCode(this.userId).then(function (_) {
              _this3.alertController.create({
                header: 'Verification Code',
                message: 'Check Email for the Verification Code',
                buttons: ['OK']
              }).then(function (alert) {
                alert.present();
              });
            })["catch"](function (errorMessage) {
              console.error(errorMessage);
              _this3.errorMessage = errorMessage;
            });
          }
        }]);

        return ConfirmComponent;
      }();

      ConfirmComponent.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }];
      };

      ConfirmComponent.propDecorators = {
        userId: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"]
        }]
      };
      ConfirmComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-confirm',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./confirm.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/confirm/confirm.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./confirm.component.scss */
        "./src/app/auth/confirm/confirm.component.scss"))["default"]]
      })], ConfirmComponent);
      /***/
    },

    /***/
    "./src/app/auth/forgot-password/forgot-password.component.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/auth/forgot-password/forgot-password.component.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppAuthForgotPasswordForgotPasswordComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/auth/forgot-password/forgot-password.component.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/auth/forgot-password/forgot-password.component.ts ***!
      \*******************************************************************/

    /*! exports provided: ForgotPasswordComponent */

    /***/
    function srcAppAuthForgotPasswordForgotPasswordComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function () {
        return ForgotPasswordComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth/auth.service.ts");

      var ForgotPasswordComponent = /*#__PURE__*/function () {
        function ForgotPasswordComponent(authService, popoverController, alertController) {
          _classCallCheck(this, ForgotPasswordComponent);

          this.authService = authService;
          this.popoverController = popoverController;
          this.alertController = alertController;
          this.isLoading = false;
          this.inputResetCode = false;
        }

        _createClass(ForgotPasswordComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.confirmForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              code: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, {
                updateOn: 'change',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
              }),
              newPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, {
                updateOn: 'change',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8), this.passwordValidator]
              })
            });
          }
        }, {
          key: "onSendResetCode",
          value: function onSendResetCode(resetForm) {
            var _this4 = this;

            this.isLoading = true;
            this.userId = resetForm.value.userId;
            this.authService.resetPassword(this.userId).then(function (_) {
              console.log('reset code request sent');
              _this4.isLoading = false;
              _this4.inputResetCode = true;
            })["catch"](function (errorMessage) {
              console.error(errorMessage);
              _this4.errorMessage = errorMessage;
              _this4.isLoading = false;
            });
          }
        }, {
          key: "onVerifyCode",
          value: function onVerifyCode() {
            var _this5 = this;

            var code = this.confirmForm.value.code;
            var newPassword = this.confirmForm.value.newPassword;
            console.log(code);
            console.log(newPassword);
            console.log(this.userId);
            this.authService.confirmResetCode(this.userId, code, newPassword).then(function (_) {})["catch"](function (errorMessage) {
              console.error(errorMessage);
              _this5.errorMessage = errorMessage;
            })["finally"](function () {
              _this5.alertController.create({
                header: 'Password Reset',
                message: _this5.errorMessage ? _this5.errorMessage : 'Your Password has been Changed',
                buttons: ['OK']
              }).then(function (alert) {
                alert.present().then(function (_) {
                  _this5.popoverController.dismiss();
                });
              });
            });
          }
        }, {
          key: "passwordValidator",
          value: function passwordValidator(formControl) {
            var hasNumber = /\d/.test(formControl.value);
            var hasLower = /[A-Z]/.test(formControl.value);
            var hasUpper = /[a-z]/.test(formControl.value);

            if (hasLower && hasUpper && hasNumber) {
              return null;
            }

            return {
              weak: true
            };
          }
        }]);

        return ForgotPasswordComponent;
      }();

      ForgotPasswordComponent.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"]
        }];
      };

      ForgotPasswordComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-forgot-password',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./forgot-password.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/forgot-password/forgot-password.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./forgot-password.component.scss */
        "./src/app/auth/forgot-password/forgot-password.component.scss"))["default"]]
      })], ForgotPasswordComponent);
      /***/
    },

    /***/
    "./src/app/auth/login/login.component.scss":
    /*!*************************************************!*\
      !*** ./src/app/auth/login/login.component.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function srcAppAuthLoginLoginComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/auth/login/login.component.ts":
    /*!***********************************************!*\
      !*** ./src/app/auth/login/login.component.ts ***!
      \***********************************************/

    /*! exports provided: LoginComponent */

    /***/
    function srcAppAuthLoginLoginComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginComponent", function () {
        return LoginComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _firebase_messaging_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../firebase-messaging.service */
      "./src/app/auth/firebase-messaging.service.ts");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../auth.service */
      "./src/app/auth/auth.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../forgot-password/forgot-password.component */
      "./src/app/auth/forgot-password/forgot-password.component.ts");

      var LoginComponent = /*#__PURE__*/function () {
        function LoginComponent(authService, popOverController, firebaseMessagingService) {
          _classCallCheck(this, LoginComponent);

          this.authService = authService;
          this.popOverController = popOverController;
          this.firebaseMessagingService = firebaseMessagingService;
          this.isLoading = false;
        }

        _createClass(LoginComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {}
        }, {
          key: "onSubmit",
          value: function onSubmit(loginForm) {
            var _this6 = this;

            this.isLoading = true;
            var userId = loginForm.value.userId;
            var password = loginForm.value.password;
            this.authService.signIn(userId, password).then(function (_) {
              _this6.isLoading = false;

              _this6.firebaseMessagingService.requestPermission();
            })["catch"](function (errorMessage) {
              console.error(errorMessage);
              _this6.errorMessage = errorMessage;
              _this6.isLoading = false;
            });
          }
        }, {
          key: "onForgotPassword",
          value: function onForgotPassword() {
            this.popOverController.create({
              component: _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_5__["ForgotPasswordComponent"]
            }).then(function (popover) {
              popover.present();
            });
          }
        }]);

        return LoginComponent;
      }();

      LoginComponent.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"]
        }, {
          type: _firebase_messaging_service__WEBPACK_IMPORTED_MODULE_1__["FirebaseMessagingService"]
        }];
      };

      LoginComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/login/login.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.component.scss */
        "./src/app/auth/login/login.component.scss"))["default"]]
      })], LoginComponent);
      /***/
    },

    /***/
    "./src/app/auth/signup/signup.component.scss":
    /*!***************************************************!*\
      !*** ./src/app/auth/signup/signup.component.scss ***!
      \***************************************************/

    /*! exports provided: default */

    /***/
    function srcAppAuthSignupSignupComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/auth/signup/signup.component.ts":
    /*!*************************************************!*\
      !*** ./src/app/auth/signup/signup.component.ts ***!
      \*************************************************/

    /*! exports provided: SignupComponent */

    /***/
    function srcAppAuthSignupSignupComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignupComponent", function () {
        return SignupComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../auth.service */
      "./src/app/auth/auth.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

      var SignupComponent = /*#__PURE__*/function () {
        function SignupComponent(authService) {
          _classCallCheck(this, SignupComponent);

          this.authService = authService;
          this.isLoading = false;
        }

        _createClass(SignupComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.signupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
              userId: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
              }),
              email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'change',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(80)]
              }),
              inputPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'change',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8), this.passwordValidator]
              })
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {}
        }, {
          key: "onSignup",
          value: function onSignup() {
            var _this7 = this;

            console.log('onSignup');

            if (!this.signupForm.valid) {
              console.log('not valid');
              return;
            }

            this.isLoading = true;
            var userId = this.signupForm.value.userId;
            var email = this.signupForm.value.email;
            var password = this.signupForm.value.inputPassword;
            this.authService.signUp(userId, email, password).then(function (_) {
              _this7.isLoading = false;
            })["catch"](function (errorMessage) {
              console.error(errorMessage);
              _this7.errorMessage = errorMessage;
              _this7.isLoading = false;
            });
          }
        }, {
          key: "passwordValidator",
          value: function passwordValidator(formControl) {
            var hasNumber = /\d/.test(formControl.value);
            var hasLower = /[A-Z]/.test(formControl.value);
            var hasUpper = /[a-z]/.test(formControl.value);

            if (hasLower && hasUpper && hasNumber) {
              return null;
            }

            return {
              weak: true
            };
          }
        }]);

        return SignupComponent;
      }();

      SignupComponent.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
        }];
      };

      SignupComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-signup',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./signup.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/signup/signup.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./signup.component.scss */
        "./src/app/auth/signup/signup.component.scss"))["default"]]
      })], SignupComponent);
      /***/
    }
  }]);
})();
//# sourceMappingURL=auth-auth-module-es5.js.map