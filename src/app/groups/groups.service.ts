import { UserService } from './../users/user.service';
import { UserModel } from './../users/user.model';
import { TasksService } from './../tasks/tasks.service';
import { environment } from './../../environments/environment';
import { GroupModel, GroupMember } from './group.model';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError, tap, switchMap } from 'rxjs/operators';
import { TaskModel } from '../tasks/task.model';


class GroupResponse {
  identifier: string;
  name: string;
  role: string;
  accepted: boolean;
}

class GroupMembershipResponse {
  groupIdentifier: string;
  userIdentifier: string;
  role: string;
  accepted: boolean;
  name: string;
  title: string;
  email: string;
}

class UpdateGroupMembershipResponse {
  groupIdentifier: string;
  userIdentifier: string;
  role: string;
}

class DeleteGroupMembershipResponse {
  deletedUser: string;
  group: string;
}

@Injectable({
  providedIn: 'root'
})
export class GroupsService {
  // tslint:disable-next-line: variable-name
  private _groups = new BehaviorSubject<GroupModel[]>([]);
  private myProfile: UserModel;

  constructor(
    private httpClient: HttpClient,
    private tasksService: TasksService,
    private userService: UserService
  ) {
    this.userService.getMyProfile()
      .subscribe((myProfile) => {
        this.myProfile = myProfile;
      });
  }

  get groups(): Observable<GroupModel[]> {
    if (this._groups.getValue()) {
      return this._groups.asObservable();
    }

    return this.fetchGroups()
      .pipe(
        switchMap((fetchGroupsResult) => {
          return this._groups.asObservable();
        })
      );
  }

  public fetchGroups(): Observable<GroupModel[]> {
    return this.httpClient.get<GroupResponse[]>(
      environment.API_URL + 'groups/mygroups'
    )
      .pipe(
        map((myGroupsResponse) => {
          const newGroups: GroupModel[] = [];

          myGroupsResponse.forEach((myGroup) => {
            const groupModel = new GroupModel(myGroup.identifier, myGroup.name, myGroup.role, myGroup.accepted);
            newGroups.push(groupModel);
          });

          const sortedGroups = this.sortGroups(newGroups);
          this._groups.next(sortedGroups);

          return sortedGroups;
        })
      );
  }

  public getGroupByIdentifier(identifier: string): Observable<GroupModel> {
    const groups = this._groups.getValue();
    console.log(groups);

    return this.httpClient.get<GroupResponse>(
      environment.API_URL + 'groups/' + identifier
    ).pipe(
      catchError(this.handleError),
      switchMap((groupResponse) => {

        const groupModel = new GroupModel(
          groupResponse.identifier,
          groupResponse.name,
          groupResponse.role,
          groupResponse.accepted);

        return this.getGroupsMembers(identifier)
          .pipe(
            switchMap((groupMembers) => {
              console.log(groupMembers);
              groupModel.setMembers(groupMembers);

              return this.tasksService.fetchTasksByGroup(identifier)
                .pipe(
                  catchError(this.handleError),
                  map((groupsTasks) => {
                    console.log(groupsTasks);
                    groupModel.setTasks(groupsTasks);

                    groups.forEach((group, index) => {
                      if (group.identifier === groupModel.identifier) {
                        groups[index] = groupModel;
                      }
                    });

                    //this._groups.next([...groups]);

                    return groupModel;

                  })
                );

            })
          );

      })

    );

  }

  public addGroup(name: string): Observable<GroupModel> {
    return this.httpClient.post<GroupResponse>(
      environment.API_URL + 'groups',
      {
        name
      }
    )
      .pipe(
        catchError(this.handleError),
        map((response) => {
          console.log(response);
          const groups = this._groups.getValue();
          const newGroup = new GroupModel(response.identifier, response.name, response.role, response.accepted);
          groups.push(newGroup);

          this._groups.next([...groups]);

          return newGroup;
        })
      );
  }

  public editGroup(identifier: string, name: string) {
    return this.httpClient.patch<GroupModel>(
      environment.API_URL + 'groups/' + identifier,
      {
        name
      }
    )
      .pipe(
        catchError(this.handleError),
        map((response) => {
          console.log(response);
          const groups = this._groups.getValue();
          const newGroup = new GroupModel(response.identifier, response.name, response.role, response.accepted);
          groups.forEach((group, index) => {
            if (group.identifier === newGroup.identifier) {
              newGroup.setMembers(groups[index].members);
              groups[index] = newGroup;
            }
          });

          const sortedGroups = this.sortGroups(groups);
          this._groups.next([...sortedGroups]);

          return newGroup;
        })
      );
  }

  deleteGroup(identifier: string) {
    return this.httpClient.delete(
      environment.API_URL + 'groups/' + identifier
    )
      .pipe(
        catchError(this.handleError),
        tap((deleteResponse) => {
          const groups = this._groups.getValue();
          groups.forEach((group, index) => {
            if (group.identifier === identifier) {
              groups.splice(index, 1);
            }
          });

          this._groups.next([...groups]);
        })
      );
  }

  private getGroupsMembers(identifier: string): Observable<GroupMember[]> {
    return this.httpClient.get<GroupMembershipResponse[]>(
      environment.API_URL + 'groups/' + identifier + '/members'
    )
      .pipe(
        catchError(this.handleError),
        map((groupMembership) => {
          console.log(groupMembership);
          const groupMembers: GroupMember[] = [];

          groupMembership.forEach((member) => {
            const newMember = new GroupMember(
              member.email,
              member.userIdentifier,
              member.name,
              member.role,
              member.accepted,
              member.title);
            groupMembers.push(newMember);
          });

          return this.sortMembers(groupMembers);
        })
      );
  }

  addGroupMember(groupIdentifier: string, newMemberEmail: string, role: string): Observable<GroupMember> {
    return this.httpClient.post<GroupMembershipResponse>(
      environment.API_URL + 'groups/' + groupIdentifier + '/members',
      {
        email: newMemberEmail,
        role
      }
    )
      .pipe(
        catchError(this.handleError),
        map((response) => {
          console.log(response);

          const newMember = new GroupMember(
            newMemberEmail,
            response.userIdentifier,
            response.name,
            role,
            response.accepted,
            response.title);

          const groups = this._groups.getValue();
          groups.forEach((group) => {
            if (group.identifier === groupIdentifier) {
              const groupsMembers = group.members;
              groupsMembers.push(newMember);
              group.setMembers(groupsMembers);
            }
          });

          this._groups.next([...groups]);

          return newMember;
        })
      );
  }

  updateGroupsMember(groupIdentifier: string, userIdentifier: string, role: string) {
    return this.httpClient.patch<UpdateGroupMembershipResponse>(
      environment.API_URL + 'groups/' + groupIdentifier + '/members/' + userIdentifier,
      {
        role
      }
    )
      .pipe(
        catchError(this.handleError),
        tap((patchResponse) => {
          console.log(patchResponse);

          const groups = this._groups.getValue();
          groups.forEach((group) => {
            if (group.identifier === groupIdentifier) {
              const groupsMembers = group.members;

              groupsMembers.forEach((member) => {
                if (member.identifier === userIdentifier) {
                  member.role = role;
                }
              });

              group.setMembers(groupsMembers);
            }
          });

          this._groups.next([...groups]);

        })
      );
  }

  deleteGroupsMember(groupIdentifier: string, userIdentifier: string) {
    return this.httpClient.delete<DeleteGroupMembershipResponse>(
      environment.API_URL + 'groups/' + groupIdentifier + '/members/' + userIdentifier
    )
      .pipe(
        catchError(this.handleError),
        tap((deleteResponse) => {
          console.log(deleteResponse);

          const groups = this._groups.getValue();
          groups.forEach((group, groupIndex) => {
            if (group.identifier === groupIdentifier) {

              if (this.myProfile && userIdentifier !== this.myProfile.identifier) {
                const groupsMembers = group.members;

                groupsMembers.forEach((member, memberIndex) => {
                  if (member.identifier === userIdentifier) {
                    groupsMembers.splice(memberIndex, 1);
                  }
                });

                group.setMembers(groupsMembers);

              } else {

                groups.splice(groupIndex, 1);

              }
            }
          });

          this._groups.next([...groups]);

          return deleteResponse;
        })
      );
  }

  updateGroupsMemberAccepted(groupIdentifier: string, accepted: boolean) {
    return this.httpClient.patch<UpdateGroupMembershipResponse>(
      environment.API_URL + 'groups/' + groupIdentifier + '/accepted/',
      {
        accepted
      }
    )
      .pipe(
        catchError(this.handleError),
        tap((patchResponse) => {
          console.log(patchResponse);

          const groups = this._groups.getValue();
          groups.forEach((group) => {
            if (group.identifier === groupIdentifier) {
              const groupsMembers = group.members;

              groupsMembers.forEach((member) => {
                if (member.identifier === patchResponse.userIdentifier) {
                  member.accepted = accepted;
                }
              });

              group.setMembers(groupsMembers);
            }
          });

          this._groups.next([...groups]);
        })
      );
  }

  addTask(
    groupIdentifier: string,
    name: string,
    description: string,
    dueDate: Date,
    owner: string,
    repeats: boolean): Observable<TaskModel> {

    return this.tasksService.addTask(groupIdentifier, name, description, dueDate, owner, repeats)
      .pipe(
        catchError(this.handleError),
        tap((taskModel) => {
          const groups = this._groups.getValue();
          groups.forEach((group) => {
            if (group.identifier === groupIdentifier) {
              const groupsTasks = group.tasks;
              groupsTasks.push(taskModel);
              group.setTasks(groupsTasks);
            }
          });

          console.log(groups);
          this._groups.next([...groups]);
        })
      );
  }

  changeTaskOwner(groupIdentifier: string, taskIdentifier: string, ownerIdentifier: string): Observable<TaskModel> {
    return this.tasksService.changeTaskOwner(taskIdentifier, ownerIdentifier)
      .pipe(
        tap((taskModel) => {
          const groups = this._groups.getValue();
          groups.forEach((group) => {
            if (group.identifier === groupIdentifier) {
              const groupsTasks = group.tasks;

              groupsTasks.forEach((task, taskIndex) => {
                if (task.identifier === taskIdentifier) {
                  groupsTasks[taskIndex] = taskModel;
                }
              });

              group.setTasks(groupsTasks);
            }
          });

          console.log(groups);
          this._groups.next([...groups]);
        })
      );
  }

  changeTaskStatus(groupIdentifier: string, taskIdentifier: string, status: string): Observable<TaskModel> {
    return this.tasksService.changeTaskStatus(taskIdentifier, status)
      .pipe(
        tap((taskModel) => {
          const groups = this._groups.getValue();
          groups.forEach((group) => {
            if (group.identifier === groupIdentifier) {
              const groupsTasks = group.tasks;

              groupsTasks.forEach((task, taskIndex) => {
                if (task.identifier === taskIdentifier) {
                  groupsTasks[taskIndex] = taskModel;
                }
              });

              group.setTasks(groupsTasks);
            }
          });

          console.log(groups);
          this._groups.next([...groups]);
        })
      );
  }

  updateTask(
    groupIdentifier: string,
    taskIdentifier: string,
    name: string,
    description: string,
    dueDate: Date,
    owner: string,
    repeats: boolean): Observable<TaskModel> {

    return this.tasksService.updateTask(taskIdentifier, name, description, dueDate, owner, repeats)
      .pipe(
        catchError(this.handleError),
        tap((taskModel) => {
          console.log(taskModel);

          const groups = this._groups.getValue();
          groups.forEach((group) => {
            if (group.identifier === groupIdentifier) {
              const groupsTasks = group.tasks;

              groupsTasks.forEach((task, taskIndex) => {
                if (task.identifier === taskIdentifier) {
                  groupsTasks[taskIndex] = taskModel;
                }
              });

              group.setTasks(groupsTasks);
            }
          });

          console.log(groups);
          this._groups.next([...groups]);
        })
      );
  }

  deleteTask(groupIdentifier: string, taskIdentifier: string) {
    return this.tasksService.deleteTask(taskIdentifier)
      .pipe(
        tap((_) => {
          const groups = this._groups.getValue();
          groups.forEach((group) => {
            if (group.identifier === groupIdentifier) {
              const groupsTasks = group.tasks;

              groupsTasks.forEach((task, taskIndex) => {
                if (task.identifier === taskIdentifier) {
                  groupsTasks.splice(taskIndex, 1);
                }
              });

              group.setTasks(groupsTasks);

            }

          });

          console.log(groups);
          this._groups.next([...groups]);
        })
      );
  }

  onLogout() {
    this._groups.next(null);
  }

  private handleError(error: HttpErrorResponse) {
    console.error(error);
    return throwError(error);
  }

  private sortGroups(groups: GroupModel[]): GroupModel[] {
    return groups.sort((a, b) => {
      return a.name.toLocaleLowerCase() > b.name.toLocaleLowerCase() ? 1 : -1;
    });
  }

  private sortMembers(members: GroupMember[]): GroupMember[] {
    return members.sort((a, b) => {
      return a.name.toLocaleLowerCase() > b.name.toLocaleLowerCase() ? 1 : -1;
    });
  }
}
