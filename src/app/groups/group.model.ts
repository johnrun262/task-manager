import { TaskModel } from './../tasks/task.model';
export class GroupMember {
    constructor(
        public email: string,
        public identifier: string,
        public name: string,
        public role: string,
        public accepted: boolean,
        public title: string) { }
}

export class MemberSelectorOptions {
    value: string;
    display: string;
    name: string;
    email: string;
}

export class GroupModel {
    // tslint:disable-next-line: variable-name
    private _members: GroupMember[] = [];
    // tslint:disable-next-line: variable-name
    private _tasks: TaskModel[] = [];
    // tslint:disable-next-line: variable-name
    private _ownerCount = 0;

    get members() {
        return [...this._members];
    }

    get tasks() {
        return [...this._tasks];
    }

    get ownerCount() {
        return this._ownerCount;
    }

    constructor(
        public identifier: string,
        public name: string,
        public role: string,
        public accepted: boolean) { }

    getMember(identifier: string): GroupMember {
        if (!identifier) {
            return null;
        }

        for (const member of this._members) {
            if (member.identifier === identifier) {
                return member;
            }
        }
    }

    getMemberSelectorOptions(): MemberSelectorOptions[] {
        const memberSelectionOptions: MemberSelectorOptions[] = [];

        this._members.forEach((member) => {
            if (member.accepted) {
                const option = {
                    value: member.identifier,
                    display: member.name + ' ' + member.email,
                    name: member.name,
                    email: member.email
                };

                memberSelectionOptions.push(option);
            }
        });

        return memberSelectionOptions;
    }

    setMembers(members: GroupMember[]) {
        this._members = members.sort((a, b) => {
            return (a.name < b.name ? -1 : 1);
        });

        this._ownerCount = 0;
        if (this._members) {
            this._members.forEach((member) => {
                if (member.role === 'Owner') {
                    this._ownerCount++;
                }
            });
        }
    }

    setTasks(tasks: TaskModel[]) {
        this._tasks = tasks.sort((e1, e2) => {
            if (e1.status.toLowerCase() === 'complete' &&
                e2.status.toLowerCase() === 'complete') {
                return e1.dueDate.getTime() - e2.dueDate.getTime();
            }

            if (e1.status.toLowerCase() === 'complete' &&
                e2.status.toLowerCase() !== 'complete') {
                return 1;
            }

            if (e2.status.toLowerCase() === 'complete' &&
                e1.status.toLowerCase() !== 'complete') {
                return -1;
            }

            return e1.dueDate.getTime() - e2.dueDate.getTime();
        });
    }
}
