import { catchError } from 'rxjs/operators';
import { GroupModel } from './../group.model';
import { PopoverController, AlertController } from '@ionic/angular';
import { GroupsService } from './../groups.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.scss'],
})
export class AddGroupComponent implements OnInit {
  public isLoading: boolean;

  constructor(
    private groupsService: GroupsService,
    private popOverController: PopoverController,
    private alertController: AlertController
  ) { }

  ngOnInit() { }

  onSubmit(addGroupForm: NgForm) {
    if (!addGroupForm.valid) {
      console.error('Add Group Form Invalid');
      return;
    }

    this.isLoading = true;

    const name = addGroupForm.value.name;

    this.groupsService.addGroup(name)
      .subscribe((responseData) => {
        this.isLoading = false;

        console.log(responseData);

        this.alertController.create({
          header: 'Group Created',
          buttons: ['OK']
        })
          .then((alert) => {
            alert.present()
              .then((_) => {
                this.isLoading = false;
                this.popOverController.dismiss();
              });
          });
      });
  }

  onCancel() {
    this.popOverController.dismiss();
  }

}
