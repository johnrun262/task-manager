import { Subscription } from 'rxjs';
import { UserModel } from './../../../users/user.model';
import { UserService } from './../../../users/user.service';
import { GroupMember, GroupModel } from './../../group.model';
import { GroupsService } from './../../groups.service';
import { AlertController } from '@ionic/angular';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-show-member',
  templateUrl: './show-member.component.html',
  styleUrls: ['./show-member.component.scss'],
})
export class ShowMemberComponent implements OnInit, OnDestroy {
  @Input() groupMember: GroupMember;
  @Input() loadedGroup: GroupModel;
  public myProfile: UserModel;
  private myProfileSubscription: Subscription;
  public isLoading = true;

  constructor(
    private groupsService: GroupsService,
    private alertController: AlertController,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.myProfileSubscription = this.userService.getMyProfile()
      .subscribe((myProfile) => {
        this.myProfile = myProfile;
        console.log(this.myProfile);
        this.isLoading = false;
      });

    console.log(this.groupMember);
    console.log(this.loadedGroup);
  }

  ngOnDestroy() {
    if (this.myProfileSubscription) {
      this.myProfileSubscription.unsubscribe();
    }
  }

  onUpdateGroupMember(userIdentifier: string, role: string) {
    console.log(userIdentifier);
    console.log(role);

    this.groupsService.updateGroupsMember(this.loadedGroup.identifier, userIdentifier, role)
      .subscribe((updateResponse) => {
        console.log(updateResponse);
      });

  }

  onDeleteGroupMember() {
    console.log(this.groupMember.identifier);

    this.alertController.create({
      header: 'Remove Member from the Group?',
      buttons: [
        {
          text: 'Remove',
          handler: (_) => {

            this.isLoading = true;

            this.groupsService.deleteGroupsMember(this.loadedGroup.identifier, this.groupMember.identifier)
              .subscribe((deleteResponse) => {
                console.log(deleteResponse);

                this.isLoading = false;

              });
          }
        },
        'Cancel'
      ]
    })
      .then((alert) => {
        alert.present();
      });

  }

}
