import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupDetailPageRoutingModule } from './group-detail-routing.module';

import { GroupDetailPage } from './group-detail.page';
import { ShowMemberComponent } from './show-member/show-member.component';
import { AddMemberComponent } from './add-member/add-member.component';
import { EditTaskComponent } from './edit-task/edit-task.component';
import { ShowTaskComponent } from './show-task/show-task.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupDetailPageRoutingModule
  ],
  declarations: [GroupDetailPage, AddMemberComponent, EditTaskComponent, ShowMemberComponent, ShowTaskComponent]
})
export class GroupDetailPageModule {}
