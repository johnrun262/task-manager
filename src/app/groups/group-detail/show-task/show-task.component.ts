import { PopoverController, AlertController } from '@ionic/angular';
import { GroupsService } from './../../groups.service';
import { MemberSelectorOptions } from './../../group.model';
import { UserService } from './../../../users/user.service';
import { TaskModel } from './../../../tasks/task.model';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { GroupMember, GroupModel } from '../../group.model';
import { Subscription } from 'rxjs';
import { UserModel } from 'src/app/users/user.model';
import { EditTaskComponent } from '../edit-task/edit-task.component';

@Component({
  selector: 'app-show-task',
  templateUrl: './show-task.component.html',
  styleUrls: ['./show-task.component.scss'],
})
export class ShowTaskComponent implements OnInit, OnDestroy {
  @Input() groupTask: TaskModel;
  @Input() loadedGroup: GroupModel;
  public myProfile: UserModel;
  private myProfileSubscription: Subscription;
  public isLoading = true;
  public taskOwner: GroupMember;
  public memberSelectorOptions: MemberSelectorOptions[];

  constructor(
    private groupsService: GroupsService,
    private userService: UserService,
    private popoverController: PopoverController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.myProfileSubscription = this.userService.getMyProfile()
      .subscribe((myProfile) => {
        this.myProfile = myProfile;
        console.log(this.myProfile);
        this.isLoading = false;
      });

    this.taskOwner = this.loadedGroup.getMember(this.groupTask.ownerIdentifier);
    this.memberSelectorOptions = this.loadedGroup.getMemberSelectorOptions();

    console.log(this.groupTask);
    console.log(this.taskOwner);
    console.log(this.loadedGroup);
  }

  ngOnDestroy() {
    if (this.myProfileSubscription) {
      this.myProfileSubscription.unsubscribe();
    }
  }

  onChangeTaskOwner(newOwnerIdentifier: string) {
    if (newOwnerIdentifier === this.groupTask.ownerIdentifier) {
      return;
    }

    this.isLoading = true;

    this.groupsService.changeTaskOwner(this.loadedGroup.identifier, this.groupTask.identifier, newOwnerIdentifier)
      .subscribe(
        (response) => {
          console.log(response);
          this.isLoading = false;
        },
        (error) => {
          console.log(error);
          this.isLoading = false;
        });
  }

  onChangeTaskStatus(newStatus: string) {
    if (newStatus === this.groupTask.status) {
      return;
    }

    this.isLoading = true;

    this.groupsService.changeTaskStatus(this.loadedGroup.identifier, this.groupTask.identifier, newStatus)
      .subscribe(
        (response) => {
          console.log(response);
          this.isLoading = false;
        },
        (error) => {
          console.log(error);
          this.isLoading = false;
        });

  }

  onEditTask() {
    this.popoverController.create({
      component: EditTaskComponent,
      componentProps: { group: this.loadedGroup, task: this.groupTask },
      cssClass: "popover-style"
    })
      .then((popover) => {
        popover.present();
      });
  }

  onDeleteTask() {
    this.alertController.create({
      header: 'Delete Task?',
      buttons: [
        {
          text: 'Delete',
          handler: (_) => {
            this.isLoading = true;

            this.groupsService.deleteTask(this.loadedGroup.identifier, this.groupTask.identifier)
              .subscribe(
                (deleteResult) => {
                  console.log('delete');
                  this.isLoading = false;
                },
                (error) => {
                  console.log(error);
                  this.isLoading = false;
                });
          }
        },
        'Cancel'
      ]
    })
      .then((alert) => {
        alert.present();
      });

  }
}
