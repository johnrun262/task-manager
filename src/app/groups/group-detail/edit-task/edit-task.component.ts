import { TaskModel } from '../../../tasks/task.model';
import { NgForm } from '@angular/forms';
import { GroupsService } from '../../groups.service';
import { NavParams, PopoverController, AlertController } from '@ionic/angular';
import { GroupModel, MemberSelectorOptions } from '../../group.model';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/users/user.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.scss'],
})
export class EditTaskComponent implements OnInit {
  private loadedGroup: GroupModel;
  public taskModel: TaskModel;
  public dueDatePickerStart: string = new Date(Date.now()).toISOString();
  public dueDatePickerStop: string = '' + (new Date(Date.now()).getFullYear() + 5);
  public isLoading = true;
  public memberSelectorOptions: MemberSelectorOptions[];
  public multipleOwners: boolean = false;
  public addTask: boolean;

  constructor(
    private groupsService: GroupsService,
    private popoverController: PopoverController,
    private alertController: AlertController,
    private navParams: NavParams,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.loadedGroup = this.navParams.get('group');
    this.taskModel = this.navParams.get('task');

    this.addTask = !this.taskModel;

    console.log(this.taskModel);

    this.memberSelectorOptions = this.loadedGroup.getMemberSelectorOptions();

    this.isLoading = false;
  }

  onAddTask(newTaskForm: NgForm) {
    console.log(newTaskForm);

    if (!newTaskForm.valid) {
      console.log('new task for not valid');
      return;
    }

    this.isLoading = true;

    const name = newTaskForm.value.name;
    const description = newTaskForm.value.description;
    const dueDate = newTaskForm.value.dueDate;
    const taskStatus = newTaskForm.value.taskStatus;
    console.log(taskStatus);

    let owners: string[] = [];

    if (this.multipleOwners) {
      owners = newTaskForm.value.owners;
    } else {
      owners[0] = newTaskForm.value.owner;
    }

    console.log(owners);

    owners.forEach((owner) => {

      if (!this.addTask) {

        const dueDateAsDate = new Date(dueDate);
        let changes = false;

        if (this.taskModel.name !== name || this.taskModel.description !== description ||
          this.taskModel.dueDate.getTime() !== dueDateAsDate.getTime() || this.taskModel.ownerIdentifier !== owner) {

          changes = true;

          this.groupsService.updateTask(
            this.loadedGroup.identifier, this.taskModel.identifier, name, description, dueDateAsDate, owner, false)
            .subscribe(
              (addTaskResponse) => {
                console.log(addTaskResponse);
                this.isLoading = false;
                this.popoverController.dismiss();
              },
              (error) => {
                console.log(error);
                this.isLoading = false;
                this.popoverController.dismiss();
                this.alertController.create(
                  {
                    header: 'Task Update Failed',
                    message: 'Updating a task failed. Refresh the page and try again',
                    buttons: [
                      'OK'
                    ]
                  }
                ).then((alert) => {
                  alert.present();
                });
              });
        }

        if (this.taskModel.status !== taskStatus) {

          changes = true;

          this.groupsService.changeTaskStatus(this.loadedGroup.identifier, this.taskModel.identifier, taskStatus)
            .subscribe(
              (changeTaskStatus) => {
                console.log(changeTaskStatus);
                this.isLoading = false;
                this.popoverController.dismiss();
              },
              (error) => {
                console.log(error);
                this.isLoading = false;
                this.popoverController.dismiss();
                this.alertController.create(
                  {
                    header: 'Task Status Change Failed',
                    message: 'Changing a task status failed. Refresh the page and try again',
                    buttons: [
                      'OK'
                    ]
                  }
                ).then((alert) => {
                  alert.present();
                });
              }
            );
        }

        if (!changes) {
          this.isLoading = false;
          this.popoverController.dismiss();
        }

      } else {

        if (owners.length > 1) {
          this.userService.getUserById(owner)
            .subscribe((userModel) => {
              const taskName: string = name + ' (' + userModel.name + ')';

              this.doAddTask(this.loadedGroup.identifier, taskName, description, dueDate, owner);

            })
        } else {

          this.doAddTask(this.loadedGroup.identifier, name, description, dueDate, owner);

        }

      }

    });
  }

  private doAddTask(identifier, name, description, dueDate, owner) {
    this.groupsService.addTask(
      identifier, name, description, new Date(dueDate), owner, false)
      .subscribe(
        (addTaskResponse) => {
          console.log(addTaskResponse);
          this.isLoading = false;
          this.popoverController.dismiss();
        },
        (error) => {
          console.log(error);
          this.isLoading = false;
          this.popoverController.dismiss();
          this.alertController.create(
            {
              header: 'Task Add Failed',
              message: 'Adding a task failed. Refresh the page and try again',
              buttons: [
                'OK'
              ]
            }
          ).then((alert) => {
            alert.present();
          });
        });
  }

  onToggleMultipleOwners(checked: boolean) {
    console.log(checked)
    this.multipleOwners = checked;
    console.log(this.multipleOwners)
  }

  onCancel() {
    this.popoverController.dismiss();
  }
}
