import { UserModel } from './../../users/user.model';
import { UserService } from './../../users/user.service';
import { NgForm } from '@angular/forms';
import { GroupModel } from './../group.model';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController, GestureController, GestureConfig, PopoverController } from '@ionic/angular';
import { GroupsService } from '../groups.service';
import { Subscription } from 'rxjs';
import { EditTaskComponent } from './edit-task/edit-task.component';
import { AddMemberComponent } from './add-member/add-member.component';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.page.html',
  styleUrls: ['./group-detail.page.scss'],
})
export class GroupDetailPage implements OnInit, OnDestroy {
  private myProfileSubscription: Subscription;
  private groupsSubscription: Subscription;
  public loadedGroup: GroupModel;
  public isLoading: boolean;
  public amOwner = false;
  public amManager = false;
  public isEdit = false;
  public showMembers = false;
  public showTasks = false;
  public ownerCount = 0;
  public myProfile: UserModel;
  private groupIdentifier: string;

  constructor(
    private groupsService: GroupsService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    private alertController: AlertController,
    private popoverController: PopoverController,
    private gestureController: GestureController,
    private elementRef: ElementRef
  ) { }

  ngOnInit() {
    this.isLoading = true;

    const gestureConfig: GestureConfig = {
      gestureName: 'refreshGesture',
      el: this.elementRef.nativeElement,
      direction: 'y',
      blurOnStart: true,
      threshold: 500,
      canStart: () => {
        return !this.isEdit && !this.isLoading;
      },
      onEnd: (event) => {
        console.log(event);

        this.isLoading = true;
        this.onGetGroupByIdentifier(this.groupIdentifier)
      }
    };

    this.gestureController.create(gestureConfig, true)
      .enable();

  }

  ionViewWillEnter() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('groupIdentifier')) {
        this.navController.navigateBack('/groups');
      }

      this.isLoading = true;

      this.groupIdentifier = paramMap.get('groupIdentifier');
      console.log(this.groupIdentifier);

      this.myProfileSubscription = this.userService.getMyProfile()
        .subscribe((myProfile) => {
          console.log(myProfile.identifier);

          this.myProfile = myProfile;

          this.groupsSubscription = this.groupsService.groups
            .subscribe((groups) => {
              this.isLoading = true;

              if (groups) {
                this.loadedGroup = null;

                groups.forEach((group) => {
                  if (group.identifier === this.groupIdentifier &&
                    group.members.length > 0) {
                    console.log(group);
                    this.loadedGroup = group;

                    if (this.loadedGroup.role === 'Owner') {
                      this.amOwner = true;
                    } else if (this.loadedGroup.role === 'Manager') {
                      this.amManager = true;
                    }

                    this.isLoading = false;
                  }
                });
              }

              if (!this.loadedGroup) {
                this.onGetGroupByIdentifier(this.groupIdentifier);
              }

            });
        });

    });
  }

  private onGetGroupByIdentifier(identifier: string) {
    this.groupsService.getGroupByIdentifier(identifier)
      .subscribe(
        (groupModel) => {
          console.log(groupModel);

          this.loadedGroup = groupModel;

          if (this.loadedGroup.role === 'Owner') {
            this.amOwner = true;
          } else if (this.loadedGroup.role === 'Manager') {
            this.amManager = true;
          }

          this.isLoading = false;

        },
        (errorResponse) => {
          console.log(errorResponse);
          this.navController.navigateBack('/groups');
        });
  }

  ngOnDestroy() {
    if (this.groupsSubscription) {
      this.groupsSubscription.unsubscribe();
    }

    if (this.myProfileSubscription) {
      this.myProfileSubscription.unsubscribe();
    }
  }

  onDeleteGroup() {
    console.log('onDeleteGroup');

    this.alertController.create({
      header: 'Delete Group',
      message: 'Deleting the Group will also delete all tasks.',
      buttons: [
        {
          text: 'Delete',
          handler: (_) => {
            this.groupsSubscription.unsubscribe();
            this.groupsService.deleteGroup(this.loadedGroup.identifier)
              .subscribe(
                (response) => {
                  console.log(response);
                  this.navController.navigateBack('/groups');
                },
                (errorResponse) => {
                  console.log(errorResponse);
                  this.navController.navigateBack('/groups');
                });
          }
        },
        'Cancel'
      ]
    })
      .then((alert) => {
        alert.present();
      });
  }

  toggleEditGroup() {
    this.isEdit = !this.isEdit;
  }

  onEditGroup(editForm: NgForm) {
    const groupName = editForm.value.groupName;
    console.log(groupName);

    if (!groupName || groupName.length === 0) {
      console.error('Edit Form Group Name Not Valid');
      return;
    }

    this.groupsService.editGroup(this.loadedGroup.identifier, groupName)
      .subscribe(
        (editResponse) => {
          console.log(editResponse);
          this.loadedGroup = editResponse;
          this.isEdit = false;
        },
        (errorResponse) => {
          console.log(errorResponse);
          this.isEdit = false;
          this.navController.navigateBack('/groups');
        });

    editForm.reset();
  }

  onRefreshGroup() {
    this.isLoading = true;
    this.onGetGroupByIdentifier(this.groupIdentifier);
  }

  onLeaveGroup() {
    console.log('onLeaveGroup');

    this.alertController.create({
      header: 'Leave Group?',
      buttons: [
        {
          text: 'Leave',
          handler: (_) => {
            this.isLoading = true;
            this.groupsSubscription.unsubscribe();
            this.groupsService.deleteGroupsMember(this.loadedGroup.identifier, this.myProfile.identifier)
              .subscribe(
                (deleteResponse) => {
                  console.log(deleteResponse);
                  this.isLoading = false;
                  this.navController.navigateBack('/groups');
                },
                (errorResponse) => {
                  console.log(errorResponse);
                  this.isLoading = false;
                  this.navController.navigateBack('/groups');
                });
          }
        },
        'Cancel'
      ]
    })
      .then((alert) => {
        alert.present();
      });

  }

  onAddMember() {
    this.popoverController.create({
      component: AddMemberComponent,
      componentProps: { group: this.loadedGroup },
      cssClass: "popover-style"
    })
      .then((popover) => {
        popover.present();
      });
  }

  onAddTask() {
    this.popoverController.create({
      component: EditTaskComponent,
      componentProps: { group: this.loadedGroup },
      cssClass: "popover-style"
    })
      .then((popover) => {
        popover.present();
      });
  }

  toggleShowMembers() {
    this.showMembers = !this.showMembers;
  }

  toggleShowTasks() {
    this.showTasks = !this.showTasks;
  }
}
