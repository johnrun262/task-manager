import { GroupsService } from './../../groups.service';
import { GroupModel } from './../../group.model';
import { PopoverController, AlertController, NavParams } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.scss'],
})
export class AddMemberComponent implements OnInit {
  public isLoading = true;
  public loadedGroup: GroupModel;

  constructor(
    private popoverController: PopoverController,
    private alertController: AlertController,
    private navParams: NavParams,
    private groupsService: GroupsService
  ) { }

  ngOnInit() {
    this.loadedGroup = this.navParams.get('group');
    this.isLoading = false;

  }

  onAddNewMember(editForm: NgForm) {
    const newMemberEmail = editForm.value.newMemberEmail;
    const newMemberRole = editForm.value.newMemberRole;

    console.log(newMemberEmail);
    console.log(newMemberRole);

    if (!newMemberEmail || newMemberEmail.length === 0) {
      console.error('Edit Form Email Not Valid');
      return;
    }

    if (!newMemberRole || newMemberRole.length === 0) {
      console.error('Edit Form Role Not Valid');
      return;
    }

    let duplicate = false;
    this.loadedGroup.members.forEach((member) => {
      if (member.email === newMemberEmail) {
        duplicate = true;
      }
    });

    if (duplicate) {
      this.alertController.create({
        header: 'Member Already in Group',
        buttons: ['OK']
      })
        .then((alert) => {
          this.isLoading = false;
          this.popoverController.dismiss();
          alert.present();
        });
      return;
    }

    this.groupsService.addGroupMember(this.loadedGroup.identifier, newMemberEmail, newMemberRole)
      .subscribe((response) => {
        console.log(response);
        this.isLoading = false;
        this.popoverController.dismiss();
      },
      (errorResponse) => {
        console.log(errorResponse);
        this.isLoading = false;
        this.popoverController.dismiss();
        this.alertController.create(
          {
            header: 'Add Group Member Failed',
            message: 'Adding a Member to a Group failed. Refresh the page and try again',
            buttons: [
              'OK'
            ]
          }
        ).then((alert) => {
          alert.present();
        });
      });
  }

  onCancel() {
    this.isLoading = false;
    this.popoverController.dismiss();
  }
}
