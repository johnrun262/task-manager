import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { ProfileGuard } from 'src/app/profile/profile.guard';

import { GroupDetailPage } from './group-detail.page';

const routes: Routes = [
  {
    path: ':groupIdentifier',
    component: GroupDetailPage,
    canLoad: [AuthGuard, ProfileGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupDetailPageRoutingModule {}
