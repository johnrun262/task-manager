import { AddGroupComponent } from './add-group/add-group.component';
import { PopoverController, GestureController, GestureConfig, NavController, AlertController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { GroupModel } from './group.model';
import { GroupsService } from './groups.service';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { UserModel } from '../users/user.model';
import { UserService } from '../users/user.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.page.html',
  styleUrls: ['./groups.page.scss'],
})
export class GroupsPage implements OnInit, OnDestroy {
  loadedGroups: GroupModel[];
  private groupsSubscription: Subscription;
  isLoading = true;
  private scrollAtTop = true;
  private scrollAtBottom = false;
  public myProfile: UserModel;
  private myProfileSubscription: Subscription;


  constructor(
    private groupsService: GroupsService,
    private userService: UserService,
    private popOverController: PopoverController,
    private gestureController: GestureController,
    private elementRef: ElementRef,
    private navController: NavController,
    private alertController: AlertController,
  ) { }

  ngOnInit() {
    this.myProfileSubscription = this.userService.getMyProfile()
      .subscribe((myProfile) => {
        console.log(myProfile.identifier);

        this.myProfile = myProfile;
      });

    this.groupsSubscription = this.groupsService.groups
      .subscribe((groups) => {
        this.loadedGroups = groups;
        this.isLoading = false;
        console.log(this.loadedGroups);
      });

    const gestureConfig: GestureConfig = {
      gestureName: 'refreshGesture',
      el: this.elementRef.nativeElement,
      direction: 'y',
      threshold: 500,
      canStart: () => {
        return !this.isLoading && (this.scrollAtTop || this.scrollAtBottom);
      },
      onEnd: (event) => {
        const direction = event.currentY - event.startY;

        if ((this.scrollAtTop && direction > 0) ||
          (this.scrollAtBottom && direction < 0)) {
          this.isLoading = true;

          this.groupsService.fetchGroups()
            .subscribe(
              (_) => { },
              (errorResponse) => {
                console.log(errorResponse);
                this.isLoading = false;
              });
        }
      }
    };

    this.gestureController.create(gestureConfig, true)
      .enable();

  }

  ngOnDestroy() {
    if (this.groupsSubscription) {
      console.log("groupsSubscription unsubscribe");
      this.groupsSubscription.unsubscribe();
    }

    if (this.myProfileSubscription) {
      this.myProfileSubscription.unsubscribe();
    }
  }

  ionViewWillEnter() {
    this.isLoading = true;

    this.groupsService.fetchGroups()
      .subscribe(
        (_) => { },
        (errorResponse) => {
          console.log(errorResponse);
          this.isLoading = false;
        });
  }

  onAddGroup() {
    this.popOverController.create({
      component: AddGroupComponent,
      cssClass: "popover-style"
    }).then((popOver) => {
      popOver.present();
    });
  }

  onRefreshGroups() {
    this.isLoading = true;

    this.groupsService.fetchGroups()
      .subscribe(
        (_) => { },
        (errorResponse) => {
          console.log(errorResponse);
          this.isLoading = false;
        });
  }

  onClickGroupCard(groupModel: GroupModel) {
    console.log(groupModel);

    if (groupModel.accepted) {
      this.navController.navigateForward(['/', 'groups', 'detail', groupModel.identifier]);
    } else {
      this.onAcceptGroup(groupModel.identifier);
    }
  }

  onAcceptGroup(groupIdentifier: string) {
    console.log('onLeaveGroup');

    this.alertController.create({
      header: 'Accept Group Membership?',
      buttons: [
        {
          text: 'Accept',
          handler: (_) => {
            this.isLoading = true;
            this.groupsService.updateGroupsMemberAccepted(groupIdentifier, true)
              .subscribe(
                (acceptGroupMembershipResponse) => {
                  console.log(acceptGroupMembershipResponse);
                  this.navController.navigateForward(['/', 'groups', 'detail', groupIdentifier]);
                },
                (errorResponse) => {
                  console.log(errorResponse);
                  this.isLoading = false;
                });
          }
        },
        {
          text: 'Reject',
          handler: (_) => {
            this.isLoading = true;
            this.groupsSubscription.unsubscribe();
            this.groupsService.deleteGroupsMember(groupIdentifier, this.myProfile.identifier)
              .subscribe(
                (deleteResponse) => {
                  console.log(deleteResponse);
                  this.isLoading = false;
                },
                (errorResponse) => {
                  console.log(errorResponse);
                  this.isLoading = false;
                });
          }
        },
        'Cancel'
      ]
    })
      .then((alert) => {
        alert.present();
      });

  }

  onScroll(event: any) {
    event.target.getScrollElement()
      .then((scrollElement) => {
        const scrollHeight = scrollElement.scrollHeight - scrollElement.clientHeight;
        const scrollTop = event.detail.scrollTop;

        if (scrollTop === 0) {
          this.scrollAtTop = true;
          this.scrollAtBottom = false;
        } else if (scrollTop === scrollHeight) {
          this.scrollAtTop = false;
          this.scrollAtBottom = true;
        } else {
          this.scrollAtTop = false;
          this.scrollAtBottom = false;
        }
      });
  }
}
