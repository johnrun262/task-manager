import { AuthService } from './auth.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit, OnDestroy {
  public isLogin = true;
  private confirmCodeRequiredSubscription: Subscription;
  public confirmCodeRequiredUserId: string;
  public isLoading = false;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.confirmCodeRequiredSubscription = this.authService.confirmCodeRequired.subscribe((userId) => {
        console.log(userId);
        this.confirmCodeRequiredUserId = userId;
        if (this.confirmCodeRequiredUserId) {
          this.isLogin = true;
        }
    });
  }

  ngOnDestroy() {
    if (this.confirmCodeRequiredSubscription) {
      this.confirmCodeRequiredSubscription.unsubscribe();
    }
  }

  onSwitchAuthMode() {
    this.isLogin = !this.isLogin;
  }

  setIsLoading(isLoading: boolean) {
    this.isLoading = isLoading;
  }
}
