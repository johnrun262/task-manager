import { UserService } from './../users/user.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { TasksService } from './../tasks/tasks.service';
import { GroupsService } from './../groups/groups.service';
import { Injectable } from '@angular/core';
import { AngularFireMessaging } from 'angularfire2/messaging';
import { Observable, Subscription, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Plugins, PushNotification, PushNotificationToken } from '@capacitor/core';
import { environment } from 'src/environments/environment';
import { Platform } from '@ionic/angular';
import { NotificationsService } from '../tasks/notifications.service';

const { PushNotifications } = Plugins;


@Injectable({ providedIn: 'root' })
export class FirebaseMessagingService {
  private receiveMessageSubscription: Subscription;
  private pushNotificationPermissionGranted: Boolean = false;

  constructor(
    private angularFireMessaging: AngularFireMessaging,
    private groupsService: GroupsService,
    private tasksService: TasksService,
    private userService: UserService,
    private notificationsService: NotificationsService,
    private httpClient: HttpClient,
    private platform: Platform
  ) {

    const myProfileSubscription = this.userService.getMyProfile()
      .subscribe((myProfile) => {
        if (myProfile) {

          if (this.platform.is('capacitor')) {
            console.log('capacitor');

            PushNotifications.addListener('registrationError', (error: any) => {
              console.log('Error on registration: ' + JSON.stringify(error));
            });

            PushNotifications.addListener(
              'registration',
              (token: PushNotificationToken) => {
                const deviceId = token.value;
                console.log('Push registration success, token: ' + deviceId);

                this.updateDeviceToken(deviceId)
                  .subscribe((endpointArn) => {
                    console.log(endpointArn);
                    PushNotifications.addListener(
                      'pushNotificationReceived',
                      (notification: PushNotification) => {
                        this.receiveMessage(notification);
                      },
                    );
                  });

              },
            );

            PushNotifications.register();

          } else {
            console.log('not capacitor');

            this.angularFireMessaging.messaging.subscribe(
              // tslint:disable-next-line: variable-name
              (_messaging: any) => {
                _messaging.onMessage = _messaging.onMessage.bind(_messaging);
                _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
              }
            );

            this.angularFireMessaging.requestToken
              .subscribe(
                (deviceId) => {
                  console.log(deviceId);
                  this.updateDeviceToken(deviceId)
                    .subscribe((endpointArn) => {
                      console.log(endpointArn);
                      this.receiveMessageSubscription = this.angularFireMessaging.messages
                        .subscribe((fcmMessage: any) => {
                          this.receiveMessage(fcmMessage);
                        });
                    });
                });
          }

        }

      });

  }

  requestPermission(): Promise<NotificationPermission> {
    if (this.platform.is('capacitor')) {
      PushNotifications.requestPermission().then(result => {
        if (result.granted) {
          console.log('Push notifications request granted');
          this.pushNotificationPermissionGranted = true;
        }
      });
    } else {
      return Notification.requestPermission();
    }
  }

  private receiveMessage(fcmMessage: any) {
    console.log('receiveMessage');
    console.log(fcmMessage);

    if (fcmMessage && fcmMessage.data && fcmMessage.data.default) {
      const payload: { action: string, object: string, data: string } = JSON.parse(fcmMessage.data.default);
      console.log(payload);

      if (payload) {
        let notificationBody = 'A change detected in your groups or tasks';

        if (payload.object === 'group') {
          this.groupsService.fetchGroups().subscribe();
          if (payload.action === 'add') {
            notificationBody = 'You have been added to a group';
          } else if (payload.action === 'update') {
            notificationBody = 'Your group role has been changed';
          }
        } else if (payload.object === 'task') {
          this.tasksService.fetchMyTasks().subscribe();
          if (payload.action === 'create' || payload.action === 'assign') {
            notificationBody = 'You have been assigned to a task';
          } else if (payload.action === 'unassign') {
            notificationBody = 'You have been unassigned from a task';
          } else if (payload.action === 'delete') {
            notificationBody = 'A task you were assigned to has been deleted';
          }
        }

        console.log(notificationBody);

        this.notificationsService.scheduleNotificationNow(notificationBody);

      }
    }

  }

  stopReceiveMessage() {
    if (this.receiveMessageSubscription) {
      this.receiveMessageSubscription.unsubscribe();
    }
    return this.deleteDeviceToken();
  }

  private updateDeviceToken(deviceToken: string): Observable<any> {
    return this.httpClient.patch<{ endPointArn: string }>(
      environment.API_URL + 'users/mydevice',
      {
        deviceToken
      }
    )
      .pipe(
        catchError(this.handleError),
        tap(responseData => {
          console.log('updateDeviceToken');
          console.log(responseData);
        }),
      );

  }

  private deleteDeviceToken(): Observable<any> {
    return this.httpClient.delete(
      environment.API_URL + 'users/mydevice',
    )
      .pipe(
        catchError(this.handleError),
        tap(responseData => {
          console.log('deleteDeviceToken');
        }),
      );

  }

  private handleError(error: HttpErrorResponse) {
    console.error(error);
    return throwError(error);
  }
}
