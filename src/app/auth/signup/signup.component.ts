import { AuthService } from './../auth.service';
import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, NgForm, ValidatorFn, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { typeSourceSpan } from '@angular/compiler';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit, OnDestroy {
  public signupForm: FormGroup;
  public errorMessage: string;
  public isLoading = false;
  @Output() isLoadingEvent = new EventEmitter<boolean>(this.isLoading);

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {

    this.signupForm = new FormGroup({
      userId: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      email: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.email, Validators.maxLength(80)]
      }),
      inputPassword: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(8), this.passwordValidator]
      }),
    });

  }

  ngOnDestroy() {
  }

  onSignup() {
    console.log('onSignup');

    if (!this.signupForm.valid) {
      console.log('not valid');
      return;
    }

    this.isLoading = true;
    this.isLoadingEvent.emit(this.isLoading);

    const userId = this.signupForm.value.userId;
    const email = this.signupForm.value.email;
    const password = this.signupForm.value.inputPassword;

    this.authService.signUp(userId, email, password)
      .then((_) => {
      })
      .catch((errorMessage) => {
        console.error(errorMessage);
        this.errorMessage = errorMessage;
      })
      .finally(() => {
        this.isLoading = false;
        this.isLoadingEvent.emit(this.isLoading);
      });

  }

  private passwordValidator(formControl: FormControl) {
    const hasNumber = /\d/.test(formControl.value);
    const hasLower = /[A-Z]/.test(formControl.value);
    const hasUpper = /[a-z]/.test(formControl.value);

    if (hasLower && hasUpper && hasNumber) {
      return null;
    }

    return { weak: true };
  }
}
