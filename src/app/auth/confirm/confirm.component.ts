import { AuthService } from './../auth.service';
import { NgForm } from '@angular/forms';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmComponent implements OnInit, OnDestroy {
  @Input() userId: string;
  public errorMessage: string;
  public isLoading = false;

  constructor(
    private authService: AuthService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  onSubmit(confirmForm: NgForm) {
    console.log(confirmForm);

    this.isLoading = true;

    const confirmationCode = confirmForm.value.confirmationCode;

    console.log(confirmationCode);
    console.log(this.userId);

    this.authService.confirmUser(this.userId, confirmationCode)
      .then((_) => {
        this.alertController.create(
          {
            header: 'Verification Code',
            message: 'Code Accepted. Login to Continue',
            buttons: [
              'OK'
            ]
          }
        ).then((alert) => {
          alert.present();
        });
      })
      .catch((errorMessage) => {
        console.error(errorMessage);
        this.errorMessage = errorMessage;
      })
      .finally(() => {
        this.isLoading = false;
      });

  }

  onResendCode() {
    this.authService.resendConfirmationCode(this.userId)
      .then((_) => {
        this.alertController.create(
          {
            header: 'Verification Code',
            message: 'Check Email for the Verification Code',
            buttons: [
              'OK'
            ]
          }
        ).then((alert) => {
          alert.present();
        });
      })
      .catch((errorMessage) => {
        console.error(errorMessage);
        this.errorMessage = errorMessage;
      });
  }
}
