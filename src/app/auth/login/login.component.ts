import { FirebaseMessagingService } from './../firebase-messaging.service';
import { AuthService } from './../auth.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public errorMessage: string;
  public isLoading = false;
  @Output() isLoadingEvent = new EventEmitter<boolean>(this.isLoading);

  constructor(
    private authService: AuthService,
    private popOverController: PopoverController,
    private firebaseMessagingService: FirebaseMessagingService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  onSubmit(loginForm: NgForm) {
    this.isLoading = true;
    this.isLoadingEvent.emit(this.isLoading);

    const userId = loginForm.value.userId;
    const password = loginForm.value.password;

    this.authService.signIn(userId, password)
      .then((_) => {
        this.firebaseMessagingService.requestPermission();
      })
      .catch((errorMessage) => {
        console.error(errorMessage);
        this.errorMessage = errorMessage;

      })
      .finally(() => {
        this.isLoading = false;
        this.isLoadingEvent.emit(this.isLoading);
      });

  }

  onForgotPassword() {
    this.popOverController.create({
      component: ForgotPasswordComponent,
      cssClass: "popover-style"
    }).then((popover) => {
      popover.present();
    });
  }

}
