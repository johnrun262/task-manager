import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpParams, HttpHandler, HttpHeaders } from '@angular/common/http';
import { map, take, exhaustMap, switchMap, mergeMap } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    return this.authService.getIdToken()
      .pipe(
        mergeMap((token) => {
          request = request.clone(
            {
              headers: new HttpHeaders({Authorization: token})
            }
          );
          return next.handle(request);

        })
      );
  }
}
