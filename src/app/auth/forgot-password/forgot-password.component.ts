import { PopoverController, AlertController } from '@ionic/angular';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  public errorMessage: string;
  public isLoading = false;
  public inputResetCode = false;
  private userId: string;
  public confirmForm: FormGroup;

  constructor(
    private authService: AuthService,
    private popoverController: PopoverController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.confirmForm = new FormGroup({
      code: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required]
      }),
      newPassword: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(8), this.passwordValidator]
      }),
    });
  }

  onSendResetCode(resetForm: NgForm) {
    this.isLoading = true;

    this.userId = resetForm.value.userId;

    this.authService.resetPassword(this.userId)
      .then((_) => {
        console.log('reset code request sent');
        this.isLoading = false;
        this.inputResetCode = true;

      })
      .catch((errorMessage) => {
        console.error(errorMessage);
        this.errorMessage = errorMessage;
        this.isLoading = false;
      });
  }

  onVerifyCode() {
    const code = this.confirmForm.value.code;
    const newPassword = this.confirmForm.value.newPassword;

    console.log(code);
    console.log(newPassword);
    console.log(this.userId);

    this.authService.confirmResetCode(this.userId, code, newPassword)
      .then((_) => {
      })
      .catch((errorMessage) => {
        console.error(errorMessage);
        this.errorMessage = errorMessage;
      })
      .finally(() => {
        this.alertController.create(
          {
            header: 'Password Reset',
            message: this.errorMessage ? this.errorMessage : 'Your Password has been Changed',
            buttons: [
              'OK'
            ]
          }
        ).then((alert) => {
          alert.present()
            .then((_) => {
              this.popoverController.dismiss();
            });
        });
      });
  }

  private passwordValidator(formControl: FormControl) {
    const hasNumber = /\d/.test(formControl.value);
    const hasLower = /[A-Z]/.test(formControl.value);
    const hasUpper = /[a-z]/.test(formControl.value);

    if (hasLower && hasUpper && hasNumber) {
      return null;
    }

    return { weak: true };
  }
}
