import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable, of } from 'rxjs';
import { take, tap, switchMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const isAuthenticated = this.authService.userIsAuthenticated();
    console.log(isAuthenticated);
    return isAuthenticated
      .pipe(
        take(1),
        tap(userIsAuthenticated => {
          console.log(userIsAuthenticated);

          if (!userIsAuthenticated) {
            this.router.navigateByUrl('/auth');
          }
        })
      );
  }
}

