import { UserService } from './../users/user.service';
import { FirebaseMessagingService } from './firebase-messaging.service';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { AuthenticationDetails, CognitoUser, CognitoUserAttribute, CognitoUserPool, CognitoUserSession } from 'amazon-cognito-identity-js';
import { BehaviorSubject, from, Observable, of, Subscription, throwError } from 'rxjs';
import { TasksService } from '../tasks/tasks.service';
import { GroupsService } from '../groups/groups.service';


const userPool = new CognitoUserPool(environment.COGNITO_POOL_DATA);

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  confirmCodeRequired = new BehaviorSubject<string>(null);
  authStatus = new BehaviorSubject<boolean>(false);


  constructor(
    private userService: UserService,
    private groupsService: GroupsService,
    private tasksService: TasksService,
    private firebaseMessagingService: FirebaseMessagingService
  ) {
  }

  getAuthenticatedUser(): CognitoUser {
    return userPool.getCurrentUser();
  }

  getIdToken(): Observable<string> {
    const user = this.getAuthenticatedUser();

    if (!user) {
      return of(null);
    }

    return from<Promise<string>>(
      new Promise((resolve, reject) => {
        user.getSession((err, session) => {
          if (err) {
            reject(err);
          } else if (!session.isValid()) {
            resolve(null);
          } else {
            resolve(session.getIdToken().getJwtToken() as string);
          }
        });
      })
    );

  }

  getProfileAttributes(): Promise<any> {
    const user = this.getAuthenticatedUser();

    return new Promise((resolve, reject) => {

      user.getSession((err, session) => {
        if (err) {
          reject(err);
        } else if (!session.isValid()) {
          resolve(null);
        } else {
          console.log(session.getIdToken());
          resolve(session.getIdToken().payload);
        }
      });

    });

  }

  userIsAuthenticated(): Observable<boolean> {
    const user = this.getAuthenticatedUser();
    const obs = new Observable<boolean>((observer) => {
      if (!user) {
        console.log('false');
        observer.next(false);
      } else {
        user.getSession(
          (error, session) => {
            if (error) {
              console.log(error);
            } else {
              if (session.isValid()) {
                console.log('true');
                observer.next(true);
              } else {
                console.log('false');
                observer.next(false);
              }
            }
          }
        );
      }
      observer.complete();
    });
    return obs;
  }

  initAuth() {
    this.userIsAuthenticated().subscribe(
      (auth) => this.authStatus.next(auth)
    );
  }

  signUp(username: string, email: string, password: string): Promise<string> {

    const cognitoUserAttributeList: CognitoUserAttribute[] = [];

    const emailAttribute = {
      Name: 'email',
      Value: email
    };

    cognitoUserAttributeList.push(new CognitoUserAttribute(emailAttribute));

    return new Promise((resolve, reject) => {
      userPool.signUp(
        username,
        password,
        cognitoUserAttributeList,
        null,
        (error, result) => {
          if (error) {
            console.log(error);
            reject(error.message);
          } else {
            this.confirmCodeRequired.next(username);
            resolve('Signup Success');
          }
        }
      );

    });

  }

  confirmUser(username: string, code: string): Promise<string> {
    const userData = {
      Username: username,
      Pool: userPool
    };

    const cognitoUser: CognitoUser = new CognitoUser(userData);

    return new Promise((resolve, reject) => {
      cognitoUser.confirmRegistration(
        code,
        true,
        (error, result) => {
          if (error) {
            console.error(error);
            reject(error.message);
          } else {
            this.confirmCodeRequired.next(null);
            resolve('Confirmed');
          }

        });
    });

  }

  resendConfirmationCode(username: string): Promise<string> {
    const userData = {
      Username: username,
      Pool: userPool
    };

    const cognitoUser: CognitoUser = new CognitoUser(userData);

    return new Promise((resolve, reject) => {
      cognitoUser.resendConfirmationCode(
        (error, result) => {
          if (error) {
            console.error(error);
            reject(error.message);
          } else {
            resolve('Code Resent');
          }

        });
    });

  }

  signIn(username: string, password: string): Promise<string> {
    const authData = {
      Username: username,
      Password: password
    };

    const authenticationDetails: AuthenticationDetails = new AuthenticationDetails(authData);

    const userData = {
      Username: username,
      Pool: userPool
    };

    const cognitoUser: CognitoUser = new CognitoUser(userData);

    const that = this;

    return new Promise((resolve, reject) => {
      cognitoUser.authenticateUser(
        authenticationDetails,
        {
          onSuccess: (result: CognitoUserSession) => {
            this.authStatus.next(true);
            resolve('SignIn Success');
          },
          onFailure: (error) => {
            console.log(error);

            if (error.code === 'UserNotConfirmedException') {
              this.confirmCodeRequired.next(username);
              resolve('SignIn Success Code Required');
            }

            reject(error.message);
          },
          newPasswordRequired: (userAttributes, requiredAttributes) => {
            console.log('forced password change');
            console.log(userAttributes);

            delete userAttributes.email_verified;

            cognitoUser.completeNewPasswordChallenge(password, userAttributes,
              {
                onSuccess: (result: CognitoUserSession) => {
                  this.authStatus.next(true);
                  console.log('password changed');
                  resolve('SignIn Success');
                },
                onFailure: (error) => {
                  console.log(error);
                  reject(error.message);
                }
              });
          }
        }
      );
    });

  }

  deleteUser(username: string) {
    console.log(username);

    const userData = {
      Username: username,
      Pool: userPool
    };

    const cognitoUser: CognitoUser = new CognitoUser(userData);

    return new Promise((sessionResolve, sessionReject) => {
      cognitoUser.getSession((sessionError, session) => {

        if (sessionError) {
          console.error(sessionError);
          sessionReject('Failed to Get Session for User');
        } else if (session.isValid()) {
          sessionResolve('Got Session');
        }

      });
    })
      .then((_) => {

        return new Promise((deleteResolve, deleteReject) => {
          cognitoUser.deleteUser(
            (error, result) => {
              if (error) {
                console.log(error);
                deleteReject(error.message);
              } else {
                deleteResolve('Deleted from User Pool ' + username);
              }

            });
        });

      });

  }

  changePassword(username: string, oldPassword: string, newPassword: string): Promise<string> {
    console.log(username);

    const userData = {
      Username: username,
      Pool: userPool
    };

    const cognitoUser: CognitoUser = new CognitoUser(userData);

    return new Promise((sessionResolve, sessionReject) => {
      cognitoUser.getSession((sessionError, session) => {

        if (sessionError) {
          console.error(sessionError);
          sessionReject('Failed to Get Session for User');
        } else if (session.isValid()) {
          sessionResolve('Got Session');
        }

      });
    })
      .then((_) => {
        return new Promise((changeResolve, changeReject) => {
          cognitoUser.changePassword(oldPassword, newPassword,
            (error, result) => {
              if (error) {
                console.error(error);

                let message = error.message;

                if (error.name === 'NotAuthorizedException' ||
                  error.name === 'InvalidPasswordException' ||
                  error.name === 'InvalidParameterException') {
                  message = 'Incorrect Current Password';
                } else if (error.name === 'LimitExceededException') {
                  message = 'Exceeded Password Change Attempts';
                } else if (error.name === 'PasswordResetRequiredException') {
                  message = 'Current Password Must be Reset';
                }

                changeReject(message);
              } else {
                changeResolve('Password Changed');
              }

            });
        });

      });
  }

  resetPassword(username: string): Promise<string> {
    console.log(username);

    const userData = {
      Username: username,
      Pool: userPool
    };

    const cognitoUser: CognitoUser = new CognitoUser(userData);

    return new Promise((resolve, reject) => {
      cognitoUser.forgotPassword({
        onSuccess: (data) => {
          resolve('Reset Code Sent');
        },
        onFailure: (error) => {
          console.error(error);
          reject(error.message);
        }
      }
      );
    });

  }

  confirmResetCode(username: string, code: string, newPassword: string): Promise<string> {
    console.log(username);

    const userData = {
      Username: username,
      Pool: userPool
    };

    const cognitoUser: CognitoUser = new CognitoUser(userData);

    return new Promise((resolve, reject) => {
      cognitoUser.confirmPassword(
        code,
        newPassword,
        {
          onSuccess: () => {
            resolve('Password Reset');
          },
          onFailure: (error) => {
            console.error(error);
            reject(error.message);
          }
        }
      );
    });

  }

  logout() {
    this.authStatus.next(false);
    this.firebaseMessagingService.stopReceiveMessage()
      .subscribe(
        (_) => { })
      .add(() => {
        console.log('sign out');
        this.tasksService.onLogout();
        this.groupsService.onLogout();
        this.userService.onLogout();
        this.getAuthenticatedUser().signOut();
      });
  }

}
