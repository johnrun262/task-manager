import { TasksService } from './../tasks.service';
import { Component, Input, OnInit } from '@angular/core';
import { TaskModel } from '../task.model';
import { AlertController } from '@ionic/angular';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss'],
})
export class TaskDetailComponent implements OnInit {
  @Input() myTask: TaskModel;
  public isLoading = false;

  constructor(
    private tasksService: TasksService,
    private calendarService: CalendarService,
    private alertController: AlertController
  ) { }

  ngOnInit() { }

  onChangeStatus(newStatus: string) {
    console.log(newStatus);

    this.alertController.create({
      header: 'Change Status to ' + newStatus,
      buttons: [
        {
          text: 'Confirm',
          handler: (_) => {
            this.isLoading = true;

            this.tasksService.changeTaskStatus(this.myTask.identifier, newStatus)
              .subscribe(
                (taskModel) => {
                  console.log('task status changed');
                  console.log(taskModel);
                  this.isLoading = true;
                  if (newStatus.toLowerCase() === 'accepted') {
                    console.log('will try add to calendar');
                    this.calendarService.addToCalendar(this.myTask);
                  }
                },
                (errorResponse) => {
                  console.log(errorResponse);
                  this.isLoading = true;
                });
          }
        },
        'Cancel'
      ]
    })
      .then((alert) => {
        alert.present();
      });

  }
}
