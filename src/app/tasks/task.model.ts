import { UserModel } from './../users/user.model';
export class TaskModel {
    constructor(
        public identifier: string,
        public name: string,
        public description: string,
        public dueDate: Date,
        public ownerIdentifier: string,
        public groupIdentifier: string,
        public repeats: boolean,
        public status: string
    ) { }
}
