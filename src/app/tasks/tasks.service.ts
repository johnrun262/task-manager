import { TaskModel } from './task.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError, BehaviorSubject, from } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, map, tap } from 'rxjs/operators';
import { NotificationsService } from './notifications.service';


class TaskResponse {
  identifier: string;
  name: string;
  description: string;
  dueDate: string;
  repeats: boolean;
  status: string;
  ownerIdentifier: string;
  groupIdentifier: string;
}

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  public myTasks = new BehaviorSubject<TaskModel[]>(null);

  constructor(
    private httpClient: HttpClient,
    private notificationsService: NotificationsService
  ) { }

  fetchTasksByGroup(groupIdentifier: string): Observable<TaskModel[]> {
    return this.httpClient.get<TaskResponse[]>(
      environment.API_URL + 'groups/' + groupIdentifier + '/tasks'
    )
      .pipe(
        catchError(this.handleError),
        map((tasksResponse) => {
          console.log(tasksResponse);

          const receivedTasks: TaskModel[] = [];

          tasksResponse.forEach((task) => {

            const taskModel = new TaskModel(
              task.identifier,
              task.name,
              task.description,
              new Date(task.dueDate),
              task.ownerIdentifier,
              task.groupIdentifier,
              task.repeats,
              task.status
            );

            receivedTasks.push(taskModel);
          });


          return receivedTasks;
        })
      );
  }

  fetchMyTasks(): Observable<TaskModel[]> {
    return this.httpClient.get<TaskResponse[]>(
      environment.API_URL + 'tasks/mytasks'
    )
      .pipe(
        catchError(this.handleError),
        map((tasksResponse) => {
          console.log(tasksResponse);

          const myTasks: TaskModel[] = [];

          tasksResponse.forEach((task, taskIndex) => {

            const taskModel = new TaskModel(
              task.identifier,
              task.name,
              task.description,
              new Date(task.dueDate),
              task.ownerIdentifier,
              task.groupIdentifier,
              task.repeats,
              task.status
            );

            myTasks.push(taskModel);

            this.notificationsService.scheduleTaskNotification(taskModel);
          });

          this.notificationsService.clearCompletedTaskNotifications(myTasks);
          this.myTasks.next(myTasks);

          return myTasks;
        })
      );
  }

  addTask(groupIdentifier: string, name: string, description: string, dueDate: Date,
    // tslint:disable-next-line: align
    owner: string, repeats: boolean): Observable<TaskModel> {
    return this.httpClient.post<TaskResponse>(
      environment.API_URL + 'tasks/',
      {
        group: groupIdentifier,
        name,
        description,
        dueDate: dueDate.toISOString(),
        owner,
        repeats
      }
    )
      .pipe(
        catchError(this.handleError),
        map((response) => {
          console.log(response);

          const newTask = new TaskModel(
            response.identifier,
            response.name,
            response.description,
            new Date(response.dueDate),
            response.ownerIdentifier,
            response.groupIdentifier,
            response.repeats,
            response.status
          );

          this.notificationsService.scheduleTaskNotification(newTask);

          return newTask;
        })
      );
  }

  updateTask(taskIdentifier: string, name: string, description: string, dueDate: Date,
    // tslint:disable-next-line: align
    owner: string, repeats: boolean): Observable<TaskModel> {
    return this.httpClient.patch<TaskResponse>(
      environment.API_URL + 'tasks/' + taskIdentifier,
      {
        name,
        description,
        dueDate: dueDate.toISOString(),
        owner,
        repeats
      }
    )
      .pipe(
        catchError(this.handleError),
        map((response) => {
          console.log(response);

          const newTask = new TaskModel(
            taskIdentifier,
            response.name,
            response.description,
            new Date(response.dueDate),
            response.ownerIdentifier,
            response.groupIdentifier,
            response.repeats,
            response.status
          );

          this.notificationsService.scheduleTaskNotification(newTask);

          return newTask;
        })
      );
  }

  changeTaskOwner(taskIdentifier: string, ownerIdentifier: string): Observable<TaskModel> {
    return this.httpClient.patch<TaskResponse>(
      environment.API_URL + 'tasks/' + taskIdentifier + '/owner',
      {
        newOwner: ownerIdentifier
      }
    )
      .pipe(
        catchError(this.handleError),
        map((response) => {
          console.log(response);

          const newTask = new TaskModel(
            response.identifier,
            response.name,
            response.description,
            new Date(response.dueDate),
            response.ownerIdentifier,
            response.groupIdentifier,
            response.repeats,
            response.status
          );

          this.notificationsService.scheduleTaskNotification(newTask);

          return newTask;
        })
      );
  }

  changeTaskStatus(taskIdentifier: string, status: string): Observable<TaskModel> {
    return this.httpClient.patch<TaskResponse>(
      environment.API_URL + 'tasks/' + taskIdentifier + '/status',
      {
        status
      }
    )
      .pipe(
        catchError(this.handleError),
        map((response) => {
          console.log(response);

          const newTask = new TaskModel(
            response.identifier,
            response.name,
            response.description,
            new Date(response.dueDate),
            response.ownerIdentifier,
            response.groupIdentifier,
            response.repeats,
            response.status
          );

          const myTasks = this.myTasks.getValue();
          myTasks.forEach((task, taskIndex) => {
            if (task.identifier === taskIdentifier) {
              myTasks[taskIndex] = newTask;
            }
          });

          console.log(myTasks);
          this.myTasks.next([...myTasks]);

          return newTask;
        })
      );
  }

  deleteTask(taskIdentifier: string) {
    return this.httpClient.delete(
      environment.API_URL + 'tasks/' + taskIdentifier
    )
      .pipe(
        catchError(this.handleError),
        tap((deleteResponse) => {
          console.log(deleteResponse);

          this.notificationsService.cancelNotification(taskIdentifier);
        })
      );
  }

  onLogout() {
    this.myTasks.next(null);
  }

  private handleError(error: HttpErrorResponse) {
    console.error(error);
    return throwError(error);
  }

}
