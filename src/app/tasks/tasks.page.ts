import { TaskModel } from './task.model';
import { Subscription } from 'rxjs';
import { TasksService } from './tasks.service';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { GestureConfig, GestureController } from '@ionic/angular';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.page.html',
  styleUrls: ['./tasks.page.scss'],
})
export class TasksPage implements OnInit, OnDestroy {
  private myTasksSubscription: Subscription;
  public myTasks: TaskModel[];
  public isLoading = true;
  private scrollAtTop = true;
  private scrollAtBottom = false;

  constructor(
    private tasksService: TasksService,
    private gestureController: GestureController,
    private elementRef: ElementRef
  ) { }

  ngOnInit() {
    this.myTasksSubscription = this.tasksService.myTasks
      .subscribe((myTasks) => {
        if (myTasks) {
          myTasks.sort((e1, e2) => {
            if (e1.status.toLowerCase() === 'complete' &&
              e2.status.toLowerCase() === 'complete') {
              return e1.dueDate.getTime() - e2.dueDate.getTime();
            }

            if (e1.status.toLowerCase() === 'complete' &&
              e2.status.toLowerCase() !== 'complete') {
              return 1;
            }

            if (e2.status.toLowerCase() === 'complete' &&
              e1.status.toLowerCase() !== 'complete') {
              return -1;
            }

            return e1.dueDate.getTime() - e2.dueDate.getTime();
          });
        }

        this.myTasks = myTasks;
        this.isLoading = false;
      });

    const gestureConfig: GestureConfig = {
      gestureName: 'refreshGesture',
      el: this.elementRef.nativeElement,
      direction: 'y',
      threshold: 500,
      canStart: () => {
        return !this.isLoading && (this.scrollAtTop || this.scrollAtBottom);
      },
      onEnd: (event) => {
        const direction = event.currentY - event.startY;

        if ((this.scrollAtTop && direction > 0) ||
          (this.scrollAtBottom && direction < 0)) {
          this.isLoading = true;

          this.tasksService.fetchMyTasks()
            .subscribe(
              (_) => { },
              (errorResponse) => {
                console.log(errorResponse);
                this.isLoading = false;
              });
        }
      }
    };

    this.gestureController.create(gestureConfig, true)
      .enable();
  }

  ngOnDestroy() {
    if (this.myTasksSubscription) {
      this.myTasksSubscription.unsubscribe();
    }
  }

  ionViewWillEnter() {
    this.isLoading = true;

    this.tasksService.fetchMyTasks()
      .subscribe(
        (_) => { },
        (errorResponse) => {
          console.log(errorResponse);
          this.isLoading = false;
        });
  }

  onRefreshTasks() {
    this.isLoading = true;

    this.tasksService.fetchMyTasks()
      .subscribe(
        (_) => { },
        (errorResponse) => {
          console.log(errorResponse);
          this.isLoading = false;
        });
  }

  onScroll(event: any) {
    event.target.getScrollElement()
      .then((scrollElement) => {
        const scrollHeight = scrollElement.scrollHeight - scrollElement.clientHeight;
        const scrollTop = event.detail.scrollTop;

        if (scrollTop === 0) {
          this.scrollAtTop = true;
          this.scrollAtBottom = false;
        } else if (scrollTop === scrollHeight) {
          this.scrollAtTop = false;
          this.scrollAtBottom = true;
        } else {
          this.scrollAtTop = false;
          this.scrollAtBottom = false;
        }
      });
  }
}
