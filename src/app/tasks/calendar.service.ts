import { Injectable } from '@angular/core';
import { Calendar, CalendarOptions } from '@ionic-native/calendar/ngx';
import { Platform } from '@ionic/angular';
import { UserModel } from '../users/user.model';
import { UserService } from '../users/user.service';
import { TaskModel } from './task.model';


@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  private calendarInitialized: boolean = false;
  private myProfile: UserModel;

  constructor(
    private calendar: Calendar,
    private platform: Platform,
    private userService: UserService
  ) {

    this.userService.getMyProfile()
      .subscribe((myProfile) => {
        this.myProfile = myProfile;
        console.log(this.myProfile);
        if (this.myProfile.addTasksToCalendar && !this.calendarInitialized) {
          if (this.calendarPlatform()) {
            console.log('calendar platform');
            this.calendar.createCalendar('Task Manager').then(
              (msg) => {
                console.log(msg);
                this.calendarInitialized = true;
              },
              (err) => {
                console.log(err);
              }
            );
          }
        }
      });

  }

  private createCalendarEvent(taskModel: TaskModel) {
    const calendarOptions: CalendarOptions = {
      id: taskModel.identifier
    };

    return this.calendar.createEventWithOptions(
      taskModel.name,
      '',
      taskModel.description,
      taskModel.dueDate,
      taskModel.dueDate,
      calendarOptions
    )
      .then(
        (_) => {
          console.log('task added to calendar');
        },
        (error) => {
          console.log(error);
        });

  }

  public addToCalendar(taskModel: TaskModel) {
    console.log(this.myProfile);
    if (this.myProfile && this.myProfile.addTasksToCalendar) {
      console.log('addTasksToCalendar true');
      if (this.calendarInitialized) {
        console.log('calendar initialized');
        this.calendar.hasReadWritePermission()
          .then(
            (writable) => {
              if (writable) {
                console.log('writable');
                this.createCalendarEvent(taskModel);
              } else {
                console.log('requesting write permission');
                this.calendar.requestWritePermission()
                  .then((v) => {
                    console.log('got write permission');
                    this.createCalendarEvent(taskModel);
                  }, (_) => {
                    console.log("Rejected");
                  });
              }
            },
            (error) => {
              console.log(error);
            }
          );

      }
    }
  }

  public calendarPlatform(): boolean {
    return this.platform.is('capacitor');
  }
}
