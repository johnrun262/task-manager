import { UserModel } from './../users/user.model';
import { UserService } from './../users/user.service';
import { Injectable } from '@angular/core';
import {
  LocalNotification, LocalNotificationAction, LocalNotificationActionType, LocalNotificationEnabledResult, LocalNotificationPendingList, LocalNotificationRequest, LocalNotificationSchedule, Plugins
} from '@capacitor/core';
import { TaskModel } from './task.model';
import { Platform } from '@ionic/angular';

const { LocalNotifications } = Plugins;

class LocalStorageNotification {
  notificationId: string;
  taskIdentifier: string;
  dueDate: string;
}

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  private storageKeyPrefix = 'notification:task:';
  private myProfile: UserModel;
  private actionTypeRegistered: boolean = false;

  constructor(
    private userService: UserService,
    private platform: Platform
  ) {
    this.userService.getMyProfile()
      .subscribe((myProfile) => {
        if (myProfile) {
          this.myProfile = myProfile;
        }
      });

    if (this.platform.is('capacitor')) {
      const localNotificationAction: LocalNotificationAction = {
        id: "launch-app",
        title: "Launch Task Manager",
        requiresAuthentication: true,
        foreground: true

      };

      const localNotificationActionType: LocalNotificationActionType = {
        id: "launch-app",
        actions: [localNotificationAction]
      };

      LocalNotifications.registerActionTypes({
        types: [localNotificationActionType]
      })
        .then((_) => {
          this.actionTypeRegistered = true;
        })
    }
  }

  private areEnabled(): Promise<LocalNotificationEnabledResult> {
    return LocalNotifications.areEnabled();
  }

  public scheduleNotificationNow(body: string) {
    this.areEnabled()
      .then((notificationsEnabled) => {
        console.log(notificationsEnabled);

        if (notificationsEnabled) {
          LocalNotifications.schedule({
            notifications: [{
              id: 0,
              body: body,
              title: 'Task Manager',
              actionTypeId: "launch-app",
              smallIcon: 'ic_stat_name'
            }]
          });
        }
      });

  }

  public scheduleTaskNotification(taskModel: TaskModel) {
    const now = new Date(Date.now());

    if (!this.myProfile || taskModel.ownerIdentifier !== this.myProfile.identifier) {
      console.log('not my task ' + taskModel.identifier);
    }

    this.areEnabled()
      .then((notificationsEnabled) => {
        console.log(notificationsEnabled);

        this.readStoredNotification(taskModel.identifier)
          .then((localStorageNotification) => {
            console.log(localStorageNotification);
            console.log(taskModel);

            if (taskModel.status === 'Working' || taskModel.status === 'Accepted') {
              console.log('working task schedule notification ' + taskModel.name);

              if (!localStorageNotification) {
                this.doSchedule(taskModel);

              } else if (new Date(localStorageNotification.dueDate).getTime() !== taskModel.dueDate.getTime()) {
                console.log('rescheduing - due date changed');
                console.log(new Date(localStorageNotification.dueDate));
                console.log(taskModel.dueDate);

                this.cancelNotification(taskModel.identifier)
                  .then((cancelResponse) => {
                    this.doSchedule(taskModel);

                  });

              }

            } else {
              console.log('not working task no notification ' + taskModel.name);

              if (localStorageNotification) {
                this.cancelNotification(taskModel.identifier);
              }
              return;
            }

          });

      });
  }

  private doSchedule(taskModel: TaskModel) {
    const now = new Date(Date.now());
    let localNotificationSchedule: LocalNotificationSchedule;

    if (taskModel.dueDate.getTime() <= now.getTime()) {
      localNotificationSchedule = null;
    } else {
      localNotificationSchedule = {
        repeats: false,
        at: taskModel.dueDate,
      };
    }

    let notificationBody: string;

    if (taskModel.dueDate.getTime() < now.getTime()) {
      notificationBody = 'Past Due: ';
    } else {
      notificationBody = 'Due today: ';
    }

    const notificationId = Math.round(Math.random() * 10000000);
    const notification: LocalNotification = {
      id: notificationId,
      title: 'Task Master',
      body: notificationBody + taskModel.name,
      schedule: localNotificationSchedule,
      group: 'tasks',
      actionTypeId: "launch-app",
      smallIcon: 'ic_stat_name'
    };

    const notifications: LocalNotification[] = [];
    notifications.push(notification);

    LocalNotifications.schedule({ notifications })
      .then((notificationResult) => {
        console.log(notificationResult);

        this.writeNotificationToStorage(taskModel, notificationResult.notifications[0].id);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  public clearCompletedTaskNotifications(taskModels: TaskModel[]) {
    const taskIdentifiers: string[] = [];

    if (taskModels && taskModels.length > 0) {

      taskModels.forEach((taskModel) => {
        if (!this.myProfile || taskModel.ownerIdentifier !== this.myProfile.identifier) {
          console.log('not my task - cancel any notification ' + taskModel.name);
          this.cancelNotification(taskModel.identifier);

        } else if (taskModel.status === 'Verify' || taskModel.status === 'Complete') {
          console.log('task verify or complete - cancel any notification ' + taskModel.name);
          this.cancelNotification(taskModel.identifier);

        }

        taskIdentifiers.push(taskModel.identifier);

      });

    }

    console.log(taskIdentifiers);

    Plugins.Storage.keys()
      .then((keys) => {
        if (keys && keys.keys) {
          keys.keys.forEach((key) => {
            const storageKeyPrefixIndex = key.indexOf(this.storageKeyPrefix);
            if (storageKeyPrefixIndex !== -1) {
              const taskIdentiferStartIndex = storageKeyPrefixIndex + this.storageKeyPrefix.length;
              const taskIdentifier = key.substring(taskIdentiferStartIndex);

              if (taskIdentifiers.indexOf(taskIdentifier) < 0) {
                console.log('not in tasks list - cancelling ' + taskIdentifier);
                this.cancelNotification(taskIdentifier);
              }

            }
          });
        }
      });

  }

  public cancelNotification(taskIdentifier: string) {
    return this.readStoredNotification(taskIdentifier)
      .then((localStorageNotification) => {
        console.log(localStorageNotification);

        if (localStorageNotification) {
          console.log(localStorageNotification.notificationId);
          const localNotificationRequest: LocalNotificationRequest = { id: localStorageNotification.notificationId };
          const localNotificationRequests: LocalNotificationRequest[] = [];
          localNotificationRequests.push(localNotificationRequest);

          const localNotificationPendingList: LocalNotificationPendingList = {
            notifications: localNotificationRequests
          };


          return LocalNotifications.cancel(localNotificationPendingList)
            .then((cancelResponse) => {
              this.removeNotificationInStorage(taskIdentifier)
                .then((removeResponse) => {
                });
            });

        }
      });
  }

  private readStoredNotification(taskIdentifier: string): Promise<LocalStorageNotification> {
    return Plugins.Storage.get({ key: this.storageKeyPrefix + taskIdentifier })
      .then((storedData) => {
        if (!storedData || !storedData.value) {
          console.log('no stored data ' + taskIdentifier);
          return null;
        }

        const parsedData = JSON.parse(storedData.value) as LocalStorageNotification;
        return parsedData;
      });
  }

  private writeNotificationToStorage(taskModel: TaskModel, notificationId: string) {
    const localStorageNotification: LocalStorageNotification = {
      notificationId,
      taskIdentifier: taskModel.identifier,
      dueDate: taskModel.dueDate.toISOString(),
    };

    const data = JSON.stringify(localStorageNotification);

    console.log(data);
    return Plugins.Storage.set({
      key: this.storageKeyPrefix + taskModel.identifier,
      value: data
    });
  }

  private removeNotificationInStorage(taskIdentifier: string) {
    console.log(taskIdentifier);
    return Plugins.Storage.remove({
      key: this.storageKeyPrefix + taskIdentifier,
    });
  }
}
