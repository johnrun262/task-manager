import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { TasksService } from './tasks.service';

@Injectable({
  providedIn: 'root'
})
export class TasksGuard implements CanActivate {
  private profileOk = false;

  constructor(
    private tasksService: TasksService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    console.log('TasksGuard canActivate');

    console.log(this.router);
    return this.tasksService.fetchMyTasks()
      .pipe(
        switchMap((myTasks) => {

          if (myTasks && myTasks.length > 0) {

            return of(true);

          } else {

            this.router.navigateByUrl('/groups');
            return of(false);

          }
        })
      );

  }
}
