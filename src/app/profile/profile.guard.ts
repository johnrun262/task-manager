import { UserService } from './../users/user.service';
import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivate } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileGuard implements CanActivate {
  private profileOk = false;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    console.log('canActivate');

    return this.userService.getMyProfile()
      .pipe(
        switchMap((myProfile) => {

          if (myProfile) {

            return of(true);

          } else {

            return this.userService.fetchMyProfile()
              .pipe(
                // tslint:disable-next-line: arrow-return-shorthand
                catchError((_) => { return of(false); }),
                switchMap((myProfile) => {
                  if (!myProfile) {
                    this.router.navigateByUrl('/profile');
                    return of(false);
                  }

                  this.profileOk = true;
                  return of(true);
                })
              );

          }
        })
      );

  }
}
