import { AuthService } from './../../auth/auth.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController, NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  public changePasswordForm: FormGroup;
  public errorMessage: string;
  public isLoading = false;

  constructor(
    private navParams: NavParams,
    private authService: AuthService,
    private alertController: AlertController,
    private popOverController: PopoverController
  ) { }

  ngOnInit() {
    this.changePasswordForm = new FormGroup({
      currentPassword: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required]
      }),
      newPassword: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(8), this.passwordValidator]
      }),
    });
  }

  ngOnDestroy() {
  }

  onChangePassword() {
    console.log('onChangePassword');

    if (!this.changePasswordForm.valid) {
      console.error('not valid');
      return;
    }

    const userId = this.navParams.get('user');
    if (!userId) {
      console.error('no user');
      return;
    }

    this.isLoading = true;

    const currentPassword = this.changePasswordForm.value.currentPassword;
    const newPassword = this.changePasswordForm.value.newPassword;

    this.authService.changePassword(userId, currentPassword, newPassword)
      .then((_) => {
      })
      .catch((errorMessage) => {
        console.error('failMessage ' + errorMessage);
        this.errorMessage = errorMessage;
      })
      .finally(() => {
        this.alertController.create(
          {
            header: 'Password Change',
            message: this.errorMessage ? this.errorMessage : 'Your Password has been Changed',
            buttons: [
              'OK'
            ]
          }
        ).then((alert) => {
          alert.present()
            .then((_) => {
              this.popOverController.dismiss();
            });
        });
        this.isLoading = false;
      });
  }

  onCancel() {
    this.popOverController.dismiss();
  }

  private passwordValidator(formControl: FormControl) {
    const hasNumber = /\d/.test(formControl.value);
    const hasLower = /[A-Z]/.test(formControl.value);
    const hasUpper = /[a-z]/.test(formControl.value);

    if (hasLower && hasUpper && hasNumber) {
      return null;
    }

    return { weak: true };
  }
}
