import { catchError } from 'rxjs/operators';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AuthService } from './../auth/auth.service';
import { Subscription, throwError, BehaviorSubject } from 'rxjs';
import { UserService } from './../users/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PopoverController, AlertController, NavController } from '@ionic/angular';
import { CalendarService } from '../tasks/calendar.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit, OnDestroy {
  private myProfileSubscription: Subscription;
  public profileForm: FormGroup;
  isLoading = true;
  createProfile = false;
  email: string;
  user: string;
  public errorMessage: string;
  private didFailSubscription: Subscription;
  public calendarPlatform: boolean;


  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private popOverController: PopoverController,
    private alertController: AlertController,
    private navController: NavController,
    private calendarService: CalendarService
  ) { }

  ngOnInit() {
    this.calendarPlatform = this.calendarService.calendarPlatform();

    this.profileForm = new FormGroup({
      name: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.maxLength(80)]
      }),
      title: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.maxLength(80)]
      }),
      addTasksToCalendar: new FormControl(true, {
        updateOn: 'change',
        validators: [Validators.required]
      })
    });

    this.authService.getProfileAttributes()
      .then((attributes) => {

        console.log(attributes);
        this.email = attributes.email;
        this.user = attributes['cognito:username'];
        
        this.myProfileSubscription = this.userService.getMyProfile()
          .subscribe(
            (myProfile) => {
              console.log(myProfile);

              if (myProfile) {
                this.profileForm.patchValue({
                  name: myProfile.name,
                  title: myProfile.title,
                  addTasksToCalendar: myProfile.addTasksToCalendar
                });

                this.createProfile = false;
                this.isLoading = false;

              } else {

                this.createProfile = true;
                this.isLoading = false;

              }
            });
      });

  }

  ngOnDestroy() {
    if (this.myProfileSubscription) {
      this.myProfileSubscription.unsubscribe();
    }

    if (this.didFailSubscription) {
      this.didFailSubscription.unsubscribe();
    }
  }

  ionViewWillEnter() {
    this.isLoading = true;

    this.userService.fetchMyProfile()
      .pipe(
        catchError(
          (error) => {
            this.isLoading = false;
            return error;
          })
      )
      .subscribe((groups) => {
        this.isLoading = false;
      });
  }

  onUpdate() {
    const name = this.profileForm.value.name;
    const title = this.profileForm.value.title;
    const addTasksToCalendar = this.profileForm.value.addTasksToCalendar;

    console.log(name);
    console.log(title);
    console.log(addTasksToCalendar);

    if (this.createProfile) {
      this.userService.createMyProfile(this.email, name, title, addTasksToCalendar)
        .subscribe(
          (response) => {
            console.log(response);
            this.createProfile = false;
            this.router.navigateByUrl('/groups');
          },
          (error) => {
            console.error(error);
          });

    } else {

      this.userService.updateMyProfile(name, title, addTasksToCalendar)
        .subscribe(
          (response) => {
            console.log(response);
            this.navController.back();
          },
          (error) => {
            console.error(error);
            this.alertController.create(
              {
                header: 'Update Profile Failed',
                message: 'Refresh the page and try again',
                buttons: [
                  'OK'
                ]
              }
            ).then((alert) => {
              alert.present();
              this.navController.back();
            });
          });
    }
  }

  onDelete() {
    this.alertController.create(
      {
        header: 'Delete Profile',
        message: 'Deleting your Profile will also delete all groups for which you are the sole owner',
        buttons: [
          {
            text: 'Delete',
            handler: (_) => this.doDelete()
          },
          'Cancel'
        ]
      }
    ).then((alert) => {
      alert.present();
    });

  }

  private doDelete() {
    if (this.user) {

      this.isLoading = true;

      this.userService.deleteMyProfile()
        .subscribe(
          (response) => {
            console.log(response);
            this.createProfile = true;

            this.authService.deleteUser(this.user)
              .then((_) => {
                this.router.navigateByUrl('/auth');
              })
              .catch((errorMessage) => {
                console.error(errorMessage);
                this.errorMessage = errorMessage;
                this.isLoading = false;
              });

          },
          (error) => {
            console.error(error);
            this.isLoading = false;
          });
    }
  }

  onChangePassword() {
    this.popOverController.create({
      component: ChangePasswordComponent,
      componentProps: { user: this.user },
      cssClass: "popover-style"
    }).then((popover) => {
      popover.present();
    });
  }

  onCancel() {
    this.navController.back();
  }
}
