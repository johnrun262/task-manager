import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireMessagingModule } from 'angularfire2/messaging';
import { Calendar } from '@ionic-native/calendar/ngx';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp({
      apiKey: environment.FIREBASE_CONFIG.apiKey,
      authDomain: environment.FIREBASE_CONFIG.authDomain,
      databaseURL: environment.FIREBASE_CONFIG.databaseURL,
      projectId: environment.FIREBASE_CONFIG.projectId,
      storageBucket: environment.FIREBASE_CONFIG.storageBucket,
      messagingSenderId: environment.FIREBASE_CONFIG.messagingSenderId,
      appId: environment.FIREBASE_CONFIG.appId
    }),
    AngularFireMessagingModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Calendar
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
