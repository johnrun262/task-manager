import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private router: Router,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.authService.authStatus.subscribe(
      (authenticated) => {
        if (authenticated) {
          console.log('app page authenticated');
          this.router.navigate(['/']);

        } else {

          console.log('app page not authenticated');
          this.router.navigate(['/auth']);

        }
      }
    );
    this.authService.initAuth();

  }

  ngOnDestroy() {
  }

  onLogout() {
    console.log('onLogout');
    this.authService.logout();
  }
}
