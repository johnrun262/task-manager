export class UserModel {
    constructor(
        public email: string,
        public identifier: string,
        public name: string,
        public title: string,
        public addTasksToCalendar: boolean) { }
}
