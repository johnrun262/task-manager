import { environment } from './../../environments/environment.prod';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { UserModel } from './user.model';
import { catchError, map, tap } from 'rxjs/operators';

class UserResponse {
  email: string;
  identifier: string;
  name: string;
  title: string;
  addTasksToCalendar: boolean
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private myProfile = new BehaviorSubject<UserModel>(null);

  constructor(
    private httpClient: HttpClient
  ) { }

  getMyProfile(): Observable<UserModel> {
      return this.myProfile.asObservable();
  }


  public getUserById(idToken: string): Observable<UserModel> {
    console.log(idToken);

    return this.httpClient.get<UserResponse>(
      environment.API_URL + 'users/' + idToken)
      .pipe(
        catchError(this.handleError),
        map(responseData => {
          console.log('getMyProfile');
          console.log(responseData);

          const userModel = new UserModel(
            responseData.email,
            responseData.identifier,
            responseData.name,
            responseData.title,
            responseData.addTasksToCalendar);

          return userModel;

        }),
      );

  }

  public fetchMyProfile(): Observable<UserModel> {
    return this.httpClient.get<UserResponse>(
      environment.API_URL + 'users/myprofile',
    )
      .pipe(
        // catchError(this.handleError),
        map(responseData => {
          console.log('getMyProfile');
          console.log(responseData);

          const userModel = new UserModel(
            responseData.email,
            responseData.identifier,
            responseData.name,
            responseData.title,
            responseData.addTasksToCalendar);

          this.myProfile.next(userModel);

          return userModel;

        }),
      );

  }

  public createMyProfile(email: string, name: string, title: string, addTasksToCalendar: boolean): Observable<UserModel> {

    return this.httpClient.post<UserResponse>(
      environment.API_URL + 'users',
      {
        email,
        name,
        title,
        addTasksToCalendar
      }
    )
      .pipe(
        catchError(this.handleError),
        map(responseData => {
          console.log('createMyProfile');
          console.log(responseData);

          const userModel = new UserModel(
            email,
            responseData.identifier,
            name,
            title,
            addTasksToCalendar);

          this.myProfile.next(userModel);
          return userModel;

        }),
      );

  }

  public updateMyProfile(name: string, title: string, addTasksToCalendar: boolean): Observable<any> {
    return this.httpClient.patch<UserModel>(
      environment.API_URL + 'users/myprofile',
      {
        name,
        title,
        addTasksToCalendar
      }
    )
      .pipe(
        catchError(this.handleError),
        tap(responseData => {
          console.log('updateMyProfile');
          console.log(responseData);

          this.myProfile.next(responseData);
        }),
      );

  }

  public deleteMyProfile(): Observable<any> {
    return this.httpClient.delete<UserModel>(
      environment.API_URL + 'users/myprofile'
    )
      .pipe(
        catchError(this.handleError),
        tap(responseData => {
          console.log('deleteMyProfile');
          console.log(responseData);

          this.myProfile.next(null);
        }),
      );

  }

  public onLogout() {
    console.log('onLogout');
    this.myProfile.next(null);
  }

  private handleError(error: HttpErrorResponse) {
    console.error(error);
    return throwError(error);
  }
}
